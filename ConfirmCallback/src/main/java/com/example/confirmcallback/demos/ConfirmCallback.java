package com.example.confirmcallback.demos;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
/**
 * 实现rabbitTemplate的confirmCallback方法
 * confirmCallback（交换机回调方法）执行条件
 * 1. 发送消息后，交换机接收到消息
 * 2. 发送消息后，交换机接收消息失败
 */
public class ConfirmCallback implements RabbitTemplate.ConfirmCallback{



    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private ConnectionFactory connectionFactory;
    // 将实现类注入，注入顺序:constructor -> autowired -> postConstruct

    @PostConstruct
    public void init() {
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setConnectionFactory(connectionFactory);
    }

    /**
     *
     * @param correlationData 保存回调消息的id和相关信息，发送消息时填写
     * @param ack 交换机是否接收到消息
     * @param cause 消息接收失败的原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        String id = correlationData.getId() != null ? correlationData.getId() : "";
        if(ack) {
            log.info("{}交换机接收到消息，消息id{}",correlationData.getReturned().getExchange(), id);
        }else {
            log.warn("{}交换机接收id为{}的消息失败，原因：{}",correlationData.getReturned().getExchange(),id,cause);
        }
    }
}
