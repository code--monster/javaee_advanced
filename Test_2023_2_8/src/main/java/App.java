import component.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main1(String[] args) {
        //得到spring上下文
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        //得到bean对象
        UserController uc = context.getBean("userController", UserController.class);
        //使用bean对象
        uc.sayHi();
    }

    public static void main2(String[] args) {
        //得到spring的上下文
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        //得到bean对象
        User user = context.getBean("user",User.class);
        //使用bean
        System.out.println(user.toString());
    }

    public static void main3(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        User user = context.getBean("s1",User.class);
        System.out.println(user.toString());
    }

    public static void main4(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        UserController userController = context.getBean("userController", UserController.class);
        System.out.println(userController.getUserService().getUser().toString());
    }

    public static void main5(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        UserController2 userController2 = context.getBean("userController2", UserController2.class);
        System.out.println(userController2.getUser().toString());
    }

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        UserController3 userController3 = context.getBean("userController3", UserController3.class);
        System.out.println(userController3.getUser().toString());
    }
}
