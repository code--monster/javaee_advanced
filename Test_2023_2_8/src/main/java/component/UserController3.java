package component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController3 {
    private UserService userService;
    //通过构造方法注入，来获取bean对象
    @Autowired
    public UserController3(UserService userService) {
        this.userService = userService;
    }
    public User getUser() {
        return userService.getUser();
    }
}
