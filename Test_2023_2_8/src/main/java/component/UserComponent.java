package component;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class UserComponent {
    @Bean
    public User user() {
        User user = new User();
        user.setId(10);
        user.setName("张三");
        return user;
    }
}
