package component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController2 {
    private UserService userService;
    //适用Setter注入
    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    public User getUser() {
        return userService.getUser();
    }
}
