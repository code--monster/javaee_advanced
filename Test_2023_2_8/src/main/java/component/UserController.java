package component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {
    public void sayHi() {
        System.out.println("使用@Controller将对象存入spring中");
    }
    @Bean
    public User user() {
        User user = new User();
        user.setId(10);
        user.setName("张三");
        return user;
    }
    @Bean(name = "s1")
    public User user2() {
        User user = new User();
        user.setId(20);
        user.setName("李四");
        return user;
    }
    //获取bean对象，属性注入
    @Autowired
    private UserService userService;

    public UserService getUserService() {
        return userService;
    }
}
