package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/stu")
@Controller
@ResponseBody
public class StudentController {
    private static final Logger logger = LoggerFactory.getLogger(StudentController.class);
    @RequestMapping("/hi")
    public String sayHi() {
        logger.trace("Student类的trace级别");
        logger.warn("Student类的warn级别");
        logger.error("Student类的error级别");
        return "hi,Student";
    }
}
