package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody //表示返回的是数据而非页面
@RequestMapping("/user")
@Slf4j
public class UserController {
    //1. 得到日志对象
    //private static final Logger logger = LoggerFactory.getLogger(UserController.class);//表示该日志来自于哪个类
    @RequestMapping("/hi")
    public String sayHi() {
        //写日志
        log.trace("trace级别");
        log.debug("debug级别");
        log.info("info级别");
        log.warn("warn级别");
        log.error("error级别");
        return "hi";
    }
}
