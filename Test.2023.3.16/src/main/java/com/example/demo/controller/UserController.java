package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {
    @RequestMapping("/getuser")
    public String getUser() {
        System.out.println("执行了");
        return "get user";
    }
    @RequestMapping("/login")
    public String login() {
        Object object = null;
        object.hashCode();
        System.out.println("执行了login");
        return "login";
    }
    @RequestMapping("/reg")
    public String reg() {
        System.out.println("注册");
        return "注册";
    }
}
