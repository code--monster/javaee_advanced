package com.example.demo.config;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//验证登入信息的拦截器
public class LoginInterceptor implements HandlerInterceptor {
    //此方法返回一个boolean，如果为true表示验证成功，继续执行后续流程
    //如果是false表示验证失败，后面流程不能执行
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute("userinfo") != null) {
            return true;
        }
        //身份验证失败，重定向到登入页面
        response.sendRedirect("/login2.html");
        return false;
    }
}
