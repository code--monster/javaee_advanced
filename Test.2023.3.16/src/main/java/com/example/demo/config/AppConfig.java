package com.example.demo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")  //**表示拦截所有请求
                .excludePathPatterns("/user/login") //不拦截的url地址
                .excludePathPatterns("/user/reg")
                .excludePathPatterns("/login2.html")
                .excludePathPatterns("/login.html");//登入页面不拦截，否则会报重定向多次的错误
    }

//    @Override
//    public void configurePathMatch(PathMatchConfigurer configurer) {
//        configurer.addPathPrefix("/zhangsan",  c ->true);//设置true启动前缀
//    }
}
