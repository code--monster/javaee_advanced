package com.example.test_2023_12_8_2;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class RabbitAdminTest {
    @Autowired
    private RabbitAdmin rabbitAdmin;
    @Test
    public void declareExchange() {
        rabbitAdmin.declareExchange(new DirectExchange("AdminDirectEx", false, false, null));
        rabbitAdmin.declareExchange(new FanoutExchange("AdminFanoutEx", false, false, null));
        rabbitAdmin.declareExchange(new TopicExchange("AdminTopicEx", false, false, null));
    }

    @Test
    public void declareQueue() {
        //
        rabbitAdmin.declareQueue(new Queue("directAdmin1", false, false, false));
        //使用链式方法创建的队列默认进行持久化操作
        rabbitAdmin.declareQueue(QueueBuilder.durable("directAdmin2").build());

        rabbitAdmin.declareQueue(new Queue("fanoutAdmin1", false, false, false));
        rabbitAdmin.declareQueue(QueueBuilder.durable("fanoutAdmin2").build());

        rabbitAdmin.declareQueue(new Queue("topicAdmin1", false, false, false));
        rabbitAdmin.declareQueue(QueueBuilder.durable("topicAdmin2").build());
    }

    @Test
    public void declareBind() {
        /*
         * 创建绑定方式1
         *   参数一：destination(队列名)
         *   参数二：基于什么的绑定（队列或交换机）
         *   参数三：交换机名称
         *   参数四：routingkey
         *   参数五：附加参数
         */
        rabbitAdmin.declareBinding(new Binding("directAdmin1", Binding.DestinationType.QUEUE,
                "AdminDirectEx", "info", null));

        /*
        使用链式方法进行绑定创建
         */
        rabbitAdmin.declareBinding(BindingBuilder.bind(new Queue("directAdmin2", false))
                .to(new DirectExchange("AdminDirectEx"))
                .with("info"));

        rabbitAdmin.declareBinding(new Binding("fanoutAdmin1", Binding.DestinationType.QUEUE,
                "AdminFanoutEx", "", null));
        rabbitAdmin.declareBinding(BindingBuilder.bind(new Queue("fanoutAdmin2", false))
                .to(new FanoutExchange("AdminFanoutEx")));


        rabbitAdmin.declareBinding(new Binding("topicAdmin1", Binding.DestinationType.QUEUE,
                "AdminTopicEx", "user.#", null));
        rabbitAdmin.declareBinding(BindingBuilder.bind(new Queue("topicAdmin2", false))
                .to(new TopicExchange("AdminTopicEx"))
                .with("vip.*"));



    }
}
