package com.example.test_2023_12_8_2;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.amqp.support.ConsumerTagStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.UUID;

@Configuration
public class RabbitAdminConfig {
    @Autowired
    private RabbitProperties properties;

    /**
     * RabbitMQ连接池，从配置文件中读取参数
     * @return
     */
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();
        cachingConnectionFactory.setHost(properties.getHost());
        cachingConnectionFactory.setPort(properties.getPort());
        cachingConnectionFactory.setUsername(properties.getUsername());
        cachingConnectionFactory.setPassword(properties.getPassword());
        cachingConnectionFactory.setVirtualHost(properties.getVirtualHost());
        return cachingConnectionFactory;
    }

    @Bean
    public RabbitAdmin rabbitAdmin(CachingConnectionFactory cachingConnectionFactory) {
        return new RabbitAdmin(cachingConnectionFactory);
    }

    @Bean
    public RabbitTemplate rabbitTemplate(CachingConnectionFactory cachingConnectionFactory) {
        return new RabbitTemplate(connectionFactory());
    }
    @Bean
    public DirectExchange directExchange(){
        System.out.println("TemplateDirectEx");
        return new DirectExchange("TemplateDirectEx",false,false);
    }

    @Bean
    public FanoutExchange fanoutExchange(){
        System.out.println("TemplateFanoutEx");
        return new FanoutExchange("TemplateFanoutEx",false,false);
    }

    @Bean
    public TopicExchange topicExchange(){
        System.out.println("TemplateTopicEx");
        return new TopicExchange("TemplateTopicEx",false,false);
    }

    /*
    创建队列
    */
    @Bean
    public Queue directQueue1() {
        return new Queue("directQueue1", true);
    }
    @Bean
    public Queue directQueue2() {
        return new Queue("directQueue2", true);
    }
    @Bean
    public Queue topicQueue1() {
        return QueueBuilder.durable("topicQueue1").build();
    }

    @Bean
    public Queue topicQueue2() {
        return QueueBuilder.durable("topicQueue2").build();
    }

    /*
    创建绑定
     */
    // 方式1
    @Bean
    public Binding directBind1() {
        return new Binding("directQueue1", Binding.DestinationType.QUEUE,
                "TemplateDirectEx", "weixin", null);
    }
    // 方式2
    @Bean
    public Binding directBind2() {
        return BindingBuilder.bind(new Queue("directQueue2", false))
                .to(new DirectExchange("TemplateDirectEx"))
                .with("weixin");
    }
    // 方式3,将bean方法名作为参数
    @Bean
    public Binding topicBind1(@Qualifier("topicQueue1") Queue queue,
                              @Qualifier("topicExchange") TopicExchange topicExchange) {
        return BindingBuilder.bind(queue).to(topicExchange).with("user.#");
    }

    @Bean
    public Binding topicBind2(@Qualifier("topicQueue2") Queue queue,
                              @Qualifier("topicExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("vip.*");
    }

    @Bean
    public SimpleMessageListenerContainer messageListenerContainer(CachingConnectionFactory cachingConnectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(cachingConnectionFactory);
        container.setQueues(directQueue1(),topicQueue1(), topicQueue2()); // 设置监听队列
        container.setConcurrentConsumers(1);
        container.setMaxConcurrentConsumers(10);
        container.setDefaultRequeueRejected(false);
        container.setAcknowledgeMode(AcknowledgeMode.AUTO);
        container.setConsumerTagStrategy(new ConsumerTagStrategy() {
            @Override
            public String createConsumerTag(String queue) {
                return queue + "_" + UUID.randomUUID().toString();
            }
        }); // 消费端的标签策略， 每个消费端都有独立的标签
        // 消息监听器方法一，实际用消息适配器
        container.setMessageListener(new ChannelAwareMessageListener() {
            @Override
            public void onMessage(Message message, Channel channel) throws Exception {
                System.out.println("消费者的消息" + new String(message.getBody()));
            }
        });
        return container;
    }
}
