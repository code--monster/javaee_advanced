package com.example.test_2023_12_8_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Test20231282Application {

    public static void main(String[] args) {
        SpringApplication.run(Test20231282Application.class, args);
    }

}
