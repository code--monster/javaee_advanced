package com.example.demo;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @RequestMapping("/hi")
    public String sayHi(String name) {
        if(!StringUtils.hasLength(name)){
            return "hi 张三";
        }
        return "hi 李四";
    }
}
