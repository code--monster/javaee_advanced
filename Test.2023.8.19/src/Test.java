import java.util.HashSet;

public class Test {
    /**
     * 在一个长度为n的数组里的所有数字都在0到n-1的范围内。 数组中某些数字是重复的，但不知道有几个数字是重复的。
     * 也不知道每个数字重复几次。请找出数组中任意一个重复的数字。 例如，如果输入长度为7的数组[2,3,1,0,2,5,3]，那么对应的输出是2或者3。
     * 存在不合法的输入的话输出-1
     */
    private static int duplicate(int[] nums) {

        HashSet<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if(set.contains(nums[i])) {
                return nums[i];
            }
            set.add(nums[i]);
        }
        return -1;
    }
    // 学生表student中有如下几列：id（学号）、班级（class_id）、年龄（age）、性别（gender），
    // 要求查出每个班级中年龄大于18岁的女生的数量
    // select count(*) from student  where gender = "girl"  and age > 18  group by class_id

    // 修改表user中name中包含xxx的记录name改为yyy，只改1条
    // update user set name = yyy where name like "%xxx%" limit 1


//    public static void main(String[] args) {
//        System.out.println(duplicate());
//    }
    // linux ,mysql, spring源码，tcp,http, redis，项目功能扩展
    // 天梯积分榜：如有有人分数修改，就修改redis。数据量大了就定时
    //
}
