package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;


@Controller
@RequestMapping("/resp")
//不添加@Responsebody 表示返回的是页面
public class RespController {
    //请求重定向
    @RequestMapping("/redirect")
    public String index1() {
        return "redirect:/index.html";
    }
    @RequestMapping("/forward")
    public String index2() {
        return "forward:/index.html";
    }

    @RequestMapping("/json")
    @ResponseBody //表示返回的不是页面，是数据
    public HashMap<String, String> returnJson() {
        HashMap<String, String> map = new HashMap<>();
        map.put("java", "java value");
        map.put("mysql", "mysql value");
        map.put("spring", "spring value");
        return map;
    }

    @RequestMapping("/index")
    public Object index() {
        return "/index.html";
    }
}
