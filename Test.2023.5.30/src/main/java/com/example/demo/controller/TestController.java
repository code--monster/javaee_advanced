package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/test")
@Slf4j
public class TestController {
    @RequestMapping("/getsess2")
    public String getSession2(@SessionAttribute(value = "userinfo", required = false) //非必传
                                      String userinfo) {
        return userinfo;
    }

    @RequestMapping("/getsess")
    public String getSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);//没有session不新建
        if(session != null && session.getAttribute("userinfo") != null) {
            return (String) session.getAttribute("userinfo");
        }
        return "暂无session信息";
    }

    @RequestMapping("/setsess")
    public String setSession(HttpServletRequest request) {
        HttpSession session = request.getSession(true);//没有session就新建
        session.setAttribute("userinfo", "用户信息");//设置session的键值对
        return "set session success";
    }

    @RequestMapping("/header2")
    public String getHeader2(@RequestHeader("User-Agent") String header) {
        return header;
    }
    @RequestMapping("/header1")
    public String getHeader1(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");
        return header;
    }
    @RequestMapping("/getSingleCookie")
    public String getCookie(@CookieValue("zhangsan") String value) {
        return "Cookie Value: " + value;
    }
    @RequestMapping("/getck")
    public String getCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie item :
                cookies) {
            //通过日志来打印
            log.error(item.getName() + ": " + item.getValue());
        }
        return "get cookie";
    }
    @RequestMapping("/login/{username}/{password}")//位置一定要正确
    public String login(@PathVariable("username") String name,
                        @PathVariable("password") String password) {
        return name + ": " + password;
    }
    @RequestMapping("/upfile")
    public String upfile(@RequestPart("myfile")MultipartFile file) throws IOException {
        //根目录
        String path = "D:\\javaee_advanced\\Test.2023.5.30\\";
        //根目录 + 唯一的文件名（使用UUID生成随机数）
        path += UUID.randomUUID().toString().replace("-", "");
        //再加上文件的后缀
        path += file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
        //保存文件
        file.transferTo(new File(path));
        return path;
    }
}
