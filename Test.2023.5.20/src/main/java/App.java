
import com.controller.UserController;
import com.service.UService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class App {
    public static void main1(String[] args) {
        //1.得到Spring对象
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        //2.通过Spring取Bean对象
        UserController user = context.getBean("userController", UserController.class);
        //3. 使用Bean对象
        user.sayHi("张三");
    }

    public static void main2(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        UService user = context.getBean("UService", UService.class);
        user.sayHi("李四");
    }


}
