package com.component;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        //通过方法的原名取Bean对象
        User user = context.getBean("user2", User.class);
        System.out.println(user.toString());
    }
}
