package com.component;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

class User{
    private int id;
    private String name;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }
}
@Component
public class UserComponent {
    //取别名后，原来的方法名不能在使用
    @Bean(name = {"user1", "user2"})
    public User getUser() {
        //伪代码，在spring中是不使用new的
        User user = new User();
        user.setId(1);
        user.setName("王五");
        return user;
    }
}
