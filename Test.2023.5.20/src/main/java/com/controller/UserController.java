package com.controller;

import org.springframework.stereotype.Controller;

@Controller
public class UserController {
    public void sayHi(String name) {
        System.out.println("hi: " + name);
    }
}
