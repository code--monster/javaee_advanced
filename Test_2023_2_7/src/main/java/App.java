import component.UserController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
在 Spring 项⽬中，通过 main ⽅法获取到 Controller 类，调⽤ Controller ⾥⾯通过注⼊的⽅式调⽤
Service 类，Service 再通过注⼊的⽅式获取到 Repository 类，Repository 类⾥⾯有⼀个⽅法构建⼀
个 component.User 对象，返回给 main ⽅法。Repository ⽆需连接数据库，使⽤伪代码即可
 */
public class App {
    public static void main(String[] args) {
        //获取spring上下文
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        //获取Bean对象
        UserController userController = (UserController) context.getBean("userController");
        //调用bean方法
        System.out.println(userController.getUser().toString());
    }
}
