package component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {
    //属性注入
    @Autowired
    private UserService userService;
    public User getUser() {
        return userService.getUser();
    }
}
