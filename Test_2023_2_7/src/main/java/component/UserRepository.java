package component;

import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {
    //构造user对象
    public User getUser() {
        User user = new User();
        user.setAge(10);
        user.setName("张三");
        return user;
    }
}
