public class PoliceMan implements Observer{
    private String name;
    public PoliceMan(String name) {
        this.name = name;
    }
    @Override
    public void steal(String message) {
        System.out.println("小偷：" + message);
    }
}
