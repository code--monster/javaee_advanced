public class Client {
    public static void main(String[] args) {
        SubscriptionSubject sb = new SubscriptionSubject();
        PoliceMan man1 = new PoliceMan("mack");
        PoliceMan man2 = new PoliceMan("zhangsan");
        PoliceMan man3 = new PoliceMan("mary");
        // 订阅
        sb.attach(man1);
        sb.attach(man2);
        sb.attach(man3);
        // 推送消息
        sb.notify("张三正在偷东西");
    }
}
