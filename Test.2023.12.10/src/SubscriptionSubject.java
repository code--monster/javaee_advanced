import java.util.ArrayList;
import java.util.List;

public class SubscriptionSubject implements Subject {
    // 存储警察
    private List<Observer> policeManList = new ArrayList<>();

    @Override
    public void attach(Observer observer) {
        policeManList.add(observer);
    }

    @Override
    public void detach(Observer observer) {
        policeManList.remove(observer);
    }

    @Override
    public void notify(String message) {
        for (Observer observer :
                policeManList) {
            observer.steal(message);
        }
    }
}
