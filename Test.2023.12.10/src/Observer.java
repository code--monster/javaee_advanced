public interface Observer {
    void steal(String message);
}
