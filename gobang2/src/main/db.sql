create database if not exists gobang;

use gobang;

drop table if exists user;
create table user (
    userId int primary key auto_increment,
    username varchar(50) unique,
    password varchar(255),
    score int,       -- 天梯积分
    totalCount int,  -- 比赛总场数
    winCount int     -- 获胜场数
);
# 创建username字段的索引
create index idx_name on user(username);

drop table if exists friend;
# 满足第二范式：非主键要全表依赖主键，不能部分依赖
create table friend(
    userId int,
    friendId int,
    friendName varchar(50),
    score int,
    totalCount int,
    winCount int,
    primary key(userId, friendId)
);
