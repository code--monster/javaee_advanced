package com.example.demo.api;

import com.example.demo.config.PasswordConfig;
import com.example.demo.config.SessionKeyConfig;
import com.example.demo.mapper.FriendMapper;
import com.example.demo.model.User;
import com.example.demo.mapper.UserMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

@RestController
public class UserAPI {
    @Resource
    private UserMapper userMapper;

    @Resource
    private FriendMapper friendMapper;

    @Autowired
    private StringRedisTemplate redisTemplate;


    ObjectMapper objectMapper = new ObjectMapper();
    /**
     * 注册
     * @param username
     * @param password
     * @return 返回创建的对象
     */
    @PostMapping("/register")
    @ResponseBody
    public Object register(String username, String password, String confirmPassword) {
        if(!password.equals(confirmPassword)) {
            User user = new User();
            return user;
        }
        try{
            User user = new User();
            user.setUsername(username);
            // 对密码加密
            user.setPassword(PasswordConfig.encrypt(password));
            userMapper.insert(user);

            return user;
        }catch (org.springframework.dao.DuplicateKeyException e) {
            User user = new User();
            return user;
        }
    }

    /**
     * 登陆功能
     * @param username
     * @param password
     * @return
     */
    @PostMapping("/login")
    @ResponseBody
    public Object login(String username, String password, @RequestParam("verifyCode") String captchaCode, HttpServletRequest request) {
        // 检测验证码是否正确
        if(!getCheckCaptcha(captchaCode, request)) {
            // 无效用户
            return new User();
        }
        // 去数据库查询用户信息
        User user = userMapper.selectByName(username);
        if(user != null && user.getUserId() > 0) { // 有效用户
            // 验证密码是否相同
            if(PasswordConfig.check(password, user.getPassword())) {
                // 登入成功,将会话存入redis中，持久化session
                // spring session会自动把session存储到redis中
                // 没有会话就创建，有会话就使用
                HttpSession session = request.getSession(true);
                session.setAttribute(SessionKeyConfig.USER_SESSION_KEY, user);
                return user;
            }
        }
        // 无效用户
        return new User();
    }

    /**
     * 校验验证码功能
     * @param captchaCode
     * @param request
     * @return
     */
    public boolean getCheckCaptcha(String captchaCode, HttpServletRequest request) {
        try {
            // toLowerCase() 不区分大小写进行验证码校验
            String sessionCode = String.valueOf(request.getSession().getAttribute("verifyCode")).toLowerCase();
            System.out.println("session里的验证码：" + sessionCode);
            String receivedCode = captchaCode.toLowerCase();
            System.out.println("用户的验证码：" + receivedCode);
            return !"".equals(sessionCode) && !"".equals(receivedCode) && sessionCode.equals(receivedCode);
        } catch (Exception e) {
            return false;
        }
    }

    @GetMapping("/userInfo")
    public Object getUserInfo(HttpServletRequest request) {
        try{
            HttpSession session = request.getSession(false);
            User user = (User) session.getAttribute(SessionKeyConfig.USER_SESSION_KEY);
            //  拿着user对象去数据库中查找，找到最新数据，redis做缓存
            String jsonUser = redisTemplate.opsForValue().get(user.getUsername());
            if(jsonUser != null) {
                User redisUser = objectMapper.readValue(jsonUser, User.class);
                if (redisUser != null) {
                    return redisUser;
                }
            }
            // redis中没有找到，去mysql中查找并更新到redis中
            User newUser = userMapper.selectByName(user.getUsername());
            redisTemplate.opsForValue().set(newUser.getUsername(), objectMapper.writeValueAsString(newUser));//将java对象转成json字符串放入redis中
            // 不能给前端返回敏感信息
            newUser.setPassword("");
            return newUser;
        }catch (NullPointerException | JsonProcessingException e) {
            return new User();
        }
    }

    /**
     * 搜索关注玩家，支持模糊搜索
     * @param username
     * @return
     */
    @GetMapping("/search")
    public Object SearchPlayer(String username) {
        if(username == null) {
            return new User();
        }
        // 利用覆盖索引搜索用户
        List<User> users = userMapper.searchUsers(username);
        return users;
    }

    /**
     * 积分排行榜，每隔1分钟刷新一次排行榜数据
     * @param key
     * @return
     */
    @GetMapping("/getRanking")
    public Set<ZSetOperations.TypedTuple<String>> getRanking(@RequestParam String key) {
        return redisTemplate.opsForZSet().reverseRangeWithScores(key,0, 5);
    }


}

