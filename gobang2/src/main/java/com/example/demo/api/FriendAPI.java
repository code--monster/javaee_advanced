package com.example.demo.api;

import com.example.demo.config.SessionKeyConfig;
import com.example.demo.mapper.FriendMapper;
import com.example.demo.model.Friend;
import com.example.demo.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.HashSet;
import java.util.Set;

@RestController
public class FriendAPI {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private FriendMapper friendMapper;
    @Autowired
    private StringRedisTemplate redisTemplate;

    @PostMapping("/addFriend")
    public Integer addFriend(Integer playerId, HttpServletRequest request) throws JsonProcessingException {
        int result = -1;
        HttpSession session = request.getSession(false);
        User user = (User) session.getAttribute(SessionKeyConfig.USER_SESSION_KEY);
        if(user == null) {
            return -1;
        }
        if(playerId < 0 ) {
            return -1;
        }
        result =  friendMapper.addFriend(playerId, user.getUserId());
        // 关注新的玩家，更新redis
        User newUser = friendMapper.selectFriend(user.getUserId());
        // 隐藏敏感信息
        newUser.setPassword("");
        redisTemplate.opsForSet().add(user.getUsername() + "friend",objectMapper.writeValueAsString(newUser));
        return result;
    }

    @GetMapping("/getFriend")
    public Set<String> getFriend(HttpServletRequest request) {
        try{
            HttpSession session = request.getSession(false);
            User user = (User) session.getAttribute(SessionKeyConfig.USER_SESSION_KEY);
            // 玩家的好友通常长时间不变，可以缓存到redis中，当关注新玩家时才刷新redis
            // 为什么要加friend呢？ 因为username已经存在string类型中
            Set<String> friends = redisTemplate.opsForSet().members(user.getUsername() + "friend");
//            if(friends.size() == 0) {
//                // 可能是没有关注玩家，也可能是没有更新到缓存中（小概率）
//                // 去数据库中找一次
//                Set<User> newFriends = friendMapper.selectAll(user.getUserId());
//                if(newFriends.size() != 0) {
//                    // 更新到redis中
//                    for (User x :
//                            newFriends) {
//                        redisTemplate.opsForSet().add(user.getUsername(), objectMapper.writeValueAsString(x));
//                        // 也要更新到friends中
//                        friends.add(x.toString());
//                    }
//                }
//            }
            return friends;
        }catch (NullPointerException e) {
            return null;
        }
    }

    @GetMapping("/flushFriendList")
    public String flushFriendList(HttpServletRequest request) {
        try{
            HttpSession session = request.getSession(false);
            User user = (User) session.getAttribute(SessionKeyConfig.USER_SESSION_KEY);
            // 去数据库中查找，再更新到redis中
            Set<Friend> friends = friendMapper.selectAll(user.getUserId());
            if(friends.size() != 0) {
                // 更新到redis中
                // 更新之前需要将redis之前存的数据清除掉
                redisTemplate.delete(user.getUsername() + "friend");
                for (Friend x:
                     friends) {
                    redisTemplate.opsForSet().add(user.getUsername() + "friend", objectMapper.writeValueAsString(x));
                }
            }
            return objectMapper.writeValueAsString(friends);
        }catch (NullPointerException | JsonProcessingException e) {
            return null;
        }

    }


}
