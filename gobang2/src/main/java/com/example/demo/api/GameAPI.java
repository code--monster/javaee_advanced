package com.example.demo.api;

import com.example.demo.config.SessionKeyConfig;
import com.example.demo.game.*;
import com.example.demo.mapper.FriendMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;

@Component
public class GameAPI extends TextWebSocketHandler {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private RoomManager roomManager;
    @Autowired
    private OnlineUserManager onlineUserManager;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private FriendMapper friendMapper;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        GameReadyResponse resp = new GameReadyResponse();
        // 1. 获取用户信息
        User user = (User) session.getAttributes().get(SessionKeyConfig.USER_SESSION_KEY);
        if(user == null) {
            resp.setOk(false);
            resp.setReason("用户尚未登入");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(resp)));
            return;
        }
        // 2. 判断当前用户是否已经进入房间
        Room room = roomManager.getRoomByUserId(user.getUserId());
        if(room == null) {
            // 如果为null，说明当前用户没有找到对应的房间，该玩家没有匹配成功
            resp.setOk(false);
            resp.setReason("用户尚未匹配到！");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(resp)));
            return;
        }
        // 3.判断当前是不是多开
        if(onlineUserManager.getFromGameHall(user.getUserId()) != null
            || onlineUserManager.getFromGameRoom(user.getUserId()) != null) {
            resp.setOk(true);
            resp.setMessage("禁止游戏多开");
            resp.setMessage("repeatConnection");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(resp)));
            return;
        }
        // 4. 设置玩家上线
        onlineUserManager.enterGameRoom(user.getUserId(),session);
        // 5. 将两个玩家放入游戏房间中
        //    前面的创建房间/匹配过程, 是在 game_hall.html 页面中完成的.
        //    因此前面匹配到对手之后, 需要经过页面跳转, 来到 game_room.html 才算正式进入游戏房间(才算玩家准备就绪)
        //    当前这个逻辑是在 game_room.html 页面加载的时候进行的.
        //    执行到当前逻辑, 说明玩家已经页面跳转成功了!!
        synchronized (room) {
            // 两个玩家同时连接一个房间，可能出现线程安全问题
            if(room.getUser1() == null) {
                // 把当前用户设为user1
                room.setUser1(user);
                // 规定谁先进入房间，谁就是先手方
                room.setWhiteUser(user.getUserId());
                System.out.println("玩家" + user.getUsername() + "已经准备就绪，作为玩家1");
                return;
            }
            if(room.getUser2() == null) {
                // 进入这个逻辑说明玩家1已经进入房间，现在要把当前玩家作为玩家2
                room.setUser2(user);
                System.out.println("玩家" + user.getUsername() + "已经准备就绪，作为玩家2");
                // 当两个玩家都加入成功之后, 就要让服务器, 给这两个玩家都返回 websocket 的响应数据.
                // 通知这两个玩家说, 游戏双方都已经准备好了
                // 通知玩家1
                noticeGameReady(room, room.getUser1(), room.getUser2());
                // 通知玩家2
                noticeGameReady(room, room.getUser2(), room.getUser1());
                return;
            }
        }
        // 6. 如果有其他玩家尝试连接同一个房间，就提示报错
        resp.setOk(false);
        resp.setReason("当前房间已经满了，无法加入");
        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(resp)));
    }

    private void noticeGameReady(Room room, User thisUser, User thatUser) throws IOException {
        GameReadyResponse resp = new GameReadyResponse();
        resp.setMessage("gameReady");
        resp.setOk(true);
        resp.setReason("");
        resp.setRoomId(room.getRoomId());
        resp.setThisUserId(thisUser.getUserId());
        resp.setThatUserId(thatUser.getUserId());
        resp.setWhiteUser(room.getWhiteUser());
        // 把响应数据返回给前端
        WebSocketSession session = onlineUserManager.getFromGameRoom(thisUser.getUserId());
        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(resp)));

    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        User user = (User) session.getAttributes().get(SessionKeyConfig.USER_SESSION_KEY);
        if(user == null) {
            System.out.println("[handleTextMessage] 当前玩家尚未登录! ");
            return;
        }
        // 根据玩家id获取到房间对象
        Room room = roomManager.getRoomByUserId(user.getUserId());
        // 通过room对象来处理这次具体的请求
        room.putChess(message.getPayload());
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        User user = (User) session.getAttributes().get(SessionKeyConfig.USER_SESSION_KEY);
        if(user == null) {
            return;
        }
        WebSocketSession exitSession = onlineUserManager.getFromGameRoom(user.getUserId());
        if(session == exitSession) {
            // 避免在多开情况下，第二个用户的退出动作，误删了第一个用户的会话
            onlineUserManager.exitGameRoom(user.getUserId());
        }
        System.out.println("当前用户：" + user.getUsername() + "游戏房间连接异常");
        //  通知对手获胜
        noticeThatUserWin(user);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        User user = (User) session.getAttributes().get(SessionKeyConfig.USER_SESSION_KEY);
        if(user == null) {
            return;
        }
        WebSocketSession exitSession = onlineUserManager.getFromGameRoom(user.getUserId());
        if(session == exitSession) {
            onlineUserManager.exitGameRoom(user.getUserId());
        }
        System.out.println("当前用户：" + user.getUsername() + "离开游戏房间");
        //  通知对手获胜
        noticeThatUserWin(user);

    }

    private void noticeThatUserWin(User user) throws IOException {
        // 1. 根据当前玩家，找到玩家所在房间
        Room room = roomManager.getRoomByUserId(user.getUserId());
        if(room == null) {
            // 房间已经被释放，不存在对手
            System.out.println("当前房间已经释放，无需通知对手");
            return;
        }
        // 2. 找到房间对手
        User thatUser = (user == room.getUser1()) ? room.getUser1() : room.getUser2();
        // 3. 找到对手的在线状态
        WebSocketSession session = onlineUserManager.getFromGameRoom(thatUser.getUserId());
        if(session == null) {
            // 意味着对手也掉线
            System.out.println("对手也已经掉线，无需通知");
            return;
        }
        // 4. 构造一个响应，来通知对手
        GameResponse resp = new GameResponse();
        resp.setMessage("putChess");
        resp.setUserId(thatUser.getUserId());
        resp.setWinner(thatUser.getUserId());
        session.sendMessage(new TextMessage(objectMapper.writeValueAsString(resp)));
        // 5. 更新玩家分数信息
        userMapper.userWin(thatUser.getUserId());
        // 同步更新到redis中
        User winUser = userMapper.selectByUserId(thatUser.getUserId());
        // 更新个人的信息
        redisTemplate.opsForValue().set(winUser.getUsername(), objectMapper.writeValueAsString(winUser));
        // 更新排行榜信息
        redisTemplate.opsForZSet().add("rank",winUser.getUsername(), winUser.getScore());
        // TODO 还得更新friend表的信息
        friendMapper.updateMessage(winUser);
        userMapper.userLose(user.getUserId());
        User loseUser = userMapper.selectByUserId(user.getUserId());
        // 更新个人的信息
        redisTemplate.opsForValue().set(loseUser.getUsername(),objectMapper.writeValueAsString(loseUser));
        // 更新排行榜信息
        redisTemplate.opsForZSet().add("rank",loseUser.getUsername(), loseUser.getScore());
        // TODO 更新friend表的信息
        friendMapper.updateMessage(loseUser);
        // 6. 释放房间对象
        roomManager.remove(room.getRoomId(), room.getUser1().getUserId(), room.getUser2().getUserId());
    }
}
