package com.example.demo.game;

import com.example.demo.DemoApplication;
import com.example.demo.mapper.FriendMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.model.Friend;
import com.example.demo.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.UUID;

/**
 * 由于房间不是单例的，所以不能注册到spring中
 * 如果把room注册为多例，也不行，因为room已经被roomManager管理
 * 所以只能通过入口类来解决
 */
@Data
public class Room {
    // 房间id
    private String roomId;
    private User user1;
    private User user2;

    // 先手方玩家id
    private int whiteUser;

    private OnlineUserManager onlineUserManager;

    private RoomManager roomManager;

    private UserMapper userMapper;

    private ObjectMapper objectMapper = new ObjectMapper();

    private StringRedisTemplate redisTemplate;

    private FriendMapper friendMapper;

    private static final int MAX_ROW = 15;
    private static final int MAX_COL = 15;

    // 用二维数组来表示棋盘
    // 约定，使用0表示当前位置还未落子，使用1表示user1的落子位置，使用2表示user2的落子
    private int[][] board = new int[MAX_ROW][MAX_COL];

    public Room() {
        // 构造room时生成一个唯一的字符串表示房间id
        roomId = UUID.randomUUID().toString();
        // 通过入口类记录的context来手动注入RoomManager和OnlineUserManager
        onlineUserManager = DemoApplication.context.getBean(OnlineUserManager.class);
        roomManager = DemoApplication.context.getBean(RoomManager.class);
        userMapper = DemoApplication.context.getBean(UserMapper.class);
        redisTemplate = DemoApplication.context.getBean(StringRedisTemplate.class);
        friendMapper = DemoApplication.context.getBean(FriendMapper.class);
    }

    /**
     * 处理落子请求
     * @param reqJson
     */
    public void putChess(String reqJson) throws IOException {
        // 1. 记录当前落子的位置
        GameRequest request = objectMapper.readValue(reqJson, GameRequest.class);
        GameResponse response = new GameResponse();
        // 根据落子的玩家来判定是
        int chess = request.getUserId() == user1.getUserId() ? 1 : 2;
        int row = request.getRow();
        int col = request.getCol();
        if(board[row][col] != 0) {
            // 在客服端已经对重复落子进行判定了，但是为了程序更加稳健还是再判定一次。
            System.out.println("当前位置(" + row + "," + col + ")已经有子了");
            return;
        }
        board[row][col] = chess;
        //  2. 打印出棋盘信息
        printBoard();
        //  3. 进行胜负判断
        int winner = checkWinner(row, col, chess);
        // 4. 给房间内的客服端都返回响应
        response.setMessage("putChess");
        response.setUserId(request.getUserId());
        response.setRow(row);
        response.setCol(col);
        response.setWinner(winner);
        // 要想给用户websocket数据，就需要获取到这个用户的websocketSession
        WebSocketSession session1 = onlineUserManager.getFromGameRoom(user1.getUserId());
        WebSocketSession session2 = onlineUserManager.getFromGameRoom(user2.getUserId());
        // 万一当前查到的会话为空，玩家已经下线，需要特殊处理一下
        if(session1 == null) {
            response.setWinner(user2.getUserId());
            System.out.println("玩家1掉线");
        }
        if(session2 == null) {
            response.setWinner(user1.getUserId());
            System.out.println("玩家2掉线");
        }
        // 把响应构造成json格式，通过session进行传输
        String respJson = objectMapper.writeValueAsString(response);
        if(session1 != null) {
            session1.sendMessage(new TextMessage(respJson));
        }
        if(session2 != null) {
            session2.sendMessage(new TextMessage(respJson));
        }
        // 5. 如果当前胜负已分，此时这个房间就失去了意义，需要进行回收，防止内存溢出
        if(response.getWinner() != 0) {
            System.out.println("游戏结束! 房间即将销毁! roomId=" + roomId + " 获胜方为: " + response.getWinner());
            // 更新获胜方和失败方的信息
            //  redis也要进行更新（实时更新策略）
            int winUserId = response.getWinner();
            int loseUserId = response.getWinner() == user1.getUserId() ? user2.getUserId() : user1.getUserId();
            userMapper.userWin(winUserId);
            User winUser = userMapper.selectByUserId(winUserId);
            // 按照json格式存入
            redisTemplate.opsForValue().set(winUser.getUsername(), objectMapper.writeValueAsString(winUser));
            // 更新排行榜信息
            redisTemplate.opsForZSet().add("rank", winUser.getUsername(), winUser.getScore());
            // TODO 更新friend表的信息
            friendMapper.updateMessage(winUser);
            userMapper.userLose(loseUserId);
            User loseUser = userMapper.selectByUserId(loseUserId);
            // 按照json格式存入
            redisTemplate.opsForValue().set(loseUser.getUsername(), objectMapper.writeValueAsString(loseUser));
            // 更新排行榜信息
            redisTemplate.opsForZSet().add("rank", loseUser.getUsername(), loseUser.getScore());
            // TODO 更新friend表的信息
            friendMapper.updateMessage(loseUser);
            // 销毁房间
            roomManager.remove(roomId, user1.getUserId(), user2.getUserId());
        }
    }

    /**
     * 判断当前落子的位置是否已经分出胜负
     * @param row
     * @param col
     * @param chess
     * @return 返回胜者玩家的id
     */
    private int checkWinner(int row, int col, int chess) {
        // 1. 遍历所有行
        // 一行的第一个位置相对于（row,col）有5种情况
        for(int c = col - 4; c <= col; c++) {
            // 针对其中一种情况，来判定这五个子是不是连在一起
            // 而且得和落子的颜色相同
            try{
                if(board[row][c] == chess
                    && board[row][c+1] == chess
                    && board[row][c+2] == chess
                    && board[row][c+3] == chess
                    && board[row][c+4] == chess) {
                    // 构成五子连珠，胜负已分
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            }catch (ArrayIndexOutOfBoundsException e) {
                // 如果数组越界，直接忽略，该情况作废
                continue;
            }
        }
        // 2.检查所有列
        // 一列的第一个位置相对与（row,col)有5种情况
        for(int r = row - 4; r <= row; r++) {
            try{
                if(board[r][col] == chess
                    && board[r+1][col] == chess
                    && board[r+2][col] == chess
                    && board[r+3][col] == chess
                    && board[r+4][col] == chess) {
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            }catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        // 3. 检查左对角线
        for(int r = row - 4, c = col - 4; r <= row && c <= col; r++, c++) {
            try{
                if(board[r][c] == chess
                    && board[r+1][c+1] == chess
                    && board[r+2][c+2] == chess
                    && board[r+3][c+3] == chess
                    && board[r+4][c+4] == chess) {
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            }catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        // 检查右对角线
        for(int r = row - 4, c = col + 4; r <= row && c >= col; r++, c--) {
            try{
                if(board[r][c] == chess
                    && board[r+1][c-1] == chess
                    && board[r+2][c-2] == chess
                    && board[r+3][c-3] == chess
                    && board[r+4][c-4] == chess) {
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            }catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        // 胜负未分，返回0
        return 0;
    }

    /**
     * 打印出棋盘信息
     */
    private void printBoard() {
        System.out.println("[棋盘信息]" + roomId);
        System.out.println("=====================================================================");
        for (int i = 0; i < MAX_ROW; i++) {
            for (int j = 0; j < MAX_COL; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("=====================================================================");
    }
}
