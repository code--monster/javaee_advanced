package com.example.demo.game;

import com.example.demo.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 处理匹配功能
 */
@Component
public class Matcher {
    // 创建三个匹配队列
    private Queue<User> normalQueue = new LinkedList<>();
    private Queue<User> highQueue = new LinkedList<>();
    private Queue<User> veryHighQueue = new LinkedList<>();

    @Autowired
    private OnlineUserManager onlineUserManager;

    @Autowired
    private RoomManager roomManager;

    private ObjectMapper objectMapper = new ObjectMapper();

    // 将玩家放到匹配队列中
    // 需要对不同的队列加锁，保证线程安全
    public void add(User user) {
        if(user.getScore() < 2000) {
            synchronized (normalQueue) {
                normalQueue.offer(user);
                normalQueue.notify();
            }
            System.out.println("把玩家" + user.getUsername() + "放到了normalQueue中");
        }else if(user.getScore() < 3000) {
            synchronized (highQueue) {
                highQueue.offer(user);
                highQueue.notify();
            }
            System.out.println("把玩家" + user.getUsername() + "放到了highQueue中");
        }else {
            synchronized (veryHighQueue) {
                veryHighQueue.offer(user);
                veryHighQueue.notify();
            }
            System.out.println("把玩家" + user.getUsername() + "放到了veryHighQueue中");
        }
    }

    // 将玩家从匹配队列中删除
    // 也需要对各自的队列进行加锁
    public void remove(User user) {
        if(user.getScore() < 2000) {
            synchronized (normalQueue) {
                normalQueue.remove(user);
            }
            System.out.println("把玩家" + user.getUsername() + " 移除了normalQueue");
        }else if(user.getScore() < 3000) {
            synchronized (highQueue) {
                highQueue.remove(user);
            }
            System.out.println("把玩家" + user.getUsername() + " 移除了highQueue");
        }else {
            synchronized (veryHighQueue) {
                veryHighQueue.remove(user);
            }
            System.out.println("把玩家" + user.getUsername() + " 移除了veryHighQueue");
        }
    }

    public Matcher() {
        // 创建三个线程，分别对三个匹配队列进行轮询
        Thread t1 = new Thread() {
            @Override
            public void run() {
                // 扫描 normalQueue
                while(true) {
                    handlerMatch(normalQueue);
                }
            }
        };
        t1.start();

        Thread t2 = new Thread(() -> {
            while (true) {
                handlerMatch(highQueue);
            }
        });
        t2.start();

        Thread t3 = new Thread(() -> {
            while(true) {
                handlerMatch(veryHighQueue);
            }
        });
        t3.start();
    }

    private void handlerMatch(Queue<User> matchQueue) {
        synchronized (matchQueue) {
            try{
                // 当队列元素小于2个，要进行等待
                // 为了防止忙等使用wait,当有新玩家进入队列就notify
                // 使用while循环判断，因为如果原本队列0个玩家，加入一个玩家此时也不够两个玩家
                while(matchQueue.size() < 2) {
                    matchQueue.wait();
                }
                // 从队列中取出两个玩家
                User player1 = matchQueue.poll();
                User player2 = matchQueue.poll();
                System.out.println("匹配出两个玩家：" + player1.getUsername() + "," + player2.getUsername());
                // 获取到玩家websocket会话，用来告知玩家，匹配成功
                WebSocketSession session1 = onlineUserManager.getFromGameHall(player1.getUserId());
                WebSocketSession session2 = onlineUserManager.getFromGameHall(player2.getUserId());
                // 理论上来说，匹配队列中的玩家是在线的
                // 因为在前面的逻辑中，当玩家断开连接的时候就会把玩家从匹配队列中移除
                // 但是咱们进行double check
                if (session1 == null) {
                    // 将玩家二重新放回去
                    matchQueue.offer(player2);
                    return;
                }
                if (session2 == null) {
                    matchQueue.offer(player1);
                    return;
                }
                // 理论上不存在玩家1=玩家2
                // 因为玩家一下线就把玩家移除了匹配队列
                // 又禁止了多开情况
                // 但是double check
                if(session1 == session2) {
                    // 把其中一个玩家放回匹配队列
                    matchQueue.offer(player1);
                    return;
                }
                // 匹配成功后，把这两个玩家放到一个游戏房间中
                Room room = new Room();
                roomManager.add(room, player1.getUserId(), player2.getUserId());
                // 给玩家反馈：你匹配到对手
                //    通过 websocket 返回一个 message 为 'matchSuccess' 这样的响应
                //    此处是要给两个玩家都返回 "匹配成功" 这样的信息.
                //    因此就需要返回两次
                MatchResponse response1 = new MatchResponse();
                response1.setOk(true);
                response1.setMessage("matchSuccess");
                String json1 = objectMapper.writeValueAsString(response1);
                session1.sendMessage(new TextMessage(json1));

                MatchResponse response2 = new MatchResponse();
                response2.setOk(true);
                response2.setMessage("matchSuccess");
                String json2 = objectMapper.writeValueAsString(response2);
                session2.sendMessage(new TextMessage(json2));
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        }
    }
}
