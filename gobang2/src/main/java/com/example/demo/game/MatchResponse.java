package com.example.demo.game;

import lombok.Data;

/**
 * 匹配时返回的响应
 */
@Data
public class MatchResponse {
    private boolean ok;
    private String reason;
    private String message;
}
