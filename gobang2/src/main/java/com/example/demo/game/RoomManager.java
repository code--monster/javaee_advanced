package com.example.demo.game;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class RoomManager {
    // 存放房间信息
    private ConcurrentHashMap<String, Room> rooms = new ConcurrentHashMap<>();
    // 玩家与房间的联系
    private ConcurrentHashMap<Integer, String> userIdToRoomId = new ConcurrentHashMap<>();
    // 将玩家放到游戏房间中
    public void add(Room room, int userId1, int userId2) {
        rooms.put(room.getRoomId(), room);
        userIdToRoomId.put(userId1, room.getRoomId());
        userIdToRoomId.put(userId2, room.getRoomId());
    }
    // 将玩家从游戏房间中移除
    public void remove(String roomId, int userId1, int userId2) {
        rooms.remove(roomId);
        userIdToRoomId.remove(userId1);
        userIdToRoomId.remove(userId2);
    }
    // 通过房间id获取房间信息
    public Room getRoomByRoomId(String roomId) {
        return rooms.get(roomId);
    }
    // 通过玩家id获取房间信息
    public Room getRoomByUserId(int userId) {
        String roomId = userIdToRoomId.get(userId);
        if(roomId == null) {
            // 玩家不在房间内
            return null;
        }
        return rooms.get(roomId);
    }


}
