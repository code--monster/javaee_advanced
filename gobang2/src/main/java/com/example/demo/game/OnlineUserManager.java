package com.example.demo.game;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ConcurrentHashMap;

@Component
public class OnlineUserManager {
    // 表示用户在游戏大厅的在线状态
    // 同时有多个玩家进入游戏大厅，存在并发问题
    private ConcurrentHashMap<Integer, WebSocketSession> gameHall = new ConcurrentHashMap<>();
    // 表示当前用户在游戏房间的在线状态
    private ConcurrentHashMap<Integer, WebSocketSession> gameRoom = new ConcurrentHashMap<>();
    // 进入游戏大厅
    public void enterGameHall(int userId, WebSocketSession session) {
        gameHall.put(userId, session);
    }
    // 离开游戏大厅
    public void exitGameHall(int userId) {
        gameHall.remove(userId);
    }
    // 获取在游戏大厅时的玩家信息
    public WebSocketSession getFromGameHall(int userId) {
        return gameHall.get(userId);
    }
    // 进入游戏房间
    public void enterGameRoom(int userId, WebSocketSession session) {
        gameRoom.put(userId, session);
    }
    // 离开游戏房间
    public void exitGameRoom(int userId) {
        gameRoom.remove(userId);
    }
    // 获取游戏房间中的玩家信息
    public WebSocketSession getFromGameRoom(int userId) {
        return gameRoom.get(userId);
    }
}
