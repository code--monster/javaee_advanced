package com.example.demo.game;

import lombok.Data;

@Data
public class MatchRequest {
    private String message = "";
}
