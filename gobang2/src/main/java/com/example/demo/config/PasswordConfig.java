package com.example.demo.config;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.UUID;

public class PasswordConfig {
    /**
     * 加盐生成密码，用于注册
     */
    public static String encrypt(String password) {
        // 1. 生成盐值32位
        String salt = UUID.randomUUID().toString().replace("-", "");
        // 2. 生成加盐以后的密码
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        // 3. 生成最终的密码
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }

    /**
     * 2. 生成加盐的密码，用于登入
     * @param password 明文密码
     * @param salt 固定盐值
     * @return 最终密码
     */
    public static String encrypt(String password, String salt) {
        // 1. 生成一个加盐之后的密码
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        // 2. 生成约定格式的密码
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }

    /**
     * 3. 验证密码，用于登入
     * @param inputPassword
     * @param finalPassword
     * @return
     */
    public static boolean check(String inputPassword, String finalPassword) {
        if(StringUtils.hasLength(inputPassword) && StringUtils.hasLength(finalPassword) &&
            finalPassword.length() == 65)  {
            // 1. 得到盐值
            String salt = finalPassword.split("\\$")[0];
            // 2. 使用之前加密的步骤，将明文密码和得到的盐值，生成最终的密码
            String confirmPassword = PasswordConfig.encrypt(inputPassword, salt);
            // 3. 对比两个最终密码是否相同
            return confirmPassword.equals(finalPassword);
        }
        return false;
    }
}
