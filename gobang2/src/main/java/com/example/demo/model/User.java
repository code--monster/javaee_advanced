package com.example.demo.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class User implements Serializable { // 将对象存储到session需要持久化
    private int userId;
    private String username;
    private String password;
    private int score;
    private int totalCount;
    private int winCount;

}
