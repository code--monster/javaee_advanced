package com.example.demo.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Friend implements Serializable {
    private int userId;
    private int friendId;
    private String friendName;
    private int score;
    private int totalCount;
    private int winCount;

}
