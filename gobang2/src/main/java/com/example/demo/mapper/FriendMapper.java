package com.example.demo.mapper;

import com.example.demo.model.Friend;
import com.example.demo.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Set;

@Mapper
public interface FriendMapper {
    int addFriend(Integer playerId, Integer userId);

    User selectFriend(Integer userId);

    Set<Friend> selectAll(Integer userId);

    Integer updateMessage(User user);
}
