package com.example.demo.mapper;

import com.example.demo.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    // 往数据库插入一个用户，用于注册
    void insert(User user);

    // 根据用户名查询用户信息，用于登入
    User selectByName(String username);

    List<User> searchUsers(String username);

    // 胜者：总场数+1，获胜场数+1，天梯分+30
    void userWin(Integer userId);

    // 败者：总场数+1，获胜场数不变，天梯分数-30
    void userLose(Integer userId);

    User selectByUserId(Integer userId);

}
