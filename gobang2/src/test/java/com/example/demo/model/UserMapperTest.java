package com.example.demo.model;

import com.example.demo.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

class UserMapperTest {
    @Autowired
    private UserMapper userMapper;
    @Test
    void searchUsers() {
        List<User> list =  userMapper.searchUsers("zhang");
        for (User x : list) {
            System.out.println(x);
        }
    }
}