package com.example.demo.mapper;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@Transactional
class FriendMapperTest {
    @Autowired
    private FriendMapper friendMapper;
    @Test
    void addFriend() {
        int userId = 1;
        int playerId = 1;
        int result = friendMapper.addFriend(playerId, userId);
        System.out.println(result);
    }
}