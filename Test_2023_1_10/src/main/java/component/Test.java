package component;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
    public static void main1(String[] args) {
        //1.获取到Spring对象的两种方式
        //使用application得到spring上下文对象
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        //使用BeanFactory得到spring上下文对象
//        BeanFactory beanFactory =
//                new XmlBeanFactory(new ClassPathResource("spring-config.xml"));
        //2.从spring中取出bean对象的三种方法1
        //通过名称获取bean，可能出现getBean获得的对象为空
        //component.User user = (component.User) context.getBean("user");
        //通过bean类型获取bean
//        component.User user2 = context.getBean(component.User.class);
//        component.User user = context.getBean(component.User.class);
        //根据bean名称+bean类型来获取bean
        User user3 = context.getBean("user", User.class);
        System.out.println(user3.hello());
//        System.out.println(user.hello());
//        System.out.println(user2.hello());

//        ArticleController articleController = context.getBean("articleController", ArticleController.class);
//        System.out.println(articleController.say());

    }

    public static void main2(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        Student student = (Student) context.getBean("student");
        System.out.println(student);
    }

    public static void main3(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        User user = context.getBean("user", User.class);
        System.out.println(user.hello());
    }

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        User user = context.getBean("s1", User.class);
        System.out.println(user.hello());
    }
}
