package component;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class StudentBeans {
    @Bean
    public Student student() {
        Student stu = new Student();
        stu.setAge(10);
        stu.setId(11);
        stu.setName("张三");
        return stu;
    }
}
