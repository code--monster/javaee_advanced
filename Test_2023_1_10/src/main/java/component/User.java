package component;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;

@Controller
public class User {
    public String hello() {
        return "hello";
    }
    @Bean(name = {"s1"})
    public User getUser() {
        User user = new User();
        return user;
    }
}
