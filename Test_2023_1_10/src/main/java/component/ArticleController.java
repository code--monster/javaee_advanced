package component;

import org.springframework.stereotype.Controller;

@Controller
public class ArticleController {
    public String say() {
        return "hello,Controller";
    }
}
