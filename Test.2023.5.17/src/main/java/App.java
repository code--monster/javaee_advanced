import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class App {
    public static void main(String[] args) {
        //1. 得到Spring的上下文对象,需要写上Spring配置信息的名称
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");

        //2. 通过Spring对象，获取到Bean对象
        //根据id属性来取Bean对象
//        User user = (User) context.getBean("user");
        User user = context.getBean("user", User.class);
        //3. 使用Bean对象
        user.sayHi("张三");
    }
}
