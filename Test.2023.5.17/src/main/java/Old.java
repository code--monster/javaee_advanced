public class Old {
    static class Car{
        private FrameWork frameWork;
        public Car(int size) {
            frameWork = new FrameWork(size);
        }
        public void init() {
            System.out.println("执行了car的init方法");
            frameWork.init();
        }
    }
    static class FrameWork{
        private Bottom bottom;
        public FrameWork(int size) {
            bottom = new Bottom(size);
        }
        public void init() {
            System.out.println("执行了frameWork的init方法");
            bottom.init();
        }
    }
    static class Bottom{
        private Tire tire;
        public Bottom(int size) {
            tire = new Tire(size);
        }
        public void init() {
            System.out.println("执行了bottom的init方法");
            tire.init();
        }
    }
    static class Tire {
        public int size;

        //此时添加一个属性
//        public String color;
//        public Tire(int size, String color) {
//            this.size = size;
//        }
        public Tire(int size) {
            this.size = size;
        }
        public void init() {
            System.out.println("轮胎尺寸：" + size);
        }
    }
    public static void main(String[] args) {
        Car car = new Car(30);
        car.init();
    }
}
