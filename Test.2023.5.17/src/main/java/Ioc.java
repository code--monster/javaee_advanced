public class Ioc {
    static class Car{
        private FrameWork frameWork;
        public Car(FrameWork frameWork) {
            this.frameWork = frameWork;
        }
        public void init() {
            System.out.println("执行了car的init方法");
            frameWork.init();
        }
    }
    static class FrameWork{
        private Bottom bottom;
        public FrameWork(Bottom bottom) {
            this.bottom = bottom;
        }
        public void init() {
            System.out.println("执行了frameWork的init方法");
            bottom.init();
        }
    }
    static class Bottom{
        private Tire tire;
        public Bottom(Tire tire) {
            this.tire = tire;
        }
        public void init() {
            System.out.println("执行了bottom的init方法");
            tire.init();
        }
    }
    static class Tire {
        public int size;

        //此时添加一个属性
//        public String color;
//        public Tire(int size, String color) {
//            this.size = size;
//        }
        public Tire(int size) {
            this.size = size;
        }
        public void init() {
            System.out.println("轮胎尺寸：" + size);
        }
    }
    public static void main(String[] args) {
        Tire tire = new Tire(20);
        Bottom bottom = new Bottom(tire);
        FrameWork frameWork = new FrameWork(bottom);
        Car car = new Car(frameWork);
        car.init();
    }
}
