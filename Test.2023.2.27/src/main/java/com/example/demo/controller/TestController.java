package com.example.demo.controller;

import com.example.demo.modle.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RequestMapping("/test")
//@ResponseBody
//@Controller
@RestController
@Slf4j
public class TestController {
    @RequestMapping("/hi")
    public String sayHi() {
        return "hi spring mvc";
    }
    @RequestMapping("/name")
    public String sayHi(String name) {
        return "hi" + name;
    }
    @RequestMapping("/user")
    public String getUser(User user) {
        return user.toString();
    }
    @RequestMapping("/time")
    public String getTime(@RequestParam(value = "t1", required = false) String startTime,
                          @RequestParam("t2") String endTime) {
        return "开始时间: " + startTime + " 结束时间： " + endTime;
    }
    @RequestMapping("/jsonUser")
    public String getJson(@RequestBody User user) {
        return user.toString();
    }
    @RequestMapping("/login/{username}/and/{password}")
    public String login(@PathVariable("username") String username,
                        @PathVariable("password") String password) {
            return username + ": " + password;
    }
    @RequestMapping("/upfile")
    public String upFile(@RequestPart ("myfile")MultipartFile file) throws IOException {
        String path = "D:\\javaee_advanced\\Test.2023.2.27\\img.png";
        //保存文件
        file.transferTo(new File(path));
        return path;
    }
    @RequestMapping("/upfile2")
    public String upFile2(@RequestPart("myfile") MultipartFile file) throws IOException {
        //根目录
        String path = "D:\\javaee_advanced\\Test.2023.2.27\\";
        // 加上文件名
        path += UUID.randomUUID().toString().replace("-", "");
        //加上后缀
        path += file.getOriginalFilename().substring
                (file.getOriginalFilename().lastIndexOf("."));
        file.transferTo(new File(path));
        return path;
    }
    //spring mvc内置了servlet，
    //可以使用httpServletRequest和httpServletResponse
    @RequestMapping("/getck")
    public String getCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if(cookies == null) {
            return "no Cookie";
        }
        for (Cookie x :
                cookies) {
            log.error(x.getName() + ": " + x.getValue());
        }
        return "get Cookie";
    }
    //获取单个Cookie
    @RequestMapping("/getck2")
    public String getCookie2(@CookieValue("name") String val) {
        return "name:" + val;
    }
    //获取header信息
    @RequestMapping("/getHeader")
    public String getHeader(@RequestHeader("User-Agent") String userAgent) {
        return userAgent;
    }

    //session存储
    @RequestMapping("/setsess")
    public String setSession(HttpServletRequest request) {
        //如果不存在就新建
        HttpSession session = request.getSession(true);
        session.setAttribute("username", "lisi");
        return "set session success";
    }
    //session的获取
    @RequestMapping("/getsess")
    public String getSession(HttpServletRequest request) {
        //没有session不新建
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute("username") != null) {
            return (String) session.getAttribute("username");
        }else {
            return "没有session";
        }
    }
    @RequestMapping("/getsess2")
    public String getSession2(@SessionAttribute(value = "username", required = false)
                              String username) {
        return username;
    }

}
