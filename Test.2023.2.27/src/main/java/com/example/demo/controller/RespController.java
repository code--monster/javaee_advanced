package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;

@RequestMapping("/resp")
@Controller
public class RespController {
    @RequestMapping("/hi")
    public String sayHi() {
        return "/index.html";
    }
    @RequestMapping("/json")
    @ResponseBody
    public HashMap<String, String> respJson() {
        HashMap<String, String> map = new HashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");
        map.put("key3", "value3");
        return map;
    }

    @RequestMapping("index")
    public String index() {
        //请求重定向 redirect
        return "redirect:/index.html";
    }
    @RequestMapping("index2")
    public String index2() {
        //请求重发 forward
        return "forward:/index.html";
    }
}
