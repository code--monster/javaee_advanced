package com.example.demo.mapper;

import com.example.demo.entity.UserInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class UserMapperTest {
    @Autowired
    private UserMapper userMapper;
    @Test
    void getUserinfoById() {
        UserInfo userinfo = userMapper.getUserinfoById(1);
        Assertions.assertEquals("admin", userinfo.getUsername());
    }

    @Test
    void getAll() {
        List<UserInfo> userInfos = userMapper.getAll();
        System.out.println(userInfos);
    }

    @Test
    void add() {
        //伪代码
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("张三");
        userInfo.setPassword("12345");
        userInfo.setCreatetime(LocalDateTime.now());
        userInfo.setUpdatetime(LocalDateTime.now());
        int result = userMapper.add(userInfo);
        Assertions.assertEquals(1, result);
    }

    @Test
    void addGetId() {
        //伪代码
        UserInfo userInfo = new UserInfo();
        userInfo.setUsername("李四");
        userInfo.setPassword("12345");
        userInfo.setCreatetime(LocalDateTime.now());
        userInfo.setUpdatetime(LocalDateTime.now());
        int result = userMapper.addGetId(userInfo);
        Assertions.assertEquals(1, result);
        System.out.println(userInfo.getId());
    }

    @Test
    void upUserName() {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(2);
        userInfo.setUsername("王五");
        int result = userMapper.upUserName(userInfo);
        Assertions.assertEquals(1, result);

    }

    @Test
    void delById() {
        int result = userMapper.delById(2);
        Assertions.assertEquals(1, result);
    }

    @Test
    void getListByOrder() {
        List<UserInfo> list = userMapper.getListByOrder("desc");
        System.out.println(list);

    }
}