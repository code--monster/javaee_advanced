package com.example.demo.controller;

import com.example.demo.entity.UserInfo;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/test")
@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @RequestMapping("/getuserbyid")
    public UserInfo getUserinfoById(Integer id) {
        if(id == null) {
            return null;
        }
        return userService.getUserinfoById(id);
    }
}
