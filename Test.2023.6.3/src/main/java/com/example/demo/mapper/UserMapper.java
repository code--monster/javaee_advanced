package com.example.demo.mapper;
import com.example.demo.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    public UserInfo getUserinfoById(@Param("userId") Integer id);
    public List<UserInfo> getAll();
    int add(UserInfo userInfo);
    //添加并设置自增id
    int addGetId(UserInfo userInfo);
    int upUserName(UserInfo userInfo);
    int delById(@Param("id") Integer id);

    List<UserInfo> getListByOrder(@Param("order") String order);
}
