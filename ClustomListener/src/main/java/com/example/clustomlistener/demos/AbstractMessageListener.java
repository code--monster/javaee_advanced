package com.example.clustomlistener.demos;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;

import java.io.IOException;

// 自定义消息监听，可以编写公共消息应答方法
@Slf4j
public abstract class AbstractMessageListener implements ChannelAwareMessageListener {

    public abstract void receiveMessage(Message message);
    /*获取到队列消息执行抽象方法，抽象方法实际落地是 OrderMessageService 的 receiveMessage*/
    @Override
    public void onMessage(Message message, Channel channel) throws Exception {
        MessageProperties messageProperties = message.getMessageProperties();
        long deliveryTag = messageProperties.getDeliveryTag();
        log.info("收到消息{}：",message);

        try {
            receiveMessage(message);
            channel.basicAck(deliveryTag, false);
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            if(message.getBody().length > 100) {
                channel.basicReject(deliveryTag,false);
            }else {
                channel.basicNack(deliveryTag,false,true);
            }
        }
    }
}
