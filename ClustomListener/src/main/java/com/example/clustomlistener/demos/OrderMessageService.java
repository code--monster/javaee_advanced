package com.example.clustomlistener.demos;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderMessageService extends AbstractMessageListener{
    @Override
    public void receiveMessage(Message message) {
        log.info("OrderMessageService获取到消息:{}", new String(message.getBody()));
    }
}
