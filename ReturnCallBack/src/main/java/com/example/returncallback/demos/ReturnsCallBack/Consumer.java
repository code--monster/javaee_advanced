package com.example.returncallback.demos.ReturnsCallBack;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.nio.channels.Channel;

@Slf4j
@Component
public class Consumer {
    @RabbitListener(queues = RabbitMQConfig.QUEUE_NAME)
    public void consumerMessage(Message message, Channel channel) {
        System.out.println("获取到消息：" + new String(message.getBody()));
    }
}
