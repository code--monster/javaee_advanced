package com.example.returncallback.demos.ReturnsCallBack;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    @Autowired
    private RabbitProperties properties;
    public static final String EXCHANGE_NAME = "ReturnCallBack-Exchange";
    public static final String QUEUE_NAME = "ReturnCallBack-StudentA";
    public static final String Routing_Key = "ReturnCallBack-Key";
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory();
        cachingConnectionFactory.setHost(properties.getHost());
        cachingConnectionFactory.setPort(properties.getPort());
        cachingConnectionFactory.setUsername(properties.getUsername());
        cachingConnectionFactory.setPassword(properties.getPassword());
        cachingConnectionFactory.setVirtualHost(properties.getVirtualHost());
        cachingConnectionFactory.setPublisherReturns(properties.isPublisherReturns());
        return cachingConnectionFactory;
    }
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(EXCHANGE_NAME, false,false,  null);
    }
    @Bean
    public Queue queue() {
        return QueueBuilder.durable(QUEUE_NAME).build();
    }
    @Bean
    public Binding binding(@Qualifier("directExchange") DirectExchange directExchange,
                           @Qualifier("queue") Queue queue) {
        return BindingBuilder.bind(queue).to(directExchange).with(Routing_Key);
    }
}
