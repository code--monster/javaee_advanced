import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.List;

public class RedisDemoList {
    /**
     * lpush和lrange的使用
     * @param jedis
     */
    private static void test1(Jedis jedis) {
        System.out.println("lpush和lrange使用");
        jedis.flushAll();

        jedis.lpush("key", "111", "222", "333");

        List<String> result = jedis.lrange("key", 0, -1);
        System.out.println(result);
    }

    /**
     * rpush的使用
     * @param jedis
     */
    private static void test2(Jedis jedis) {
        System.out.println("rpush的使用");
        jedis.flushAll();

        jedis.rpush("key", "111", "222", "333");

        List<String> result = jedis.lrange("key", 0, -1);
        System.out.println("result:" + result);
    }

    /**
     * lpop和rpop的使用
     * @param jedis
     */
    private static void test3(Jedis jedis) {
        System.out.println("lpop和rpop");
        jedis.flushAll();

        jedis.rpush("key", "111", "222", "333");
        String result = jedis.lpop("key");
        System.out.println("result:" + result);
        String result2 = jedis.rpop("key");
        System.out.println("resul2:" + result2);
    }

    /**
     * blpop的使用
     * @param jedis
     */
    private static void test4(Jedis jedis) {
        System.out.println("blpop的使用");
        jedis.flushAll();
        // 返回结果是一个“二元组”，第一个是从哪个key对应的list中删除，第二个是删除元素是什么
        List<String> results = jedis.blpop(100, "key");
        // 此时会阻塞等待，通过linux打开redis-cli，往key中添加元素即可
        System.out.println("results[0]:" + results.get(0));
        System.out.println("results[1]:" + results.get(1));
    }

    /**
     * llen的使用
     * @param jedis
     */
    private static void test5(Jedis jedis) {
        System.out.println("llen");
        jedis.flushAll();
        jedis.rpush("key", "111", "222", "333");
        long llen = jedis.llen("key");
        System.out.println("len:" + llen);
    }
    public static void main(String[] args) {
        JedisPool jedisPool = new JedisPool("tcp://127.0.0.1:8888");
        try (Jedis jedis = jedisPool.getResource()){
            test1(jedis);
            test2(jedis);
            test3(jedis);
            test4(jedis);
            test5(jedis);
        }
    }
}
