import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RedisDemoHash {
    /**
     *
     * @param jedis
     */
    private static void test1(Jedis jedis) {
        System.out.println("hset和hget");
        jedis.flushAll();
        // 只添加一个value
        jedis.hset("key", "f1", "111");
        // 添加多个value
        Map<String, String> fields = new HashMap<>();
        fields.put("f2", "222");
        fields.put("f3", "333");
        jedis.hset("key", fields);

        String result = jedis.hget("key", "f1");
        System.out.println("result:" + result);

        result = jedis.hget("key", "f2");
        System.out.println("result:" + result);

        result = jedis.hget("key", "f100");
        System.out.println("result:" + result);
    }

    /**
     * hexists使用
     * @param jedis
     */
    private static void test2(Jedis jedis) {
        System.out.println("hexists使用");
        jedis.flushAll();

        Map<String, String> fields = new HashMap<>();
        fields.put("f1", "111");
        fields.put("f2", "222");
        fields.put("f3", "333");
        jedis.hset("key", fields);

        boolean result = jedis.hexists("key", "f1");
        System.out.println("result:" + result);

        result = jedis.hexists("key", "f100");
        System.out.println("reshult:" + result);
    }

    /**
     * hdel的使用
     * @param jedis
     */
    private static void test3(Jedis jedis) {
        System.out.println("hdel的使用");
        jedis.flushAll();

        jedis.hset("key", "f1", "111");
        jedis.hset("key", "f2", "222");

        long result = jedis.hdel("key", "f1", "f2");
        System.out.println("result:" + result);

        boolean exists = jedis.hexists("key", "f1");
        System.out.println("exists:" + exists);
        exists = jedis.hexists("key", "f2");
        System.out.println("exists:" + exists);
    }

    /**
     * hkeys和kvals的使用
     * @param jedis
     */
    private static void test4(Jedis jedis) {
        System.out.println("hkeys和hvals的使用");
        jedis.flushAll();

        jedis.hset("key", "f1", "111");
        jedis.hset("key", "f2", "222");
        jedis.hset("key", "f3", "333");

        Set<String> fields = jedis.hkeys("key");
        List<String> vals = jedis.hvals("key");
        System.out.println("fields:" + fields);
        System.out.println("vals:"+ vals);
    }

    /**
     * hmget和hmset的使用
     * @param jedis
     */
    private static void test5(Jedis jedis) {
        System.out.println("hmget和hmset的使用");
        jedis.flushAll();

        Map<String, String> fields = new HashMap<>();
        fields.put("f1", "111");
        fields.put("f2", "222");
        fields.put("f3", "333");
        jedis.hmset("key", fields);

        List<String> values = jedis.hmget("key", "f1", "f2", "f3");
        System.out.println("values:" + values);

    }
    public static void main(String[] args) {
        JedisPool jedisPool = new JedisPool("tcp://127.0.0.1:8888");
        try (Jedis jedis = jedisPool.getResource()){
            test1(jedis);
            test2(jedis);
            test3(jedis);
            test4(jedis);
            test5(jedis);
        }
    }
}
