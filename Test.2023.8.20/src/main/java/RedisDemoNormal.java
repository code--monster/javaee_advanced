import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.params.SetParams;

import java.util.Set;

public class RedisDemoNormal {
    /**
     * get和set的使用
     * @param jedis
     */
    private static void test1(Jedis jedis) {
        System.out.println("get和set的使用");
        //清空数据库，避免上一组测试的数据影响下一组测试的结果
        jedis.flushAll();
        jedis.set("key", "111");
        jedis.set("key2", "222");

        String value1 = jedis.get("key");
        String value2 = jedis.get("key2");
        System.out.println("value1=" + value1);
        System.out.println("value2=" + value2);
        // 设置参数
        SetParams params = new SetParams();
        params.ex(10);
        params.xx();
        jedis.set("key", "3333");
        String value3 = jedis.get("key");
        System.out.println("value3=" + value3);
    }

    /**
     * exists和del的使用
     * @param jedis
     */
    private static void test2(Jedis jedis) {
        System.out.println("exists和del的使用");
        jedis.flushAll();

        jedis.set("key", "111");
        jedis.set("key2", "222");
        jedis.set("key3", "333");

        boolean result = jedis.exists("key");
        System.out.println("result:" + result);

        long result2 = jedis.del("key");
        System.out.println("result2:" + result2);

        result = jedis.exists("key");
        System.out.println("result:" + result);

        result2 = jedis.del("key", "key2", "key3");
        System.out.println("result2:" + result2);
    }

    /**
     * keys的使用
     * @param jedis
     */
    private static void test3(Jedis jedis) {
        System.out.println("keys的使用");
        jedis.flushAll();

        jedis.set("key", "111");
        jedis.set("key2", "222");
        jedis.set("key3", "333");
        // redis中的key不能重复，并且没有顺序
        Set<String> keys = jedis.keys("*");
        System.out.println(keys);
    }

    /**
     * expire和ttl的使用
     * @param jedis
     */
    private static void test4(Jedis jedis) {
        System.out.println("expire和ttl的使用");
        jedis.flushAll();

        jedis.set("key", "111");
        jedis.expire("key", 10);
        long time = jedis.ttl("key");
        System.out.println("time:" + time);
    }

    /**
     * type的使用
     * @param jedis
     */
    private static void test5(Jedis jedis) {
        System.out.println("type的使用");
        jedis.flushAll();
        // string
        jedis.set("key", "111");
        String type = jedis.type("key");
        System.out.println("type:" + type);
        // list
        jedis.lpush("key2", "111", "222", "333");
        type = jedis.type("key2");
        System.out.println("type:" + type);
        //hash
        jedis.hset("key3", "f1", "111");
        type = jedis.type("key3");
        System.out.println("type:" + type);
        // set
        jedis.sadd("key4", "111", "222", "333");
        type = jedis.type("key4");
        System.out.println("type:" + type);
        // zset
        jedis.zadd("key5", 10, "zhangsan");
        type = jedis.type("key5");
        System.out.println("type:" + type);

    }
    public static void main(String[] args) {
        JedisPool jedisPool = new JedisPool("tcp://127.0.0.1:8888");
        try(Jedis jedis = jedisPool.getResource()) {
            test1(jedis);
            test2(jedis);
            test3(jedis);
            test4(jedis);
            test5(jedis);
        }
    }



    public static void main1(String[] args) {
        // 连接redis服务器，8888是我们配置的端口转发
        JedisPool jedisPool = new JedisPool("tcp://127.0.0.1:8888");
        //从redis连接池中获取一个连接，并且使用完要释放
        try(Jedis jedis = jedisPool.getResource()) {
//            String pong = jedis.ping();
//            System.out.println(pong);
        }
    }
}
