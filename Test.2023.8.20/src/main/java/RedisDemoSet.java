import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Set;

public class RedisDemoSet {
    /**
     * sadd和smembers的使用
     * @param jedis
     */
    private static void test1(Jedis jedis) {
        System.out.println("sadd和smembers的使用");
        jedis.flushAll();

        jedis.sadd("key", "111", "222", "333");
        Set<String> result = jedis.smembers("key");
        System.out.println("result:" + result);
    }

    /**
     * sismember和scard的使用
     * @param jedis
     */
    private static void test2(Jedis jedis) {
        System.out.println("sismember和scard的使用");
        jedis.flushAll();

        jedis.sadd("key", "111", "222", "333");
        boolean result = jedis.sismember("key", "100");
        System.out.println("result: " + result);

        long len = jedis.scard("key");
        System.out.println("len:" + len);
    }

    /**
     * spop使用
     * @param jedis
     */
    private static void test3(Jedis jedis) {
        System.out.println("spop使用");
        jedis.flushAll();

        jedis.sadd("key", "111", "222", "333");
        String result = jedis.spop("key");
        System.out.println("result:" + result);
    }

    /**
     * sinter的使用
     * @param jedis
     */
    private static void test4(Jedis jedis) {
        System.out.println("sinter的使用");
        jedis.flushAll();

        jedis.sadd("key", "111", "222", "333");
        jedis.sadd("key2", "222", "333", "444");

        Set<String> result = jedis.sinter("key", "key2");
        System.out.println("result:" + result);
    }

    /**
     * sinterstore的使用
     * @param jedis
     */
    private static void test5(Jedis jedis) {
        System.out.println("sinterstore使用");
        jedis.flushAll();

        jedis.sadd("key", "111", "222", "333");
        jedis.sadd("key2", "111", "333", "444");
        long len = jedis.sinterstore("key3", "key", "key2");
        System.out.println("len:" + len);

        Set<String> result = jedis.smembers("key3");
        System.out.println("result:" + result);
    }
    public static void main(String[] args) {
        JedisPool jedisPool = new JedisPool("tcp://127.0.0.1:8888");
        try (Jedis jedis = jedisPool.getResource()){
            test1(jedis);
            test2(jedis);
            test3(jedis);
            test4(jedis);
            test5(jedis);
        }
    }
}
