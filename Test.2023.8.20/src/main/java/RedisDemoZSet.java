import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.resps.Tuple;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RedisDemoZSet {
    /**
     * zadd和zrange的使用
     * @param jedis
     */
    private static void test1(Jedis jedis) {
        System.out.println("zadd和zrange的使用");
        jedis.flushAll();
        // 添加一个value
        jedis.zadd("key", 10, "zhangsan");
        // 添加多个value
        Map<String, Double> map = new HashMap<>();
        map.put("lisi", 20.0);
        map.put("wangwu", 30.0);
        jedis.zadd("key", map);

        List<String> members = jedis.zrange("key", 0, -1);
        System.out.println("members:" + members);
        // 获取zset的value要借助jedis封装的Tuple类型
        List<Tuple> membersWithScore = jedis.zrangeWithScores("key", 0, -1);
        System.out.println("membersWithScore:" + membersWithScore);
        String member = membersWithScore.get(0).getElement();
        double score = membersWithScore.get(0).getScore();
        System.out.println("member:" + member + ", score:" + score);
    }

    /**
     * zcard的使用
     * @param jedis
     */
    private static void test2(Jedis jedis) {
        System.out.println("zcard的使用");
        jedis.flushAll();

        Map<String, Double> map = new HashMap<>();
        map.put("zhangsan", 10.0);
        map.put("lisi", 20.0);
        map.put("wangwu", 30.0);

        jedis.zadd("key", map);
        long len = jedis.zcard("key");
        System.out.println("len:" + len);
    }

    /**
     * zrem的使用
     * @param jedis
     */
    private static void test3(Jedis jedis) {
        System.out.println("zrem的使用");
        jedis.flushAll();

        jedis.zadd("key", 10, "zhangsan");
        jedis.zadd("key", 20, "lisi");
        jedis.zadd("key", 30, "wangwu");

        long count = jedis.zrem("key", "zhangsan", "lisi");
        System.out.println("count:" + count);

        List<Tuple> result = jedis.zrangeWithScores("key", 0, -1);
        System.out.println("result:" + result);
    }

    /**
     * zscore的使用
     * @param jedis
     */
    private static void test4(Jedis jedis) {
        System.out.println("zscore的使用");
        jedis.flushAll();

        jedis.zadd("key", 10, "zhangsan");
        jedis.zadd("key", 20, "lisi");
        jedis.zadd("key", 30, "wangwu");

        Double score = jedis.zscore("key", "zhangsan");
        System.out.println("score: " + score);
    }

    /**
     * zrank的使用
     * @param jedis
     */
    private static void test5(Jedis jedis) {
        System.out.println("zrank的使用");
        jedis.flushAll();

        jedis.zadd("key", 10, "zhangsan");
        jedis.zadd("key", 20, "lisi");
        jedis.zadd("key", 30, "wangwu");

        Long rank = jedis.zrank("key", "zhangsan");
        System.out.println("rank:" + rank);

    }
    public static void main(String[] args) {
        JedisPool jedisPool = new JedisPool("tcp://127.0.0.1:8888");
        try (Jedis jedis = jedisPool.getResource()) {
            test1(jedis);
            test2(jedis);
            test3(jedis);
            test4(jedis);
            test5(jedis);
        }

    }
}
