package com.imooc.controller;

import com.aliyun.core.utils.StringUtils;
import com.imooc.base.BaseInfoProperties;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.imooc.grace.result.GraceJSONResult;
import com.imooc.grace.result.ResponseStatusEnum;
import com.imooc.bo.RegistLoginBO;
import com.imooc.pojo.Users;
import com.imooc.vo.UsersVO;
import com.imooc.service.SmsService;
import com.imooc.service.UserService;
import com.imooc.utils.RandomUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Api(tags = "PassportController 通信证接口模块")
@RequestMapping("passport")
@RestController
public class PassportController extends BaseInfoProperties {
    @Autowired
    private SmsService smsService;

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @PostMapping("getSMSCode")
    public GraceJSONResult getSmsCode(@RequestParam String mobile,
                                      HttpServletRequest request) throws JsonProcessingException {
        // 从redis获取验证码，如果获取到返回ok
        // key 手机号 value 验证码
        String code = redisTemplate.opsForValue().get(MOBILE_SMSCODE+ ":" + mobile);
        if(!StringUtils.isEmpty(code)) {
            return GraceJSONResult.ok();
        }
        // 如果从redis获取不到，生成验证码
        code = RandomUtil.getSixBitRandom();
        // 发送验证码
        boolean isSend = smsService.send(mobile, code);
        // 生成验证码放到redis中，并设置有效时间
        if (isSend) {
            redisTemplate.opsForValue().set(MOBILE_SMSCODE+ ":" + mobile, code, 30, TimeUnit.MINUTES);
            return GraceJSONResult.ok();
        }else {
            return GraceJSONResult.errorMsg("发送短信失败");
        }
    }

    @PostMapping("login")
    public GraceJSONResult login(@Valid @RequestBody RegistLoginBO registLoginBO,
//                                 BindingResult result,    // 对代码有侵入性
                                 HttpServletRequest request) throws Exception {
        String mobile = registLoginBO.getMobile();
        String code = registLoginBO.getSmsCode();
        // 1.从redis中获取验证码进行校验
        String redisCode = redisTemplate.opsForValue().get(MOBILE_SMSCODE + ":" + mobile);
        if(StringUtils.isBlank(redisCode) || !redisCode.equalsIgnoreCase(code)) {
            return GraceJSONResult.errorCustom(ResponseStatusEnum.SMS_CODE_ERROR);
        }
        // 2. 查询数据库，判断用户是否存在
        Users user = userService.queryMobileIsExist(mobile);
        if(user == null) {
            // 如果用户为空，表示没有注册过，需要注册信息入库
            user = userService.createUser(mobile);
        }
        // 3. 如果不为空，继续下面的业务，保存用户会话信息
        // 在分布式中使用token,无法使用session因为部署在不同服务器上协议，域名，端口可能不一样
        String uToken = UUID.randomUUID().toString();
        // 4. 将token存入Redis中
        redisTemplate.opsForValue().set(REDIS_USER_TOKEN + ":" + user.getId(), uToken);
        // 5. 用户登入/注册成功后，删除redis中的短信验证码
        redisTemplate.delete(MOBILE_SMSCODE + ":" + mobile);
        // 6. 返回用户信息，包含token令牌
        UsersVO usersVO = new UsersVO();
        BeanUtils.copyProperties(user, usersVO);
        usersVO.setUserToken(uToken);

        return GraceJSONResult.ok();
    }
    @PostMapping("logout")
    public GraceJSONResult logout(@RequestParam String userId,
                                  HttpServletRequest request) {
        // 后端只需要清除用户的token信息即可，前端也需要清除本地app中的用户信息和token
        redisTemplate.delete(REDIS_USER_TOKEN + ":" + userId);
        return GraceJSONResult.ok();
    }

}
