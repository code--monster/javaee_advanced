package com.imooc.controller;

import com.imooc.base.BaseInfoProperties;
import com.imooc.base.RabbitMQConfig;
import com.imooc.grace.result.GraceJSONResult;
import com.imooc.bo.CommentBO;
import com.imooc.mo.MessageMO;
import com.imooc.pojo.Comment;
import com.imooc.pojo.Vlog;
import com.imooc.vo.CommentVO;
import com.imooc.service.CommentService;
import com.imooc.service.MsgService;
import com.imooc.service.VlogService;
import com.imooc.utils.JsonUtils;
import com.imooc.enums.MessageEnum;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Api(tags = "CommentController 评论模块的接口")
@RequestMapping("comment")
@RestController
public class CommentController extends BaseInfoProperties {
    @Autowired
    private CommentService commentService;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private MsgService msgService;

    @Autowired
    private VlogService vlogService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostMapping("create")
    public GraceJSONResult create(@RequestBody @Valid CommentBO commentBO) {
        CommentVO commentVO = commentService.createComment(commentBO);
        return GraceJSONResult.ok(commentVO);
    }

    /**
     * 从redis中获取评论数
     * @param vlogId
     * @return
     */
    @GetMapping("counts")
    public GraceJSONResult counts(@RequestParam String vlogId) {
        String countsStr = redisTemplate.opsForValue().get(REDIS_VLOG_COMMENT_COUNTS + ":" + vlogId);

        if (StringUtils.isBlank(countsStr)) {
            countsStr = "0";
        }
        return GraceJSONResult.ok(Integer.valueOf(countsStr));
    }

    @GetMapping("list")
    public GraceJSONResult list(@RequestParam String vlogId,
                                @RequestParam(defaultValue = "") String userId,
                                @RequestParam Integer page,
                                @RequestParam Integer pageSize) {
        return GraceJSONResult.ok(
                commentService.queryVlogComments(
                        vlogId,
                        userId,
                        page,
                        pageSize));
    }

    @DeleteMapping("delete")
    public GraceJSONResult delete(@RequestParam String commentUserId,
                                  @RequestParam String commentId,
                                  @RequestParam String vlogId) {
        commentService.deleteComment(commentUserId, commentId, vlogId);

        return GraceJSONResult.ok();
    }


    @PostMapping("like")
    public GraceJSONResult like(@RequestParam String commentId,
                                @RequestParam String userId) {
        // 使用hash类型来统计评论的点赞数
        // 注意：使用上面的方式数据量大时会出现bigkey,可以对field域再次hash，进行hash路由
        redisTemplate.opsForHash().increment(REDIS_VLOG_COMMENT_LIKED_COUNTS, commentId, 1);
        // 统计谁点赞了该评论
        redisTemplate.opsForHash().put(REDIS_USER_LIKE_COMMENT, userId + ":" + commentId, "1");

        // 系统消息：谁点赞了评论
        Comment comment = commentService.getComment(commentId);
        Vlog vlog = vlogService.getVlog(comment.getVlogId());
        Map<String, String> msgContent = new HashMap<>();
        msgContent.put("vlogId", vlog.getId());
        msgContent.put("vlogCover", vlog.getCover());
        msgContent.put("commentId", commentId);

//        msgService.createMsg(userId, comment.getCommentUserId(),
//                MessageEnum.LIKE_COMMENT.type, msgContent);

        // mq异步解耦
        MessageMO messageMO = new MessageMO();
        messageMO.setFromUserId(userId);
        messageMO.setToUserId(comment.getCommentUserId());
        messageMO.setMsgContent(msgContent);
        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE_MSG,
                "sys.msg." + MessageEnum.LIKE_COMMENT.enValue,
                JsonUtils.objectToJson(messageMO));
        return GraceJSONResult.ok();
    }



    @PostMapping("unlike")
    public GraceJSONResult unlike(@RequestParam String commentId,
                                  @RequestParam String userId) {

        redisTemplate.opsForHash().increment(REDIS_VLOG_COMMENT_LIKED_COUNTS, commentId, -1);
        redisTemplate.opsForHash().delete(REDIS_USER_LIKE_COMMENT, userId + ":" + commentId);

        return GraceJSONResult.ok();
    }

}
