package com.imooc.service;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface SmsService {
    // 发送手机验证码
    boolean send(String phone, String code) throws JsonProcessingException;
}
