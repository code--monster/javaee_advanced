package com.imooc.service;

import com.imooc.bo.VlogBO;
import com.imooc.pojo.Vlog;
import com.imooc.vo.IndexVlogVO;
import com.imooc.utils.PagedGridResult;

public interface VlogService {
    /**
     * 新增vlog视频
     * @param vlogBO
     */
    public void createVlog(VlogBO vlogBO);

    /**
     * 查询首页/搜索vlog列表
     */
    public PagedGridResult getIndexVlogList(String userId,
                                            String search,
                                            Integer page,
                                            Integer pageSize);

    /**
     * 根据视频id查询vlog
     * @param userId
     * @param vlogId
     * @return
     */
    public IndexVlogVO getVlogDetailById(String userId, String vlogId);

    /**
     * 用户把视频改为公开/私密
     * @param userId
     * @param vlogId
     * @param yesOrNo
     */
    public void changeToPrivateOrPublic(String userId, String vlogId, Integer yesOrNo);

    /**
     * 查询用的公开/私密的视频列表
     * @param userId
     * @param page
     * @param pageSize
     * @param yesOrNo
     * @return
     */
    public PagedGridResult queryMyVlogList(String userId,
                                           Integer page,
                                           Integer pageSize,
                                           Integer yesOrNo);

    /**
     * 用户点赞/喜欢的视频
     * @param userId
     * @param vlogId
     */
    public void userLikeVlog(String userId, String vlogId);

    /**
     * 根据主键查询vlog
     * @param id
     * @return
     */
    Vlog getVlog(String id);

    /**
     * 用户取消点赞/喜欢的视频
     * @param userId
     * @param vlogId
     */
    public void userUnLikeVlog(String userId, String vlogId);

    /**
     * 获得视频被点赞的总数
     * @param vlogId
     * @return
     */
    public Integer getVlogBeLikedCounts(String vlogId);

    /**
     * 查询用户点赞过的短视频
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    public PagedGridResult getMyLikedVlogList(String userId,
                                              Integer page,
                                              Integer pageSize);

    /**
     * 查询用户关注的博主发布的短视频列表
     * @param myId
     * @param page
     * @param pageSize
     * @return
     */
    public PagedGridResult getMyFollowVlogList(String myId,
                                               Integer page,
                                               Integer pageSize);

    /**
     * 查询朋友发布的短视频列表
     */
    public PagedGridResult getMyFriendVlogList(String myId, Integer page, Integer pageSize);

    /**
     * 达到阈值刷新到数据库中
     * @param vlogId
     * @param counts
     */
    public void flushCounts(String vlogId, Integer counts);

}
