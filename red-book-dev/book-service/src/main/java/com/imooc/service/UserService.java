package com.imooc.service;

import com.imooc.bo.UpdatedUserBO;
import com.imooc.pojo.Users;

public interface UserService {
    /**
     * 判断用户是否存在，如果存在则返回用户消息
     */
    public Users queryMobileIsExist(String mobile);

    /**
     * 创建用户信息，并返回用户对象
     */
    public Users createUser(String mobile);

    /**
     * 获取用户信息
     * @param userId
     * @return
     */
    public Users getUser(String userId);

    /**
     * 用户信息修改
     */
    public Users updateUserInfo(UpdatedUserBO updatedUserBO);

    /**
     * 用户信息修改
     */
    public Users updateUserInfo(UpdatedUserBO updatedUserBO, Integer type);
}
