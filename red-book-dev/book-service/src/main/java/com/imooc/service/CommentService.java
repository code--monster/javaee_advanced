package com.imooc.service;

import com.imooc.bo.CommentBO;
import com.imooc.pojo.Comment;
import com.imooc.vo.CommentVO;
import com.imooc.utils.PagedGridResult;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentService {
    /**
     * 发表评论
     * @param commentBO
     * @return
     */
    public CommentVO createComment(CommentBO commentBO);

    /**
     * 查询评论列表
     * @param vlogId
     * @param userId
     * @param page
     * @param pageSize
     * @return
     */
    public PagedGridResult queryVlogComments(String vlogId, String userId,
                                             Integer page, Integer pageSize);


    /**
     * 删除评论
     * @param commentUserId
     * @param commentId
     * @param vlogId
     */
    public void deleteComment(String commentUserId,
                              String commentId,
                              String vlogId);

    /**
     * 根据主键查询comment
     * @param id
     * @return
     */
    public Comment getComment(String id);
}
