package com.imooc.service;

import com.imooc.utils.PagedGridResult;
import org.springframework.stereotype.Repository;

@Repository
public interface FansService {
    /**
     * 关注博主
     * @param myId 自己的id
     * @param vlogerId 博主id
     */
    public void doFollow(String myId, String vlogerId);

    /**
     * 取关
     * @param myId
     * @param vlogerId
     */
    public void doCancel(String myId, String vlogerId);

    /**
     * 查看用户是否关注博主
     * @param myId
     * @param vlogerId
     * @return
     */
    public boolean queryDoIFollowVloger(String myId, String vlogerId);

    /**
     * 查看我关注的博主列表
     * @param myId
     * @param page
     * @param pageSize
     * @return
     */
    public PagedGridResult queryMyFollows(String myId,
                                          Integer page,
                                          Integer pageSize);

    /**
     * 查询我的粉丝列表
     * @param myId
     * @param page
     * @param pageSize
     * @return
     */
    public PagedGridResult queryMyFans(String myId,
                                       Integer page,
                                       Integer pageSize);
}
