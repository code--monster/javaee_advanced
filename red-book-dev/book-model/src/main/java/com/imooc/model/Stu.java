package com.imooc.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor // 全参构造方法
@NoArgsConstructor // 无参构造方法
public class Stu {
    private String name;
    private Integer age;
}
