package com.imooc.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Data
@PropertySource("classpath:aliyun.properties")  // 配置文件的扫描位置
@ConfigurationProperties(prefix = "aliyun.sms") // 前缀
public class AliyunProperties {
    private String regionId;
    private String accessKeyId;
    private String accessKeySecret;
}
