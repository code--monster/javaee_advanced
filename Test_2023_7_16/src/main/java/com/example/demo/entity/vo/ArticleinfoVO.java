package com.example.demo.entity.vo;

import com.example.demo.entity.Articleinfo;
import lombok.Data;

@Data
public class ArticleinfoVO extends Articleinfo {
    private String username;

    @Override
    public String toString() {
        return "ArticleinfoVO{" +
                "username='" + username + '\'' +
                "} " + super.toString();
    }
}
