package com.example.demo.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Articleinfo {
    private int id;
    private String title;
    private String content;
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private int state;
}
