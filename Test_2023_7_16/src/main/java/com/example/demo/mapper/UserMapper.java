package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.catalina.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper {
    int dels(List<Integer> list);

    int update(Userinfo userinfo);

    List<Userinfo> selectByParam(@Param("username") String username,
                           @Param("password") String password);

    int addUser2(Userinfo userinfo);

    int addUser(Userinfo userinfo);
    Userinfo selectById2(@Param("id") Integer id);

    Userinfo selectById(@Param("id") Integer id);

    Userinfo selectByKeyword(@Param("username") String username);

    List<Userinfo> login(@Param("username") String username,
                         @Param("password") String password);

    Userinfo selectByName(@Param("username") String username);

    int delById(@Param("id") Integer id);

    int upUserName(Userinfo userinfo);

    public int addGetId(Userinfo userinfo);

    public int  add(Userinfo userinfo);

}
