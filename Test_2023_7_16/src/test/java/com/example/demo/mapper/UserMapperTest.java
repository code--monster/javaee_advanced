package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@Transactional // 添加事务，回滚保证测试代码不入侵到数据库和业务代码中
@SpringBootTest //表示当前单元测试是运行在spring boot项目上
class UserMapperTest {
    @Autowired
    private UserMapper userMapper;
//    @Test
//    void add() {
//        Userinfo userinfo = new Userinfo();
//        userinfo.setUsername("张三");
//        userinfo.setPassword("12345");
//        userinfo.setPhoto("111");
//        userinfo.setState(1);
//        Assertions.assertEquals(1, userMapper.add(userinfo));
//    }
//
//    @Test
//    void addGetId() {
//        Userinfo userinfo = new Userinfo();
//        userinfo.setUsername("张三");
//        userinfo.setPassword("12345");
//        userinfo.setPhoto("111");
//        userinfo.setState(1);
//        Assertions.assertEquals(1, userMapper.addGetId(userinfo));
//        System.out.println("id:" + userinfo.getId());
//    }
//
//    @Test
//    void upUserName() {
//        Userinfo userinfo = new Userinfo();
//        userinfo.setUsername("张三");
//        userinfo.setId(1);
//        Assertions.assertEquals(1, userMapper.upUserName(userinfo));
//        System.out.println("修改后的姓名：" + userinfo.getUsername());
//    }

    @Test
    void delById() {
        Assertions.assertEquals(1,userMapper.delById(1));
    }

    @Test
    void selectByName() {
        String name = "admin";
        Userinfo userinfo = userMapper.selectByName(name);
        System.out.println("用户信息：" + userinfo);
    }

    @Test
    void login() {
        String username = "admin";
        String password = "' or 1='1"; //sql注入
        List<Userinfo> list = userMapper.login(username, password);
        System.out.println("用户信息：" + list);
    }

    @Test
    void selectByKeyword() {
        String keyWord = "ad";
        Userinfo userinfo = userMapper.selectByKeyword(keyWord);
        Assertions.assertEquals(userinfo.getId(), 1);
    }

//    @Test
//    void selectById() {
//        int id = 1;
//        Userinfo userinfo = userMapper.selectById(id);
//        System.out.println("用户名：" + userinfo.getName());
//    }
//
//    @Test
//    void selectById2() {
//        int id = 1;
//        Userinfo userinfo = userMapper.selectById(id);
//        System.out.println("用户名：" + userinfo.getName());
//    }

    @Test
    void addUser() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("张三");
        userinfo.setPassword("2345");
        userinfo.setPhoto(null); //在sql中”“不等于Null
        userMapper.addUser(userinfo);
        System.out.println(userinfo);
    }

    @Test
    void addUser2() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("王五");
        userinfo.setPassword("45667");
        userinfo.setPhoto(null);
        userMapper.addUser2(userinfo);
        System.out.println(userinfo);
    }

    @Test
    void selectByParam() {
        List<Userinfo> list = userMapper.selectByParam(null, "admin");
        System.out.println(list);
    }

    @Test
    void update() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("张三");
        userinfo.setPassword(null);
        userinfo.setPhoto("bbb.png");
        userinfo.setId(1);
        userMapper.update(userinfo);
        System.out.println(userinfo);
    }

    @Test
    void dels() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(3);
        int dels = userMapper.dels(list);
        Assertions.assertEquals(2, dels);
    }
}