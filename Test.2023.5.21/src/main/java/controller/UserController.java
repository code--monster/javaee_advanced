package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import service.User;
import service.UserService;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

@Controller
public class UserController {
    //属性注入
//    @Autowired
//    private UserService userService;


    //setter注入
//    private final UserService userService;
//    @Autowired
//    public void setUserService(UserService userService) {
//        this.userService = userService;
//    }


    //构造方法注入
//    private final UserService userService;
//    @Autowired
//    public UserController(UserService userService) {
//        this.userService = userService;
//    }

//    @Resource(name = "getUser1")
//    private User user;

//    @Autowired
//    @Qualifier(value = "getUser1")
//    private User user;
//
//    public User getUser() {
//        return user;
//    }


    @Resource
    private UserService service;

    @PostConstruct
    public void postConstruct() {
        service.sayHi();
        System.out.println("执行了UserController的构造方法");
    }

}
