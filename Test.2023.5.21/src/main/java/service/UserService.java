package service;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class UserService {
//    @Bean
//    public User getUser1() {
//        //伪代码
//        User user = new User();
//        user.setId(1);
//        user.setName("张三");
//        return user;
//    }
//    @Bean
//    public User getUser2() {
//        //伪代码
//        User user = new User();
//        user.setId(2);
//        user.setName("李四");
//        return user;
//    }

    public UserService() {
        System.out.println("执行了UserService的构造方法");
    }
    public void sayHi() {
        System.out.println("hello UserService");
    }
}
