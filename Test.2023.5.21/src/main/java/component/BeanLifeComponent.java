package component;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
@Component
public class BeanLifeComponent implements BeanNameAware {
    //通知
    @Override
    public void setBeanName(String s) {
        System.out.println("执行了通知");
    }
    //进行初始化工作
    @PostConstruct
    public void postConstruct() {
        System.out.println("执行了PostConstruct");
    }

    public void init() {
        System.out.println("执行了init-method");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("执行了销毁方法： preDestroy");
    }
}
