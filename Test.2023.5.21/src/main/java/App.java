import component.BeanLifeComponent;
import controller.UserController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
//    public static void main1 (String[] args) {
//        ApplicationContext context =
//                new ClassPathXmlApplicationContext("spring-config.xml");
//        UserController controller = context.getBean("userController", UserController.class);
//        User user = controller.getUser();
//        System.out.println(user.toString());
//    }

//    public static void main(String[] args) {
//        ClassPathXmlApplicationContext context =
//                new ClassPathXmlApplicationContext("spring-config.xml");
//        BeanLifeComponent bean = context.getBean(BeanLifeComponent.class);
//        System.out.println("使用Bean");
//        bean.preDestroy();
//    }

    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        UserController userController = context.getBean("userController", UserController.class);
    }

}
