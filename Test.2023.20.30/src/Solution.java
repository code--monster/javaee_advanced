class Solution {
    public  static String longestCommonPrefix(String[] strs) {
        // 统一比较
        int n = strs.length;
        StringBuilder sb = new StringBuilder();
        try{
            int i = 0;
            while(true) {
                int j = 0;
                for(; j < n - 1; j++) { // 比较每一行
                    // 不相等情况
                    if(strs[j].charAt(i) != strs[j + 1].charAt(i)) {
                        return sb.toString();
                    }
                }
                sb.append(strs[j].charAt(i));
                i++;
            }
        }catch(Exception e) {
            // 出现数组越界
            return sb.toString();
        }
    }


    public static String longestPalindrome(String s) {
        // 利用回文串的特点，中心扩展算法，时间复杂度O(N^2)
        String result = "";
        int begin = 0, len = 0, n = s.length();
        for(int i = 0; i < n; i++) {
            // 计算奇数情况
            int left = i, right = i;
            while(left >= 0 && right < n && s.charAt(left) == s.charAt(right)) {
                left--;
                right++;
            }
            if(len < right - left - 1) {
                begin = left + 1;
                len = right - left - 1;
            }
            // 计算偶数情况
            left = i;
            right = i + 1;
            while(left >= 0 && right < n && s.charAt(left) == s.charAt(right)) {
                left--;
                right++;
            }
            if(len < right - left - 1) {
                begin = left + 1;
                len = right - left - 1;
            }
        }
        return s.substring(begin, begin + len);

    }



    public static String addBinary(String a, String b) {
        // 模拟
        int len1 = a.length() - 1;
        int len2 = b.length() - 1;
        int tmp = 0; // 记录进位数
        StringBuilder sb = new StringBuilder();
        while(len1 >= 0 || len2 >= 0) {
            if(len1 < 0 || len2 < 0) {
                tmp += 0;
            }
            if(len1 >= 0) {
                tmp += Integer.parseInt(String.valueOf(a.charAt(len1))) ;
                len1--;
            }
            if(len2 >= 0) {
                tmp += Integer.parseInt(String.valueOf(b.charAt(len2)));
                len2--;
            }
            sb.append(tmp%2);
            tmp /= 2;
        }
        if(tmp > 0) {
            sb.append(tmp);
        }
        sb.reverse();
        return sb.toString();
    }

    public static void main(String[] args) {
        String a = "11";
        String b = "1";
        System.out.println(addBinary(a, b));
    }
    public static void main1(String[] args) {
        String[] strs = {"flower","flow","flight"};
        System.out.println(longestCommonPrefix(strs));
    }

    public static void main2(String[] args) {
        String str = "babad";
        System.out.println(longestPalindrome(str));
    }
}
