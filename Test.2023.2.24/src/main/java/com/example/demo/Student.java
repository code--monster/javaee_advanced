package com.example.demo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("student")//使用此注解读取yml中的对象
@Data//实体类的属性名要和配置中的Key一致，并且提供getter和setter才能放到Student对象中
public class Student {
    private int id;
    private String name;
    private int age;
}
