package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import java.util.List;

@Controller
@ResponseBody//只返回文字不返回页面
//中文乱码问题（貌似无效）使用yml
@PropertySource(value = "application.properties", encoding = "utf8")
public class TestController {
    //properties
    @Value("${mytest}")
    private String mytest;
    //yml和properties读取配置的方式相同
    @Value("${mytest2}")
    private String mytest2;

    @Value("${myString}")
    private String myString;

    @Value("${myString2}")
    private String myString2;

    @Value("${myString3}")
    private String myString3;

    @Autowired
    private Student student;

    @Autowired
    private MyList myList;

    @PostConstruct
    public void postConstruct() {
        System.out.println(myString);
        System.out.println(myString2);
        System.out.println(myString3);
        System.out.println(student);
        System.out.println(myList);
    }


    @RequestMapping("/getconf")
    public String getConfig() {
        return "mytest:" + mytest + " | "
                + "mytest2" + mytest2;
    }

    @RequestMapping("/hi")
    public String say(String name) {
        if(!StringUtils.hasLength(name)) {
            name = "张三";
        }
        return "你好" + name;
    }
}
