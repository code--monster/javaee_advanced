package com.example.demo.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Component
@ResponseBody
@RequestMapping("/user2")
@Slf4j
public class User2Controller {
    @RequestMapping("/hi")
    public String sayHi() {
        log.warn("slf4j的warn级别");
        log.error("slf4j的error级别");
        return "hi spring boot twice";
    }
}
