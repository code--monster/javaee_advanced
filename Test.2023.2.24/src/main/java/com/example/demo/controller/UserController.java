package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
@ResponseBody//当前类中所有方法返回的全是数据而非画面
public class UserController {
    //得到日志对象
    private static final Logger logger =
            LoggerFactory.getLogger(UserController.class);
    @RequestMapping("/hi")
    public String sayHi() {
        logger.trace("trace级别");
        logger.debug("debug级别");
        logger.info("info级别");//默认级别
        logger.warn("warn级别");
        logger.error("error级别");
        return "Hi spring boot";
    }

}
