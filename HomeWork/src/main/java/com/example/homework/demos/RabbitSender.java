package com.example.homework.demos;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

@RestController
public class RabbitSender {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @RequestMapping("send/{message}")
    private void send(@PathVariable("message") String message) {
        MessageProperties properties = new MessageProperties();
        properties.setAppId(UUID.randomUUID().toString());
        properties.setContentEncoding("UTF-8");
        properties.setContentType("application/text");

        Message msg = new Message(message.getBytes(StandardCharsets.UTF_8));

        // 设置回调消息
        ReturnedMessage reMsg = new ReturnedMessage(msg,
                200, "这是send传递的消息",
                "SchoolExchange", "homework.match");
        CorrelationData correlationData = new CorrelationData();
        correlationData.setReturned(reMsg);
        rabbitTemplate.convertAndSend("SchoolExchange", "homework.match", msg, correlationData);
    }

}
