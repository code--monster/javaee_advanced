package com.example.homework.demos;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

// 不可路由消息处理
@Component
@Slf4j
public class MyBack implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnsCallback {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private ConnectionFactory connectionFactory;
    // 将实现类注入
    // 注入顺序，Constructor -> @Autowired -> @PostConstruct
    @PostConstruct
    public void init() {
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnsCallback(this);
        rabbitTemplate.setConnectionFactory(connectionFactory);
    }
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        String id = correlationData.getId() != null ? correlationData.getId() : "";
        if(ack) {
            log.info("{}交换机接收到消息，消息id为{}",correlationData.getReturned().getExchange(),id);
        }else {
            log.warn("{}交换机接收到id为{}的消息失败，原因是：{}",correlationData.getReturned().getExchange(),id,cause);
        }

    }

    @Override
    public void returnedMessage(ReturnedMessage returnedMessage) {
        log.warn("不可路由消息响应码：" + returnedMessage.getReplyCode());
        log.warn("不可路由消息主体 message：" + returnedMessage.getMessage());
        log.warn("不可路由消息描述："+returnedMessage.getReplyText());
        log.warn("不可路由消息使用的交换器 exchange : "+ returnedMessage.getMessage());
        log.warn("不可路由消息使用的路由键 routiAng : "+ returnedMessage.getRoutingKey());
    }
}
