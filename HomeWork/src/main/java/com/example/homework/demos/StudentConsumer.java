package com.example.homework.demos;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
public class StudentConsumer {
    @RabbitListener(
            containerFactory = "rabbitListenerContainerFactory",
            bindings = @QueueBinding(
                    value = @Queue(value = "StudentQueue",durable = "true"),
                    exchange = @Exchange(value = "SchoolExchange",durable = "true",type = "topic"),
                    key = "homework.*"
            )
    )
    public void mathMessage(Message message, Channel channel) throws IOException {
        log.info("获取到的作业消息：" + new String(message.getBody()));
        // 消息应答
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        channel.basicNack(deliveryTag, false,false);
    }

}
