package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.security.PublicKey;
import java.util.List;

@RestController
public class Controller {
//    @Value("${name}")
//    private String name;
//    @PostConstruct
//    public void sayHi() {
//        System.out.println("hi： " + name);
//    }
//
//    @Value("${string.value}")
//    private String hello;
//    @PostConstruct
//    public void postConstruct() {
//        System.out.println(hello);
//    }
//    @Value("${boolean.value}")
//    private boolean bool;
//    @PostConstruct
//    public void postConstruct2() {
//        System.out.println(bool);
//    }
//    @Value("${null.value}")
//    private Integer integer;
//    @PostConstruct
//    public void postConstruct3() {
//        System.out.println(integer);
//    }

//    @Value("${string.str1}")
//    private String str1;
//    @PostConstruct
//    public void construct1() {
//        System.out.println("str1: " + str1);
//    }
//    @Value("${string.str2}")
//    private String str2;
//    @PostConstruct
//    public void construct2() {
//        System.out.println("str2: " + str2);
//    }
//    @Value("${string.str3}")
//    private String str3;
//    @PostConstruct
//    public void construct3() {
//        System.out.println("str3: " + str3);
//    }

//    @Resource
//    private Student student;
//    @PostConstruct
//    public void postConstruct() {
//        System.out.println(student);
//    }

    @Autowired
    private MyList myList;
    @PostConstruct
    public void postConstruct() {
        System.out.println(myList);
    }
}
