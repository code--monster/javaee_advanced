package com.example.demo;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
//三种写法
//@ConfigurationProperties(value = "student")
//@ConfigurationProperties(prefix = "student")
@ConfigurationProperties("student")
@Data //需要提供get,set方法才能够把配置文件的信息读取出来
public class Student {
    //类型和名字要一一对应
    private int id;
    private String name;
    private int age;
}
