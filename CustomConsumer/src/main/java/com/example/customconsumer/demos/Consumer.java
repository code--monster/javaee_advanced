package com.example.customconsumer.demos;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;

public class Consumer {
    public static final String QUEUE_NAME = "MyQueue";
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitUtils.getConnection();
        Channel channel = connection.createChannel();
        channel.basicConsume(QUEUE_NAME, new CustomConsumer(channel));
    }
}
