package com.example.customconsumer.demos;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import java.io.IOException;

/**
 * 自定义消费者逻辑
 */
public class CustomConsumer extends DefaultConsumer {

    public CustomConsumer(Channel channel) {
        super(channel);
    }

    @Override
    public void handleCancel(String consumerTag) throws IOException {
        System.out.println("消费者取消消费");
    }

    /**
     * 正常消费消息
     * @param consumerTag 消费者标签
     * @param envelope 与消息应答有关
     * @param properties 消息内容配置
     * @param body 消息内容
     * @throws IOException
     */
    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        System.out.println("消费者接收到的consumerTag为: "+consumerTag);
        System.out.println("消费者接收到的envelope为: "+envelope);
        System.out.println("消费者接收到的信息配置为: "+properties);
        System.out.println("消费者接收到的信息为: "+new String(body));
        super.getChannel().basicAck(envelope.getDeliveryTag(),  false); //手动应答
    }
}
