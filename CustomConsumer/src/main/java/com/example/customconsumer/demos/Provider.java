package com.example.customconsumer.demos;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class Provider {
    public static final String QUEUE_NAME = "MyQueue";
    public static final String EXCHANGE_NAME = "MyExchange";

    public static void main(String[] args) throws IOException {
        Connection connection = RabbitUtils.getConnection();
        assert connection != null;
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "hello");
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            String message = scanner.next();
            AMQP.BasicProperties properties = new AMQP.BasicProperties().builder()
                    .appId("12345")
                    .contentType("application/text")
                    .build();
            channel.basicPublish(EXCHANGE_NAME,"hello",properties ,message.getBytes(StandardCharsets.UTF_8));      //发送消息并将消息持久化到磁盘
            System.out.println("消息发送完成:" + message);
        }
    }
}
