package com.example.deadmessage;

import com.example.rabbitmqqoslimiting.demos.RabbitUtils;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Provider {
    private static final String EXCHANGE_NAME = "normal_exchange";
    private static final String KEY = "zhangsan";

    public static void main(String[] args) throws IOException {
        Connection connection = RabbitUtils.getConnection();
        assert connection != null;
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        // 死信队列，设置ttl消息过期时间，单位毫秒
        AMQP.BasicProperties basicProperties = new AMQP.BasicProperties()
                .builder()
                .expiration("10000")
                .build();
        // 模拟消息循环发送
        for (int i = 0; i < 11; i++) {
            String message = "info" + i;
            channel.basicPublish(EXCHANGE_NAME, KEY, basicProperties, message.getBytes(StandardCharsets.UTF_8));
        }
    }

}
