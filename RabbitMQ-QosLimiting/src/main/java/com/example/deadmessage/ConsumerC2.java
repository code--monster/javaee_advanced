package com.example.deadmessage;

import com.example.rabbitmqqoslimiting.demos.RabbitUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

public class ConsumerC2 {
    private static final String DEAD_EXCHANGE_NAME = "dead_exchange";
    private static final String DEAD_KEY = "lisi";
    private static final String DEAD_QUEUE_NAME = "dead-queue";
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitUtils.getConnection();
        Channel channel = connection.createChannel();

        // 声明交换机，队列，绑定，前面已经声明过可以不声明
        channel.exchangeDeclare(DEAD_EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        channel.queueDeclare(DEAD_QUEUE_NAME, false, false, false, null);
        channel.queueBind(DEAD_QUEUE_NAME, DEAD_EXCHANGE_NAME, DEAD_KEY);

        DeliverCallback successBack = (consumerTag, message) -> {
            System.out.println("C2用户收到的信息为：" + new String(message.getBody()));
        };

        CancelCallback cancelCallback = s -> {
            System.out.println("C2用户取消消费操作");
        };

        channel.basicConsume(DEAD_QUEUE_NAME, true, successBack, cancelCallback);
    }
}
