package com.example.deadmessage;

import com.example.rabbitmqqoslimiting.demos.RabbitUtils;
import com.rabbitmq.client.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class ConsumerC1 {
    private static final String EXCHANGE_NAME = "normal_exchange";              //正常交换机名称
    private static final String DEAD_EXCHANGE_NAME = "dead_exchange";           //死信队列交换机名称

    private static final String KEY = "zhangsan";        //普通队列 RoutingKey
    private static final String DEAD_KEY = "lisi";       //死信队列 RoutingKey

    private static final String QUEUE_NAME = "normal-queue";       //普通队列名称
    private static final String DEAD_QUEUE_NAME = "dead-queue";    //死信队列名称

    public static void main(String[] args) throws IOException {
        Connection connection = RabbitUtils.getConnection();
        Channel channel = connection.createChannel();

        // 声明死信和普通交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        channel.exchangeDeclare(DEAD_EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        /*
        创建队列
        通过额外参数实现什么情况下转发到死信队列
        1. ttl由生产者确定
        2. 死信队列名称
        3. 死信交换机的routingKey
         */
        Map<String, Object> arguments = new HashMap<>();
        // 死信交换机名
        arguments.put("x-dead-letter-exchange", DEAD_EXCHANGE_NAME);
        // 死信交换机的routingKey
        arguments.put("x-dead-letter-routing-key", DEAD_KEY);
        // 指定正常队列长度，超过该范围的消息就进入死信队列
        //arguments.put("x-max-length", 6);
        channel.queueDeclare(QUEUE_NAME, false, false, false, arguments);
        channel.queueDeclare(DEAD_QUEUE_NAME, false, false, false, null);

        // 绑定队列
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, KEY);
        channel.queueBind(DEAD_QUEUE_NAME, DEAD_EXCHANGE_NAME, DEAD_EXCHANGE_NAME);

        DeliverCallback successBack = (consumerTag, message) -> {
            String info = new String(message.getBody(), StandardCharsets.UTF_8);
            if (info.equals("info0")) {
                System.out.println("C1用户拒绝的信息为：" + new String(message.getBody()));
                /* requeue 设置为 false 代表拒绝重新入队 该队列如果配置了死信交换机将发送到死信队列中,未配置则进行丢弃操作*/
                channel.basicReject(message.getEnvelope().getDeliveryTag(),false);
            }else{
                System.out.println("C1用户收到的信息为：" + new String(message.getBody()));
                channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
            }
        };

        CancelCallback cancelCallback = s -> {
            System.out.println("C1用户取消消费操作");
        };
        channel.basicConsume(QUEUE_NAME, true, successBack, cancelCallback);

    }
 }
