package com.example.rabbitmqqoslimiting.demos;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Provider {
    public static final String QUEUE_NAME = "slowQueue";
    public static final String EXCHANGE_NAME = "fastExchange";
    public static void main(String[] args) throws IOException {
        Connection connection = RabbitUtils.getConnection();
        assert connection != null;
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, true, false, false,null);
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.DIRECT);
        channel.queueBind(QUEUE_NAME, EXCHANGE_NAME, "hello");
        // 使用线程池模拟短时间大批量消息发送
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(5, 10, 100,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>());
        for (int i = 0; i < 2000; i++) {
            threadPool.execute(() -> {
                String message = Thread.currentThread().getName();
                try {
                    channel.basicPublish(EXCHANGE_NAME, "hello", null, message.getBytes(StandardCharsets.UTF_8));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }

        threadPool.shutdown();
        while (!threadPool.isTerminated()) {

        }
        System.out.println("所有消息发送完成");
    }
}
