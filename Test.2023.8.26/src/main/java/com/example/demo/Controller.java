package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ZSetOperations;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@RestController
public class Controller {
    /*
    Spring通过StringRedisTemplate操作redis
    StringRedisTemplate是RedisTemplate的子类，专门用来处理文本数据
    相比于jedis来说，RedisTemplate根据类型分类，做了更进一步的封装
     */
    @Autowired
    private StringRedisTemplate redisTemplate;

    @GetMapping("/testString")
    public String testString() {

        redisTemplate.execute( (RedisConnection connection) -> {
            connection.flushAll();
            //execute要求回调方法中需要写return语句，
            //返回的对象，会作为execute本身的返回值
            return null;
        });
        redisTemplate.opsForValue().set("key", "111");
        redisTemplate.opsForValue().set("key2", "222");
        redisTemplate.opsForValue().set("key3", "333");

        String value = redisTemplate.opsForValue().get("key");
        System.out.println("value:" + value);
        return "ok";
    }

    @GetMapping("/testList")
    public String testList() {
        redisTemplate.execute((RedisConnection connection) -> {
            connection.flushAll();
            return null;
        });
        redisTemplate.opsForList().leftPush("key", "111");
        redisTemplate.opsForList().leftPushAll("key", "222", "333");

        String value = redisTemplate.opsForList().rightPop("key");
        System.out.println("value:" + value);
        value = redisTemplate.opsForList().rightPop("key");
        System.out.println("value:" + value);
        value = redisTemplate.opsForList().rightPop("key");
        System.out.println("value:" + value);
        return "ok";
    }

    @GetMapping("/testSet")
    public String testSet() {
        redisTemplate.execute((RedisConnection connection)-> {
            connection.flushAll();
            return null;
        });

        redisTemplate.opsForSet().add("key", "111", "222", "333");
        Set<String> result = redisTemplate.opsForSet().members("key");
        System.out.println("result:" + result);

        Boolean exists = redisTemplate.opsForSet().isMember("key", "222");
        System.out.println("exists:" + exists);

        Long count = redisTemplate.opsForSet().size("key");
        System.out.println("count:" + count);

        redisTemplate.opsForSet().remove("key", "111", "222");
        result = redisTemplate.opsForSet().members("key");
        System.out.println("result:" + result);
        return "ok";
    }

    @GetMapping("/testHash")
    public String testHash() {
        redisTemplate.execute((RedisConnection connection) -> {
            connection.flushAll();
            return null;
        });

        redisTemplate.opsForHash().put("key", "f1", "111");
        Map<String, String> fields = new HashMap<>();
        fields.put("f2", "222");
        fields.put("f3", "333");
        redisTemplate.opsForHash().putAll("key", fields);

        String value = (String) redisTemplate.opsForHash().get("key", "f1");
        System.out.println("value:" + value);

        Boolean exists = redisTemplate.opsForHash().hasKey("key", "f1");
        System.out.println("exists: " + exists);

        redisTemplate.opsForHash().delete("key", "f1", "f2");

        Long size = redisTemplate.opsForHash().size("key");
        System.out.println("size: " + size);
        return "ok";
    }

    @GetMapping("/testZSet")
    public String testZSet() {
        redisTemplate.execute((RedisConnection connection) -> {
            connection.flushAll();
           return null;
        });

        redisTemplate.opsForZSet().add("key", "zhangsan", 10);
        redisTemplate.opsForZSet().add("key", "lisi", 20);
        redisTemplate.opsForZSet().add("key", "wangwu", 30);

        Set<String> members = redisTemplate.opsForZSet().range("key", 0, -1);
        System.out.println("members:" + members);


        Set<ZSetOperations.TypedTuple<String>> membersWithScore =
                redisTemplate.opsForZSet().rangeWithScores("key", 0, -1);
        System.out.println("membersWithScore: " + membersWithScore);

        Double score = redisTemplate.opsForZSet().score("key", "zhangsan");
        System.out.println("score:" + score);

        redisTemplate.opsForZSet().remove("key", "zhangsan");

        Long size = redisTemplate.opsForZSet().size("key");
        System.out.println("size:" + size);

        Long rank = redisTemplate.opsForZSet().rank("key", "lisi");
        System.out.println("rank:" + rank);
        return "ok";
    }

}
