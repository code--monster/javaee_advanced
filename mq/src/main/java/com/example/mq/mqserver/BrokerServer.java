package com.example.mq.mqserver;

import com.example.mq.common.*;
import com.example.mq.mqserver.core.BasicProperties;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * BrokerServer是消息队列的本体服务器
 * 本质上是一个TCP服务器
 * 自定义应用层协议：由于当前要交互的message是二进制数据，而http/json都是文本格式
 * 请求格式：type(4字节）+ length(4字节） + payload（长度由length决定）
 * 响应格式：type(4字节）+ length(4字节） + payload（长度由length决定）
 * type: 描述当前这个请求和响应是干什么的，即访问的方法名
 * payload: 根据请求还是响应，以及type的不同取值来决定
 * 如果是请求，payload就是请求参数的序列化结果
 * 如果是响应，payload就是响应返回结果的序列化结果
 */
public class BrokerServer {
    private ServerSocket serverSocket = null;
    //当前brokerServer只有一个虚拟主机
    private VirtualHost virtualHost = new VirtualHost("default");
    // 使用hash表将当前的所有会话（有哪些客户端正在和咱们服务器进行通信）
    // 此处key是channel, value是对应的Socket对象
    // 由于tcp连接建立的成本较高，因此一个tcp中搞多个channel当作逻辑连接
    private ConcurrentHashMap<String, Socket> sessions = new ConcurrentHashMap<>();
    // 引入线程池，来处理多个客服端请求
    private ExecutorService executorService = null;
    // 引入一个boolean变量控制服务器是否继续执行，注意内存可见性问题
    private volatile boolean runnable = true;

    public BrokerServer (int port) throws IOException {
        // 监听端口
        this.serverSocket = new ServerSocket(port);
    }

    public void start() throws IOException {
        System.out.println("[Broker Server]启动");
        // 线程不够会自动添加
        executorService = Executors.newCachedThreadPool();
        try {
            while(runnable) {
                //使用accept()接受客服端的连接
                // 一个clientSocket处理一个客服端连接
                Socket clientSocket = serverSocket.accept();
                // 把处理连接的逻辑丢给线程池
                executorService.submit(() -> {
                    processConnection(clientSocket);
                });
            }
        } catch (SocketException e) {
            System.out.println("[Broker Server] 服务器停止运行");
//            e.printStackTrace();
        }
    }
    // 停止服务器，一般使用kill
    // 此处还单独搞个停止的方法，是为了单元测试
    public void stop() throws IOException {
        // 此处就会出现内存可见性问题
        runnable = false;
        executorService.shutdownNow();
        serverSocket.close();
    }

    /**
     * 通过这个方法来处理一个客服端连接
     * 在一个连接中，可能涉及倒多个请求和响应
     * @param clientSocket
     */
    private void processConnection(Socket clientSocket) {
        try(InputStream inputStream = clientSocket.getInputStream();
            OutputStream outputStream = clientSocket.getOutputStream()) {
            // 请求和响应中有int类型，需要使用DataInputStream和DataOutputStream
            try(DataInputStream dataInputStream = new DataInputStream(inputStream);
                DataOutputStream dataOutputStream = new DataOutputStream(outputStream)) {
                // 在一个连接中，可能涉及倒多个请求和响应
                while(true) {
                    // 1. 读取请求并解析
                    Request request = readRequest(dataInputStream);
                    // 2. 根据请求计算响应
                    Response response = process(request, clientSocket);
                    // 3. 把响应写回客服端
                    writeResponse(dataOutputStream, response);
                }
            }catch (EOFException | SocketException e) {
                // 当dataInputStream读到末尾，就会抛出EOF异常
                // 如果客服端关闭连接，就会抛出SocketException
                System.out.println("[BrokerServer] connection 关闭! 客户端的地址: " + clientSocket.getInetAddress().toString()
                        + ":" + clientSocket.getPort());
            } catch (ClassNotFoundException | MqException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println("[BrokerServer] connection 出现异常!");
            e.printStackTrace();
        }finally {
            // 当连接处理完，要关闭socket连接
            try {
                clientSocket.close();
                // 一个 TCP 连接中, 可能包含多个 channel. 需要把当前这个 socket 对应的所有 channel 也顺便清理掉.
                clearClosedSession(clientSocket);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    // 读取请求
    private Request readRequest(DataInputStream dataInputStream) throws IOException {
        Request request = new Request();
        request.setType(dataInputStream.readInt());
        request.setLength(dataInputStream.readInt());
        byte[] payload = new byte[request.getLength()];
        int n = dataInputStream.read(payload);
        if(n != request.getLength()) {
            throw new IOException("读取请求格式出错！");
        }
        request.setPayload(payload);
        return request;
    }

    //把响应写回客服端
    private void writeResponse(DataOutputStream dataOutputStream, Response response) throws IOException {
        dataOutputStream.writeInt(response.getType());
        dataOutputStream.writeInt(response.getLength());
        dataOutputStream.write(response.getPayload());
        dataOutputStream.flush();
    }
    // 根据请求计算响应
    private Response process(Request request, Socket clientSocket) throws IOException, ClassNotFoundException, MqException {
        // 1. 将request中的payload字节数组反序列化为对象
        BasicArguments basicArguments = (BasicArguments) BinaryTool.fromBytes(request.getPayload());
        System.out.println("[Request] rid=" + basicArguments.getRid() + ", channelId=" + basicArguments.getChannelId()
                + ", type=" + request.getType() + ", length=" + request.getLength());
        // 2. 根据type的值，来决定请求要干什么
        boolean ok = true;
        if(request.getType() == 0x1) {
            // 创建channel
            sessions.put(basicArguments.getChannelId(), clientSocket);
            System.out.println("[Broker Server] 创建channel成功，channelId=" + basicArguments.getChannelId());
        }else if(request.getType() == 0x2) {
            // 销毁channel
            sessions.remove(basicArguments.getChannelId());
            System.out.println("[Broker Server] 销毁channel成功， channelId=" + basicArguments.getChannelId());
        }else if(request.getType() == 0x3) {
            // 创建交换机，此时payload就是ExchangeDeclareArguments对象
            ExchangeDeclareArguments arguments = (ExchangeDeclareArguments) basicArguments;
            ok = virtualHost.exchangeDeclare(arguments.getExchangeName(), arguments.getExchangeType(), arguments.isDurable(),
                    arguments.isAutoDelete(), arguments.getArguments());
        }else if(request.getType() == 0x4) {
            // 销毁交换机，此时的payload是ExchangeDeleteArguments对象
            ExchangeDeleteArguments arguments = (ExchangeDeleteArguments) basicArguments;
            ok = virtualHost.exchangeDelete(arguments.getExchangeName());
        }else if(request.getType() == 0x5) {
            QueueDeclareArguments arguments = (QueueDeclareArguments) basicArguments;
            ok = virtualHost.queueDeclare(arguments.getQueueName(), arguments.isDurable(), arguments.isExclusive(),
                    arguments.isAutoDelete(), arguments.getArguments());
        }else if(request.getType() == 0x6) {
            QueueDeleteArguments arguments = (QueueDeleteArguments) basicArguments;
            ok = virtualHost.queueDelete(arguments.getQueueName());
        }else if(request.getType() == 0x7) {
            QueueBindArguments arguments = (QueueBindArguments) basicArguments;
            ok = virtualHost.queueBind(arguments.getQueueName(), arguments.getExchangeName(), arguments.getBindingKey());
        }else if(request.getType() == 0x8) {
            QueueUnBindArguments arguments = (QueueUnBindArguments) basicArguments;
            ok = virtualHost.queueUnBind(arguments.getQueueName(), arguments.getExchangeName());
        }else if(request.getType() == 0x9) {
            BasicPublicArguments arguments = (BasicPublicArguments) basicArguments;
            ok = virtualHost.basicPublish(arguments.getExchangeName(), arguments.getRoutingKey(),
                    arguments.getBasicProperties(), arguments.getBody());
        }else if(request.getType() == 0xa) {
            BasicConsumeArguments arguments = (BasicConsumeArguments) basicArguments;

            ok = virtualHost.basicConsume(arguments.getConsumerTag(), arguments.getQueueName(), arguments.isAutoAck(),
                    new Consumer() {
                        // 这个回调函数要做的工作就是，把服务器收到的消息，推送给对应消费者客服端
                        // 该处是服务端主动做的
                        @Override
                        public void handleDelivery(String consumerTag, BasicProperties basicProperties, byte[] body) throws MqException, IOException {
                            // 1. 要知道消息发送给哪个客服端
                            // 此处的consumerTag是消费者身份标识，而一个消费者对应一个channelId,所以consumerTag就是channelId
                            // 根据channelId去sessions中查询，就可以得到对应的socket对象
                            Socket clientSocket = sessions.get(consumerTag);
                            if(clientSocket == null || clientSocket.isClosed()) {
                                throw new MqException("[BrokerServer] 订阅消息的客服端已经关闭");
                            }
                            // 2. 构造响应数据
                            SubScribeReturns subScribeReturns = new SubScribeReturns();
                            subScribeReturns.setChannelId(consumerTag);
                            subScribeReturns.setRid(""); //由于这里只有响应，没有请求，不需要去对应，rid暂时不需要
                            subScribeReturns.setOk(true);
                            subScribeReturns.setConsumerTag(consumerTag);
                            subScribeReturns.setBasicProperties(basicProperties);
                            subScribeReturns.setBody(body);
                            byte[] payload = BinaryTool.toBytes(subScribeReturns);
                            Response response = new Response();
                            // 0xc表示服务器端给消费者客服端推送的消息数据
                            response.setType(0xc);
                            // response的payload就是一个SubScribeReturns
                            response.setLength(payload.length);
                            response.setPayload(payload);
                            // 3. 把数据写回给客服端
                            // 注意：此处的dataOutputStream不能close
                            // 如果把dataOutputStream关闭了，就会把clientSocket的OutPutStream关闭了
                            // 此时就无法继续往socket中写入后续数据了
                            DataOutputStream dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
                            writeResponse(dataOutputStream, response);
                        }
                    });
        }else if(request.getType() == 0xb) {
            // 调用basicAck确认消息
            BasicAckArguments arguments = (BasicAckArguments) basicArguments;
            ok = virtualHost.basicAck(arguments.getQueueName(), arguments.getMessageId());
        }else {
            // 当前type是非法的
            throw new MqException("[BrokerServer] 未知的type! type=" + request.getType());
        }
        // 3. 构造响应
        BasicReturns basicReturns = new BasicReturns();
        basicReturns.setChannelId(basicArguments.getChannelId());
        basicReturns.setRid(basicArguments.getRid());
        basicReturns.setOk(ok);
        byte[] payload = BinaryTool.toBytes(basicReturns);
        Response response = new Response();
        response.setType(request.getType());
        response.setLength(payload.length);
        response.setPayload(payload);
        System.out.println("[Response] rid=" + basicReturns.getRid() + ", channelId=" + basicReturns.getChannelId()
                + ", type=" + response.getType() + ", length=" + response.getLength());
        return response;
    }

    /**
     * 正常来说，客服端哪个channel不使用就主动关闭（type = 0x2)
     * 特殊情况，当前channel没来得及关闭，tcp连接就断开了
     * 此时存在hash表中channel对应关系没来得及删除
     * @param clientSocket
     */
    private void clearClosedSession(Socket clientSocket) {
        List<String> toDeleteChannelId = new ArrayList<>();
        for (Map.Entry<String, Socket> entry :
                sessions.entrySet()) {
            if (entry.getValue() == clientSocket) {
                // 在一个集合类内遍历和删除一起进行是大忌，会踩空
                // sessions.remove(entry.getKey());
                toDeleteChannelId.add(entry.getKey());
            }
        }
        for(String channelId : toDeleteChannelId) {
            sessions.remove(channelId);
        }
        System.out.println("[BrokerServer] 清理sessions完成，被清理的channelId=" + toDeleteChannelId);
    }

}
