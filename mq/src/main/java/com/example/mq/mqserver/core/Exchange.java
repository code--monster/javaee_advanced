package com.example.mq.mqserver.core;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/*
这个类表示一个交换机
 */
@Data
public class Exchange {
    // 此处使用name作为交换机的唯一身份标识
    private String name;
    // 交换机类型有 direct,fanout,topic
    private ExchangeType type = ExchangeType.DIRECT;
    // 该交换机是否要持久化存储，true表示持久化，false表示不持久化
    private boolean durable = false;
    // 如果当前交换机, 没人使用了, 就会自动被删除.
    // 这个属性暂时先列在这里, 后续的代码中并没有真的实现这个自动删除功能~~ (RabbitMQ 是有的)
    private boolean autoDelete = false;
    // arguments 表示的是创建交换机时指定的一些额外的参数选项. 后续代码中并没有真的实现对应的功能, 先列出来. (RabbitMQ 也是有的)
    // 为了把这个 arguments 存到数据库中, 就需要把 Map 转成 json 格式的字符串.
    private Map<String, Object> arguments = new HashMap<>();
    // mybatis往数据库中写入数据，需要调用getter方法，拿到属性值，再往数据库中写
    // 而数据库中没有HashMap这样的结构，因此需要将hashmap转成string类型，才能写入数据库中
    public String getArguments() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String tmp = objectMapper.writeValueAsString(arguments);
            System.out.println("[Exchange]" + tmp);
            return tmp;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        // 如果代码异常了返回一个空的json字符串
        return "{}";
    }
    // mybatis从数据库读数据的时候会调用setter方法，把数据库中读到的结果设置到对象的属性中
    public void setArguments(String argumentsJson) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            //第二个参数，用来描述当前json字符串，要转成的java对象是啥类型
            // 如果是简单类型，直接使用类对象即可
            // 如果是复杂类型，使用TypeReference匿名内部类来描述复杂类型的具体信息
            this.arguments = objectMapper.readValue(argumentsJson, new TypeReference<HashMap<String, Object>>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
    // 针对arguments再提供一组getter,setter，用来更方便的获取/设置这里的键值对
    // 这一组在java代码内部使用（测试）
    public Object getArguments(String key) {
        return arguments.get(key);
    }
    public void setArguments(String key, Object value) {
        arguments.put(key, value);
    }
    public void setArguments(Map<String, Object> arguments) {
        this.arguments = arguments;
    }

}
