package com.example.mq.mqserver.core;

import lombok.Data;

import java.io.Serializable;

/**
 * Message的属性部分
 */
@Data
public class BasicProperties implements Serializable {
    // 消息的唯一身份标识，此处为了保证id的唯一性，使用uuid
    private String messageId;
    // 如果当前交换机类型是DIRECT,此时routingKey就表示要转发的队列名
    // 如果当前交换机类型FANOUT,此时routingKey无意义（不使用）
    // 如果当前交换机类型TOPIC，此时routingKey就要和bindingKey做匹配符合要求才能转发给对应队列
    private String routingKey;
    // 表示消息是否要持久化，1. 表示不持久化，2. 表示持久化
    private int deliverMode = 1;
    // 其实针对 RabbitMQ 来说, BasicProperties 里面还有很多别的属性. 其他的属性暂时先不考虑了.
}
