package com.example.mq.mqserver.core;

import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

/**
 * 表示要传递的一个消息
 * 此处message对象需要能够在网络上传输，并且也需要写入文件中
 * 因此需要对message进行序列化和反序列化
 * 序列化：将对象转成二进制的数据
 * 反序列化：把二进制数据转成对象
 */
@Data
public class Message implements Serializable {
    private BasicProperties basicProperties = new BasicProperties();
    private byte[] body;
    // message后续需要需要持久化到文件中
    // 一个文件中会存储很多信息，如何找到某个消息，在文件的具体位置呢？？
    // 需要使用偏移量来表示[offsetBeg,offsetEnd)
    // 而这两个属性不需要被序列化保存到文件中，因为消息被写入文件中，所有的位置都固定了，并不需要单独存储
    // 因此需要transient修饰
    // 这两属性存在的目的，主要是为了让内存中的message对象，能够快速找到硬盘上message的位置
    private transient long offsetBeg = 0;
    private transient long offsetEnd = 0;
    // 用来表示该消息在文件中是否是有效消息（逻辑删除）
    // ox1表示有效，0x0表示无效
    private byte isValid = 0x1;
    // 创建一个工厂方法，让工厂方法帮我们封装一下创建Message对象的过程,便于后续的扩展和维护
    // 这个方法中创建的message对象，会自动生成唯一的messageId(使用构造方法就没办法，而且也不能见名知意）
    // 万一routingKey和basicProperties里的routingKey冲突，以外面的为主
    public static Message createMessageWithId(String routingKey, BasicProperties basicProperties,byte[] body ) {
        Message message = new Message();
        if(basicProperties != null) {
            message.setBasicProperties(basicProperties);
        }
        // 此处生成的messageId以M-作为前缀
        message.setMessageId("M-" + UUID.randomUUID());
        message.setRoutingKey(routingKey);
        message.body = body;
        // 此处是把 body 和 basicProperties 先设置出来. 他俩是 Message 的核心内容.
        // 而 offsetBeg, offsetEnd, isValid, 则是消息持久化的时候才会用到. 在把消息写入文件之前再进行设定.
        // 此处只是在内存中创建一个 Message 对象.
        return message;
    }

    public String getMessageId() {
        return basicProperties.getMessageId();
    }

    public void setMessageId(String messageId) {
        basicProperties.setMessageId(messageId);
    }

    public String getRoutingKey() {
        return basicProperties.getRoutingKey();
    }

    public void setRoutingKey(String routingKey) {
        basicProperties.setRoutingKey(routingKey);
    }

    public int getDeliverMode() {
        return basicProperties.getDeliverMode();
    }

    public void setDeliverMode(int mode) {
        basicProperties.setDeliverMode(mode);
    }


}
