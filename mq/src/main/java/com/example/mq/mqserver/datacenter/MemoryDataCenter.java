package com.example.mq.mqserver.datacenter;

import com.example.mq.common.MqException;
import com.example.mq.mqserver.core.Binding;
import com.example.mq.mqserver.core.Exchange;
import com.example.mq.mqserver.core.MSGQueue;
import com.example.mq.mqserver.core.Message;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 *使用这个类统一管理内存中所有数据
 * 该类提供的一些方法，需要在多线程环境下进行，主要线程安全问题
 * 交换机：使用hashmap, key是exchangeName, value是Exchange对象
 * 队列：使用hashmap, key是queueName, value是MSGQueue对象
 * 绑定：使用嵌套hashMap, key是exchangeName, value是hashmap，其中key为queueName,value是Binding对象
 * 消息：使用hashmap, key是messageId, value是Message对象
 * 队列与消息之间的关联（每个队列有哪些消息）：使用嵌套hashmap, key是queueName,  value是LinkedList，每个元素是message对象
 * 未被确认消息（当前队列有哪些消息被消费者消费了，但没有应答）：使用嵌套hashmap，key是queueName, value是hashmap，其中key是messageId, value是message对象
 *
 */
public class MemoryDataCenter {
    // 在多线程环境下hashmap是线程不安全的，
    // 交换机,key为exchangeName, value是交换机对象
    private ConcurrentHashMap<String , Exchange> exchangeMap = new ConcurrentHashMap<>();
    // 队列， key是queueName, value是MSGQueue
    private ConcurrentHashMap<String, MSGQueue> queueMap = new ConcurrentHashMap<>();
    // 绑定，key是exchangeName, value是hashmap，其中key为queueName, value为Binding对象
    private ConcurrentHashMap<String, ConcurrentHashMap<String, Binding>> bindingsMap = new ConcurrentHashMap<>();
    // 消息，key是messageId,value是message对象
    private ConcurrentHashMap<String, Message> messageMap = new ConcurrentHashMap<>();
    // 每个队列有哪些message，key是queueName, value是message链表
    private ConcurrentHashMap<String, LinkedList<Message>> queueMessageMap = new ConcurrentHashMap<>();
    // 未被确认的消息，key是queueName,第二个key是messageId, value是Message对象
    private ConcurrentHashMap<String , ConcurrentHashMap<String, Message>> queueMessageWaitAckMap = new ConcurrentHashMap<>();

    // 交换机
    public void insertExchange(Exchange exchange) {
        exchangeMap.put(exchange.getName(), exchange);
        System.out.println("[MemoryDataCenter] 新交换机添加成功，exchangeName=" + exchange.getName());
    }

    public void deleteExchange(String exchangeName) {
        exchangeMap.remove(exchangeName);
        System.out.println("[MemoryDataCenter] 交换机删除成功， exchangeName=" + exchangeName);
    }

    public Exchange getExchange(String exchangeName) {
        return exchangeMap.get(exchangeName);
    }

    // 队列
    public void insertQueue(MSGQueue queue) {
        queueMap.put(queue.getName(), queue);
        System.out.println("[MemoryDataCenter] 队列添加成功，queueName=" + queue.getName());

    }

    public void deleteQueue(String queueName) {
        queueMap.remove(queueName);
        System.out.println("[MemoryDataCenter] 队列删除成功，queueName=" + queueName);

    }

    public MSGQueue getQueue(String queueName) {
        return queueMap.get(queueName);
    }

    // 绑定
    public void insertBinding(Binding binding) throws MqException {
//        ConcurrentHashMap<String, Binding> bindingMap = bindingsMap.get(binding.getExchangeName());
//        if(bindingMap == null) {
//            bindingMap = new ConcurrentHashMap<>();
//            bindingsMap.put(binding.getExchangeName(), bindingMap);
//        }
        // 上述代码可以等价于
        // 先用exchangeName查一下，对应的哈希表是否存在，不存在就创建一个
        // 线程安全
        ConcurrentHashMap<String, Binding> bindingMap = bindingsMap.computeIfAbsent(binding.getExchangeName(),
                k -> new ConcurrentHashMap<>());
        // 多个线程同时进行绑定，可能发生两个queueName一样但binding对象不同，同时放入bindingMap造成覆盖
        synchronized (bindingMap) {
            // 再根据queueName查一下，如果已经存在，就抛出异常，不存在才能插入
            if(bindingMap.get(binding.getQueueName()) != null) {
                throw new MqException("[MemoryDataCenter] 绑定已经存在! exchangeName=" + binding.getExchangeName() +
                        ", queueName=" + binding.getQueueName());
            }
            bindingMap.put(binding.getQueueName(), binding);
        }
        System.out.println("[MemoryDataCenter] 新绑定添加成功， exchangeName=" + binding.getExchangeName()
            + ", queueName=" + binding.getQueueName());
    }

    // 获取绑定写两个版本
    // 1. 根据exchangeName和queueName确定唯一一个Binding
    // 2. 根据exchangeName获取到所有的Binding
    public Binding getBinding(String exchangeName, String queueName) {
        ConcurrentHashMap<String, Binding> bindingMap = bindingsMap.get(exchangeName);
        if(bindingMap == null) {
            System.out.println("bindingMap为null");
            return null;

        }
        return bindingMap.get(queueName);
    }

    public ConcurrentHashMap<String, Binding> getBindings(String exchangeName) {
        return bindingsMap.get(exchangeName);
    }

    public void deleteBinding(Binding binding) throws MqException {
        ConcurrentHashMap<String, Binding> bindingMap = bindingsMap.get(binding.getExchangeName());
        if(bindingMap == null) {
            //该交换机没有任何队列
            throw new MqException("[MemoryDataCenter] 绑定不存在! exchangeName=" + binding.getExchangeName()
                    + ", queueName=" + binding.getQueueName());
        }
        bindingMap.remove(binding.getQueueName());
        System.out.println("[MemoryDataCenter] 绑定删除成功! exchangeName=" + binding.getExchangeName()
                + ", queueName=" + binding.getQueueName());
    }

    // 消息
    public void addMessage(Message message) {
        messageMap.put(message.getMessageId(), message);
        System.out.println("[MemoryDataCenter] 新消息添加成功，messageId=" + message.getMessageId());
    }
    public void removeMessage(String messageId) {
        messageMap.remove(messageId);
        System.out.println("[MemoryDataCenter] 消息被移除， messageId" + messageId);
    }
    public Message getMessage(String messageId) {
        return messageMap.get(messageId);
    }


    // 发送消息到指定队列中
    public void sendMessage(MSGQueue queue, Message message) {
        // 把消息放到对应的队列的数据结构中
        // 先根据队列名字，找到该队列对应的消息链表
        LinkedList<Message> messages = queueMessageMap.computeIfAbsent(queue.getName(),
                k -> new LinkedList<>());
        // 再把数据加到messages中
        synchronized (messages) {
            messages.add(message);
        }
        // 在这里把该消息往消息中心也插入一下，如果message中心已经存在，重复插入也没有关系，不需要加锁
        // 因为相同的messageId, 对应的message的内容一定一样(因为服务端代码不会对message内容做修改basicProperties和body)
        addMessage(message);
        System.out.println();
    }

    // 从队列中取消息
    public Message pollMessage(String queueName) {
        // 根据队列名，查找一下，对应的队列的消息链表
        LinkedList<Message> messages = queueMessageMap.get(queueName);
        if(messages == null) {
            return null;
        }
        synchronized (messages) {
            // 如果没找到，说明队列没有任何消息
            if(messages.size() == 0) {
                return null;
            }
            // 链表中有元素，进行头删
            Message currentMessage = messages.remove(0);
            System.out.println("[MemoryDataCenter]消息队列中取出 messageId" + currentMessage.getMessageId());
            return currentMessage;

        }
    }

    // 获取指定队列中消息的个数
    public int getMessageCount(String queueName) {
        LinkedList<Message> messages = queueMessageMap.get(queueName);
        if(messages == null) {
            return 0;
        }
        // 获取队列元素个数时，要对链表加锁，防止有其他线程在这期间进行添加/删除
        synchronized (messages) {
            return messages.size();
        }
    }

    // 添加未被确认的消息
    public void addMessageWaitAck(String queueName, Message message) {
        ConcurrentHashMap<String, Message> messageHashMap = queueMessageWaitAckMap.computeIfAbsent(queueName,
                k -> new ConcurrentHashMap<>());
        messageHashMap.put(message.getMessageId(), message);
        System.out.println("[MemoryDataCenter] 消息进入待确认队列! messageId=" + message.getMessageId());
    }

    public void removeMessageWaitAck(String queueName, String messageId) {
        ConcurrentHashMap<String, Message> messageHashMap = queueMessageWaitAckMap.get(queueName);
        if(messageHashMap == null) {
            return;
        }
        messageHashMap.remove(messageId);
        System.out.println("[MemoryDataCenter] 消息从待确认队列删除! messageId=" + messageId);
    }

    // 获取指定未确认的消息
    public Message getMessageWaitAck(String queueName, String messageId) {
        ConcurrentHashMap<String, Message> messageHashMap = queueMessageWaitAckMap.get(queueName);
        if(messageHashMap == null) {
            return null;
        }
        return messageHashMap.get(messageId);
    }

    // 从硬盘上读取数据， 把硬盘中之前持久化的数据都恢复到内存中
    public void recovery(DiskDataCenter diskDataCenter) throws IOException, MqException, ClassNotFoundException {
        // 1. 清空之前的所有数据
        exchangeMap.clear();
        queueMap.clear();
        bindingsMap.clear();
        messageMap.clear();
        queueMessageMap.clear();

        // 2. 恢复所有交换机数据
        List<Exchange> exchanges = diskDataCenter.selectAllExchanges();
        for (Exchange exchange :
                exchanges) {
            exchangeMap.put(exchange.getName(), exchange);
        }
        // 2. 恢复所有队列的数据
        List<MSGQueue> queues = diskDataCenter.selectAllQueues();
        for (MSGQueue queue :
                queues) {
            queueMap.put(queue.getName(), queue);
        }
        // 3. 恢复所有绑定数据
        List<Binding> bindings = diskDataCenter.selectAllBindings();
        for (Binding binding:
             bindings) {
            System.out.println("binding信息：" + binding.toString());
            ConcurrentHashMap<String, Binding> bindingMap = bindingsMap.computeIfAbsent(binding.getExchangeName(),
                    k -> new ConcurrentHashMap<>());
            bindingMap.put(binding.getQueueName(), binding);
            System.out.println("binding对象" + bindingMap.get(binding.getQueueName()));
        }
        // 4. 恢复所有消息数据
        // 遍历所有队列，根据队列名字，获取到所有消息
        for (MSGQueue queue :
                queues) {
            LinkedList<Message> messages = diskDataCenter.loadAllMessageFromQueue(queue.getName());
            queueMessageMap.put(queue.getName(), messages);
            for (Message message : messages) {
                messageMap.put(message.getMessageId(), message);
            }
        }
        // 注意!! 针对 "未确认的消息" 这部分内存中的数据, 不需要从硬盘恢复. 之前考虑硬盘存储的时候, 也没设定这一块.
        // 一旦在等待 ack 的过程中, 服务器重启了, 此时这些 "未被确认的消息", 就恢复成 "未被取走的消息" .
        // 这个消息在硬盘上存储的时候, 就是当做 "未被取走"
    }

}

