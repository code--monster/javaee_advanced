package com.example.mq.mqserver.core;

import com.example.mq.common.ConsumerEnv;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/*
这个类表示一个存储消息的队列
 */
@Data
public class MSGQueue {

    // 表示队列的身份标识
    private String name;
    // 表示队列是否持久化，true表示持久化保存，false表示不持久化
    private boolean durable = false;
    // 这个属性为 true, 表示这个队列只能被一个消费者使用(别人用不了). 如果为 false 则是大家都能使用
    // 这个 独占 功能, 也是先把字段列在这里, 具体的独占功能暂时先不实现.
    private boolean exclusive = false;
    // 为 true 表示没有人使用之后, 就自动删除. false 则是不会自动删除.
    // 这个 自动删除 功能, 也是先把字段列在这里, 具体的独占功能暂时先不实现.
    private boolean autoDelete = false;
    // 也是表示扩展参数. 当前也是先列在这里, 先暂时不实现
    private Map<String, Object> arguments = new HashMap<>();
    // 当前队列都有哪些消费者订阅了
    private List<ConsumerEnv> consumerEnvList  = new ArrayList<>();
    // 记录当前取到了第几个消费者，方便实现轮询
    // 使用原子类，保证自增操作的线程安全性
    private AtomicInteger consumerSeq = new AtomicInteger(0);

    // 添加一个订阅者
    public void addConsumerEnv(ConsumerEnv consumerEnv) {
        consumerEnvList.add(consumerEnv);
    }
    // 删除订阅者先不考虑

    // 挑选一个订阅者，用来处理当前的消息（轮询方式）
    public ConsumerEnv chooseConsumer() {
        if(consumerEnvList.size() == 0) {
            // 该队列没人订阅
            return null;
        }
        int index = consumerSeq.get() % consumerEnvList.size();
        consumerSeq.getAndIncrement();
        return consumerEnvList.get(index);
    }



    // mybatis往数据库中写入数据，需要调用getter方法，拿到属性值，再往数据库中写
    // 而数据库中没有HashMap这样的结构，因此需要将hashmap转成string类型，才能写入数据库中
    public String getArguments() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(arguments);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        // 如果代码异常了返回一个空的json字符串
        return "{}";
    }
    // mybatis从数据库读数据的时候会调用setter方法，把数据库中读到的结果设置到对象的属性中
    public void setArguments(String argumentsJson) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            //第二个参数，用来描述当前json字符串，要转成的java对象是啥类型
            // 如果是简单类型，直接使用类对象即可
            // 如果是复杂类型，使用TypeReference匿名内部类来描述复杂类型的具体信息
            this.arguments = objectMapper.readValue(argumentsJson, new TypeReference<HashMap<String, Object>>() {});
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public Object getArguments(String key) {
        return arguments.get(key);
    }
    public void setArguments(String key, Object value) {
        arguments.put(key, value);
    }
    public void setArguments(Map<String, Object> arguments) {
        this.arguments = arguments;
    }
}
