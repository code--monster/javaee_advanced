package com.example.mq.mqserver.core;

import com.example.mq.common.MqException;
import com.sun.xml.internal.bind.v2.TODO;

/**
 * 使用这个类来实现交换机的转发规则
 * 同时也借助这个类来验证bindingKey是否合法
 * bindingKey:创建绑定的时候，给绑定指定特殊的字符串，相当于出题
 * routingKey:发布消息的时候，给消息指定的特殊字符串，相当于做题
 * bindingKey的构造规则
 * 1. 数字，字母，下划线
 * 2. 使用 . 进行分割
 * 3. 允许存在 * 和 # 作为通配符，但是通配符只能作为独立的分段(aa.*bb.cc就是非法的）
 *  1) *可以匹配任何一个独立部分
 *  2) #可以匹配任何0个或多个独立部分
 *
 *  routingKey的构造规则
 *  1. 数字，字母，下划线
 *  2. 使用 . 把整个routingKey分成多个部分
 *
 */
public class Router {
    /**
     * 检查是否符合bindingKey的构造规则
     * @param bindingKey
     * @return
     */
    public boolean checkBindingKey(String bindingKey) {
        if(bindingKey.length() == 0) {
            // 空字符串，也是合法的。在使用direct/fanout交换机时，bindingKey用不上
            return true;
        }
        // 检查字符串中不能存在非法字符
        for(int i = 0; i < bindingKey.length(); i++) {
            char ch = bindingKey.charAt(i);
            if(ch >= 'a' && ch <= 'z') {
                continue;
            }
            if(ch >= 'A' && ch <= 'Z') {
                continue;
            }
            if(ch >= '0' && ch <= '9') {
                continue;
            }
            if(ch == '_' || ch == '.' || ch == '*' || ch == '#') {
                continue;
            }
            return false;
        }
        // 检查*或者#是否是独立的部分
        // aaa.b*.ccc是非法
        // .在正则表达式中是特殊字符，想要当做原始字符处理需要进行转义。在正则中使用\.表示
        // 又在java中写入\.这样的文本，\又是一个特殊字符，需要使用\\进行转义
        String[] words = bindingKey.split("\\.");
        for(String word : words) {
            if(word.length() > 1 && (word.contains("*") || word.contains("#"))) {
                return false;
            }
        }
        // 约定通配符之间的相邻关系
        // 1. aaa.#.#.bbb非法
        // 2. aaa.#.*.bbb非法
        // 3. aaa.*.#.bbb非法
        // 4. aaa.*.*.bbb合法
        // 因为前三种相邻实现匹配的逻辑非常繁琐，同时功能性提高不大
        for(int i = 0; i < words.length - 1; i++) {
            if(words[i].equals("#") && words[i+1].equals("#")) {
                return false;
            }
            if(words[i].equals("#") && words[i+1].equals("*")) {
                return false;
            }
            if(words[i].equals("*") && words[i+1].equals("#")) {
                return false;
            }
        }
        return true;

    }

    /**
     * 检查是否符合routingKey的构造规则
     * @param routingKey
     * @return
     */
    public boolean checkRoutingKey(String routingKey) {
        if(routingKey.length() == 0) {
            // 空字符串是合法的，使用fanout交换机时，routingKey用不上，可以设为“”
            return true;
        }
        for (int i = 0; i < routingKey.length(); i++) {
            char ch = routingKey.charAt(i);
            if(ch >= 'A' && ch <= 'Z') {
                continue;
            }
            if(ch >= 'a' && ch <= 'z') {
                continue;
            }
            if(ch >= '0' && ch <= '9') {
                continue;
            }
            if(ch == '_' || ch == '.') {
                continue;
            }
            return false;
        }
        return true;
    }

    /**
     * 判定该消息是否可以转发给这个绑定对应的队列
     * 1. 无*和#
     * bindingKey:aaa.ccc.111
     * routingKey:aaa.ccc.111
     * 匹配成功，这种情况类似于直接交换机，尤其是把bindingKey设为队列名时，此时完全等价于直接交换机
     * 2. bindingKey中有*
     * bindingKey:aaa.*.ccc
     * routingKey:aaa.bbb.ccc 匹配成功
     * routingKey:aaa.b.b.ccc匹配失败
     * 3. bindKey中有#
     * bindingKey: aaa.#.ccc
     * routingKey: aaa.b.b.ccc匹配成功
     * routingKey: aaa.ccc匹配成功
     * aaa.b.b.b匹配失败
     * 如果bindingKey只有一个#，就是fanout交换机
     * @param exchangeType
     * @param binding
     * @param message
     * @return
     */
    public boolean route(ExchangeType exchangeType, Binding binding, Message message) throws MqException {
        // 根据不同的exchangeType使用不同的判定转发规则
        if(exchangeType == ExchangeType.FANOUT) {
            return true;
        } else if(exchangeType == ExchangeType.TOPIC) {
            // 如果是topic主体交换机，规则就更复杂些
            return routeTopic(binding, message);
        }else {
            throw new MqException("[Router]交换机类型非法 exchangeType=" + exchangeType);
        }
    }

    private boolean routeTopic(Binding binding, Message message) {
        // 1. 先把这两个key进行切分
        String[] bindingTokens = binding.getBindingKey().split("\\.");
        String[] routingTokens = message.getRoutingKey().split("\\.");

        // 2. 引入两个下标，指向上述两个数组，初始为0
        int bindingIndex = 0;
        int routingIndex = 0;
        /*
        情况1：字符串匹配
        情况2：*
        情况3：#是最后一个字符串
        情况4：#不是最好一个字符串
        情况5：双方都到了末尾
         */
        while (bindingIndex < bindingTokens.length && routingIndex < routingTokens.length) {
            if(bindingTokens[bindingIndex].equals("*")) {
                // 情况2，遇到*，直接进入下一轮
                routingIndex++;
                bindingIndex++;
                continue;
            }else if(bindingTokens[bindingIndex].equals("#")) {
                bindingIndex++;
                // 情况3，#可以匹配任意个字符串
                if(bindingIndex == bindingTokens.length) {
                    return true;
                }
                // 情况4，#不是最后一个字符串，需要拿着后面的字符串去routingKey中找匹配的字符串
                // 如果找到返回下标，没有找到返回-1
                routingIndex = findNextMatch(routingTokens, routingIndex, bindingTokens[bindingIndex]);
                if(routingIndex == -1) {
                    return false;
                }
                // 找到匹配的情况，继续往后匹配
                bindingIndex++;
                routingIndex++;
            } else{
                // 情况1，匹配普通字符串
                if(!bindingTokens[bindingIndex].equals(routingTokens[routingIndex])) {
                    return false;
                }
                bindingIndex++;
                routingIndex++;
            }
        }
        // 情况5， 判定双非是否同时到达末尾
        // aaa.bbb.ccc和aaa.bbb是匹配失败的
        if(bindingIndex == bindingTokens.length && routingIndex == routingTokens.length) {
            return true;
        }
        return false;
    }

    private int findNextMatch(String[] routingTokens, int routingIndex, String bindingToken) {
        for (int i = routingIndex; i < routingTokens.length; i++) {
            if(routingTokens[i].equals(bindingToken)) {
                return i;
            }
        }
        return -1;
    }


}
