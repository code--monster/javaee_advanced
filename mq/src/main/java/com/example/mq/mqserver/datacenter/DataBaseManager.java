package com.example.mq.mqserver.datacenter;

import com.example.mq.MqApplication;
import com.example.mq.mqserver.core.Binding;
import com.example.mq.mqserver.core.Exchange;
import com.example.mq.mqserver.core.ExchangeType;
import com.example.mq.mqserver.core.MSGQueue;
import com.example.mq.mqserver.mapper.MetaMapper;

import java.io.File;
import java.util.List;

/**
 * 通过这个类整合数据库操作
 */
public class DataBaseManager {
    // DataBaseManager不交给ioc容器管理，因此此处需要自己通过context来引入
    private MetaMapper metaMapper;

    // 针对数据库进行初始化，建库建表，插入一些默认数据
    // 把broker server部署到一个新的服务器上，显然此时没有数据库，需要让broker server启动的时候，自动把对应的数据库创建好
    // 如果是已经部署的机器，broker server重启会发现数据库已经存在了，此时不做任何数据库操作
    public void init() {
        // 手动获取MetaMapper
        metaMapper = MqApplication.context.getBean(MetaMapper.class);
        if(!checkDBExists()) {
            // 数据库不存在，就进行建库建表操作
            // 先创建一个data目录
            File dataDir = new File("./data");
            dataDir.mkdirs();
            // 创建数据表
            createTable();
            // 插入默认数据
            createDefaultData();
            System.out.println("[DataBaseManager]数据库初始化完成");
        }else{
            // 数据库已经存在
            System.out.println("[DataBashManager]数据库已经存在");
        }
    }

    public void deleteDB() {
        File file = new File("./data/meta.db");
        boolean ret = file.delete();
        if(ret) {
            System.out.println("[DataBaseManager]删除数据库文件成功");
        }else {
            System.out.println("[DataBaseManager]删除数据库文件失败");
        }
    }

    private boolean checkDBExists() {
        File file = new File("./data/meta.db");
        if(file.exists()) {
            return true;
        }
        return false;
    }

    /**
     * 用于建表
     * 建库操作不需要手动执行（不需要自己手动创建meta.db文件）
     * 首次执行这里的数据库操作的时候，就会自动创建出meta。db文件来
     */
    private void createTable() {
        metaMapper.createExchangeTable();
        metaMapper.createQueueTable();
        metaMapper.createBindingTable();
        System.out.println("[DataBaseManage]创建表完成");
    }

    /**
     * 给数据库表中，添加默认数据
     * 此时主要添加一个默认的交换机
     * rabbitmq中默认带有一个匿名的交换机，类型是direct
     */
    private void createDefaultData() {
        // 构造一个默认的交换机
        Exchange exchange = new Exchange();
        exchange.setName("");
        exchange.setType(ExchangeType.DIRECT);
        exchange.setDurable(true);
        exchange.setAutoDelete(false);
        metaMapper.insertExchange(exchange);
        System.out.println("[DataBaseManager]创建初始数据完成");
    }

    public void insertExchange(Exchange exchange) {
        metaMapper.insertExchange(exchange);
    }

    public void deleteExchange(String exchangeName) {
        metaMapper.deleteExchange(exchangeName);
    }

    public List<Exchange> selectAllExchanges() {
        return metaMapper.selectAllExchanges();
    }

    public void insertQueue(MSGQueue queue) {
        metaMapper.insertQueue(queue);
    }

    public void deleteQueue(String queueName) {
        metaMapper.deleteQueue(queueName);
    }

    public List<MSGQueue> selectAllQueues() {
        return metaMapper.selectAllQueues();
    }

    public void insertBinding(Binding binding) {
        metaMapper.insertBinding(binding);
    }

    public void deleteBinding(Binding binding) {
        metaMapper.deleteBinding(binding);
    }

    public List<Binding> selectAllBindings() {
        return metaMapper.selectAllBindings();
    }
}
