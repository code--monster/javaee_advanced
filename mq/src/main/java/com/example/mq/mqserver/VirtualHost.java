package com.example.mq.mqserver;

import com.example.mq.common.Consumer;
import com.example.mq.common.MqException;
import com.example.mq.mqserver.core.*;
import com.example.mq.mqserver.datacenter.DiskDataCenter;
import com.example.mq.mqserver.datacenter.MemoryDataCenter;
import lombok.Data;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 通过这个类来表示虚拟主机，目的是保证隔离性
 * 每个虚拟主机下面管理着交换机，队列，绑定，消息，数据
 * 同时提供api供上层使用，如下所示
 * 1. 创建/删除交换机 exchangeDeclare,exchangeDelete
 * 2. 创建/删除队列 queueDeclare, queueDelete
 * 3. 创建/删除绑定 queueBind, queueUnBind
 * 4. 发送消息 basicPublic
 * 5. 订阅消息 basicConsume
 * 6. 确认消息 basicAck
 * 作为业务逻辑的整合者，这需要对于代码中抛出的异常做出处理
 */
@Data
public class VirtualHost {
    private String virtualHostName;
    private MemoryDataCenter memoryDataCenter = new MemoryDataCenter();
    private DiskDataCenter diskDataCenter = new DiskDataCenter();
    private Router router = new Router();
    private ConsumerManager consumerManager = new ConsumerManager(this);

    // 操作交互机的锁对象
    private final Object exchangeLocker = new Object();
    // 操作队列的锁对象
    private final Object queueLocker = new Object();

    public VirtualHost(String name) {
        this.virtualHostName = name;
        // 对于MemoryDataCenter来说，不需要额外的初始化操作，只要对象New出来即可
        // 但是对于DiskDataCenter来说，则需要进行初始化，进行建库建表操作
        diskDataCenter.init();
        // 还需要将硬盘上的数据，恢复到内存中
        try {
            memoryDataCenter.recovery(diskDataCenter);
        } catch (IOException | MqException | ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("[VirtualHost]恢复内存数据失败");
        }
    }

    /**
     * 创建交换机，不存在就创建，存在直接返回
     * @param exchangeName 交换机名
     * @param exchangeType 交换机类型
     * @param durable 持久化
     * @param autoDelete 自动删除
     * @param arguments 参数
     * @return 创建成功返回true, 失败返回false
     */
    public boolean exchangeDeclare(String exchangeName, ExchangeType exchangeType, boolean durable,
                                   boolean autoDelete, Map<String, Object> arguments) {
        // 如何表示交换机与主机之间的从属关系呢？？
        // 1. 参考数据库设计的一对多方案，给每个交换机表中添加虚拟主机属性
        // 2. 交换机名 = 虚拟机名 + 交换机的真实名字
        // 3. 更优雅的方案：给每个虚拟主机，分配一组不同数据库和文件，但麻烦
        // 我们选择的方案是 交换机名 = 虚拟机名 + 交换机的真实名字
        // 虚拟机是为了保证隔离性：即虚拟主机1中的exchangeName可以和虚拟主机2的exchangeName重名
        // 此时加上虚拟主机作为前缀就可以区分出，同理，也可以按照这个方法去区分队列
        // 由于绑定和交换机，队列是强相关的，此时绑定也就被隔离开了
        // 再进一步，消息和队列是强相关的，队列名区分开了，消息也就区分开了
        exchangeName = virtualHostName + exchangeName;

        try {
            // 当两个线程都判定交换机为null,同时创建交换机，可能造成交换机对象覆盖
            synchronized (exchangeLocker) {
                // 1. 判定交换机是否已经存在，直接从内存查询
                Exchange existsExchange = memoryDataCenter.getExchange(exchangeName);
                if (existsExchange != null) {
                    // 交换机已经存在
                    System.out.println("[VirtualHost]交换机已经存在 exchangeName=" + exchangeName);
                    return true;
                }
                // 2. 创建交换机，先构造exchange对象
                Exchange exchange = new Exchange();
                exchange.setName(exchangeName);
                exchange.setType(exchangeType);
                exchange.setDurable(durable);
                exchange.setAutoDelete(autoDelete);
                exchange.setArguments(arguments);
                // 3. 把交换机对象写入硬盘
                if(durable) {
                    diskDataCenter.insertExchange(exchange);
                }
                // 4. 把交换机对象写入内存
                memoryDataCenter.insertExchange(exchange);
                System.out.println("[VirtualHost] 交互机创建完成，exchangeName=" + exchangeName);
                // 上述逻辑，先写硬盘，后写内存，因为硬盘更容易写失败， 如果硬盘写失败了，内存就不写了
                // 要是先写内存后写硬盘，如果内存写成功，硬盘写失败了，还需要把内存的数据给删除掉，就比较麻烦了
            }
            return true;
        } catch (Exception e) {
            // 创建交换机如果失败，就返回false
            System.out.println("[VirtualHost]交换机创建失败，exchangeName=" + exchangeName);
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除交换机
     * @param exchangeName
     * @return
     */
    public boolean exchangeDelete(String exchangeName) {
        exchangeName = virtualHostName + exchangeName;
        try {
            // 两个线程同时删除交换机，问题不大，但是一个线程要删除，一个线程要创建，就可能造成误删除
            synchronized (exchangeLocker) {
                // 1. 先找到对应的交换机
                Exchange toDelete = memoryDataCenter.getExchange(exchangeName);
                if(toDelete == null) {
                    throw new MqException("[VirtualHost]交换机不存在");
                }
                // 2. 删除硬盘上的数据
                if(toDelete.isDurable()) {
                    diskDataCenter.deleteExchange(exchangeName);
                }
                // 3. 删除内存上的数据
                memoryDataCenter.deleteExchange(exchangeName);
                System.out.println("[VirtualHost]交换机删除成功，exchangeName=" + exchangeName);
            }
            return true;
        } catch (MqException e) {
            // 出现异常在这里统一处理
            System.out.println("[VirtualHost]交换机删除失败，exchangeName=" + exchangeName);
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 创建队列
     * @param queueName
     * @param durable
     * @param exclusive
     * @param autoDelete
     * @param arguments
     * @return 创建成功返回true, 否则返回false
     */
    public boolean queueDeclare(String queueName, boolean durable, boolean exclusive, boolean autoDelete,
                        Map<String, Object> arguments ) {
        // 把队列名，拼接上虚拟主机名
        queueName = virtualHostName + queueName;
        try {
            // 两个线程都判定队列为null,如果创建的queueName都一样就可能造成对象覆盖
            synchronized (queueLocker) {
                // 1. 判定队列是否存在
                MSGQueue existsQueue = memoryDataCenter.getQueue(queueName);
                if(existsQueue != null) {
                    System.out.println("[VirtualHost] 队列已经存在, queueName" + queueName);
                    return true;
                }
                // 2. 创建队列对象
                MSGQueue queue = new MSGQueue();
                queue.setName(queueName);
                queue.setDurable(durable);
                queue.setExclusive(exclusive);
                queue.setAutoDelete(autoDelete);
                queue.setArguments(arguments);
                // 3. 先写入硬盘
                if (durable) {
                    diskDataCenter.insertQueue(queue);
                }
                // 4. 再写入内存
                memoryDataCenter.insertQueue(queue);
                System.out.println("[VirtualHost] 队列创建成功， queueName = " + queueName);
            }
            return true;
        } catch (IOException e) {
            System.out.println("[VirtualHost]创建队列失败 queueName " + queueName);
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除队列
     * @param queueName
     * @return
     */
    public boolean queueDelete(String queueName) {
        queueName = virtualHostName  + queueName;
        try {
            // 如果一个线程删除，一个线程创建队列，可能造成误删
            synchronized (queueName) {
                // 1. 查看是否存在队列
                MSGQueue toDelete = memoryDataCenter.getQueue(queueName);
                if(toDelete == null) {
                    throw new MqException("[VirtualHost] 队列不存在,queueName" + queueName);
                }
                // 2. 先删除硬盘数据
                if(toDelete.isDurable()) {
                    diskDataCenter.deleteQueue(queueName);
                }
                // 3. 删除内存数据
                memoryDataCenter.deleteQueue(queueName);
                System.out.println("[VirtualHost]删除队列成功 queueName=" + queueName);
            }
            return true;
        } catch (Exception e) {
            System.out.println("[VirtualHost] 创建队列失败，queueName=" + queueName);
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 绑定交换机和队列
     * @param queueName
     * @param exchangeName
     * @param bindingKey
     * @return
     */
    public boolean queueBind(String queueName, String exchangeName, String bindingKey) {
        queueName = virtualHostName + queueName;
        exchangeName = virtualHostName + exchangeName;
        try {
            // 因为绑定是否创建取决于交换机和队列，需要创建交换机对象，队列对象，因此需要加两把锁
            synchronized (exchangeLocker) {
                synchronized (queueLocker) {
                    // 1. 判断当前绑定是否存在
                    Binding existsBinding = memoryDataCenter.getBinding(exchangeName, queueName);
                    if(existsBinding != null) {
                        throw new MqException("[VirtualHost] existsBinding 已经存在! queueName=" + queueName
                                + ", exchangeName=" + exchangeName);
                    }
                    // 2. 验证binding是否合法
                    if(!router.checkBindingKey(bindingKey)) {
                        throw new MqException("[VirtualHost] bindingKey非法 bindingKey= " + bindingKey);
                    }
                    // 3. 创建Binding对象
                    Binding binding = new Binding();
                    binding.setExchangeName(exchangeName);
                    binding.setQueueName(queueName);
                    binding.setBindingKey(bindingKey);
                    // 4. 获取一下对应的交换机和队列，如果交换机或者队列不存在，这样的绑定是无法创建的
                    MSGQueue queue = memoryDataCenter.getQueue(queueName);
                    if(queue == null) {
                        throw new MqException("[virtualHost] 队列不存在！queueName=" + queueName);
                    }
                    Exchange exchange = memoryDataCenter.getExchange(exchangeName);
                    if(exchange == null) {
                        throw new MqException("[virtualHost] 交换机不存在 exchangeName=" + exchangeName);
                    }
                    // 5. 先写入硬盘
                    if(queue.isDurable() && exchange.isDurable()) {
                        diskDataCenter.insertBinding(binding);
                    }
                    // 6. 写入内存
                    memoryDataCenter.insertBinding(binding);
                }
            }
            System.out.println("[VirtualHost] 绑定创建成功! exchangeName=" + exchangeName
                    + ", queueName=" + queueName);
            return true;
        } catch (Exception e) {
            System.out.println("[VirtualHost] binding创建失败");
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 删除绑定
     * @param queueName
     * @param exchangeName
     * @return
     */
    public boolean queueUnBind(String queueName, String exchangeName) {
        queueName = virtualHostName + queueName;
        exchangeName = virtualHostName + exchangeName;
        try {
            // 要按顺序加锁，否则会造成循环依赖，死锁
            synchronized (exchangeLocker) {
                synchronized (queueLocker) {
                    // 1. 获取binding是否存在
                    Binding binding = memoryDataCenter.getBinding(exchangeName, queueName);
                    if(binding == null) {
                        throw new MqException("[VirtualHost] 删除绑定失败，绑定不存在，exchangeName=" + exchangeName
                        + ", queueName=" + queueName);
                    }
                    // 2. 无论绑定是否持久化，都尝试从硬盘上删除，就算不存在也没有副作用
                    // 这里就不校验队列/交换机是否存在了
                    // 如果要先删除交换机/队列再删除绑定，就类似于mysql的外键了，要判定当前交换机/队列是否存在对应的绑定了
                    diskDataCenter.deleteBinding(binding);
                    // 3. 删除内存中的数据
                    memoryDataCenter.deleteBinding(binding);
                }
            }
            System.out.println("[VirtualHost] 删除绑定成功");
            return true;
        } catch (Exception e) {
            System.out.println("[VirtualHost] 删除绑定失败");
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 发送消息到指定的交换机/队列中
     * @param exchangeName
     * @param routingKey
     * @param basicProperties
     * @param body
     * @return
     */
    public boolean basicPublish(String exchangeName, String routingKey, BasicProperties basicProperties, byte[] body) {
        try {
            // 1. 交换机名
            exchangeName = virtualHostName + exchangeName;
            // 2. 检查routingKey 是否合法
            if(!router.checkRoutingKey(routingKey)) {
                throw new MqException("[VirtualHost] routingKey非法, routingKey=" + routingKey);
            }
            // 3. 查找交换机对象
            Exchange exchange = memoryDataCenter.getExchange(exchangeName);
            if(exchange == null) {
                throw new MqException("[VirtualHost] 交换机不存在， exchangeName=" + exchangeName);
            }
            // 4. 判定交换机类型
            if(exchange.getType() == ExchangeType.DIRECT) {
                // 按照直接交换机的方式转发消息
                // 以routingKey作为队列名，直接将消息写入指定的队列中
                // 此时可以无视绑定关系
                String queueName = virtualHostName + routingKey;
                // 5. 构造消息对象
                Message message = Message.createMessageWithId(routingKey, basicProperties, body);
                // 6. 查找队列名对应的队列对象
                MSGQueue queue = memoryDataCenter.getQueue(queueName);
                if(queue == null) {
                    throw new MqException("[virtualHost] 队列不存在， queueName= " + queueName);
                }
                // 7.队列存在，直接给队列发送消息
                sendMessage(queue, message);
            } else {
                // 按照fanout和topic的方式转发
                // 5. 找到该交换机的所有绑定， 遍历这些绑定对象
                ConcurrentHashMap<String, Binding> bindingsMap = memoryDataCenter.getBindings(exchangeName);
                for (Map.Entry<String, Binding> entry:
                    bindingsMap.entrySet()){
                    // 1)获取到绑定对象，判定队列是否存在
                    Binding binding = entry.getValue();
                    MSGQueue queue = memoryDataCenter.getQueue(binding.getQueueName());
                    if(queue == null) {
                        // 此处就不抛出异常了，因为此处可能又很多队列
                        // 不能因为一个队列的失败，影响其他队列的消息传输
                        System.out.println("[VirtualHost] basicPublish发送消息时，队列不存在，queueName=" + binding.getQueueName());
                        continue;
                    }
                    // 2) 构造消息对象
                    Message message = Message.createMessageWithId(routingKey, basicProperties, body);
                    // 3) 判定这个消息是否能转发给该队列
                    // 如果是fanout，所有绑定的队列都要转发
                    // 如果是topic，需要判定bindingKey和routingKey是否匹配
                    if(!router.route(exchange.getType(), binding, message)) {
                        continue;
                    }
                    // 4) 真正转发消息给队列（如果是fanout一定能转发）
                    sendMessage(queue, message);
                }
            }
            return true;
        } catch (Exception e) {
            System.out.println("[VirtualHost] 发送消息失败");
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 发送消息，就是把消息写入硬盘和内存中
     * @param queue
     * @param message
     */
    private void sendMessage(MSGQueue queue, Message message) throws IOException, MqException, InterruptedException {
        // 查看该消息是否持久化
        int deliverMode = message.getDeliverMode();
        // deliverMode = 1不持久化， deliverMode=2持久化
        if(deliverMode == 2) {
            diskDataCenter.sendMessage(queue, message);
        }
        // 写入内存
        memoryDataCenter.sendMessage(queue, message);
        //  通知消费者可以消费消息了
        consumerManager.notifyConsume(queue.getName());
    }

    /**
     * 订阅消息
     * 添加一个队列的订阅者，当队列收到消息后，就要把消息推送给对应的订阅者
     * @param consumerTag 消费者的身份标识
     * @param queueName 要订阅的队列名
     * @param autoAck 消息消费完成后的应答方式，为true是自动应答，为false是手动应答
     * @param consumer 回调函数，此处设定为函数式接口，这样后续调用basicConsume并且传实参的时候，就可以写做lambda样子
     * @return
     */
    public boolean basicConsume(String consumerTag, String queueName, boolean autoAck, Consumer consumer) {
        //
        queueName = virtualHostName + queueName;
        try {
            consumerManager.addConsumer(consumerTag, queueName, autoAck, consumer);
            System.out.println("[VirtualHost] basicConsume成功，queueName=" + queueName);
            return true;
        } catch (MqException e) {
            System.out.println("[VirtualHost] basicConsume失败 queueName=" + queueName);
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 当消费者的autoAck=false，手动应答，需要消费者这边，自己再回调方法内部，显示的调用basicAck
     * @param queueName
     * @param messageId
     * @return
     */
    public boolean basicAck(String queueName, String messageId) {
        queueName = virtualHostName + queueName;
        try {
            // 1. 获取到消息和队列
            Message message = memoryDataCenter.getMessage(messageId);
            if(message == null) {
                throw new MqException("[VirtualHost] 要确认的消息不存在,messageId=" + messageId);
            }
            MSGQueue queue = memoryDataCenter.getQueue(queueName);
            if(queue == null) {
                throw new MqException("[VirtualHost] 要确认的消息的队列不存在， queueName=" + queueName);
            }
            // 2. 删除硬盘上的数据
            if(message.getDeliverMode() == 2) {
                diskDataCenter.deleteMessage(queue, message);
            }
            // 3. 删除消息中心的数据
            memoryDataCenter.removeMessage(messageId);
            // 4. 删除待确认的集合中的数据
            memoryDataCenter.removeMessageWaitAck(queueName,messageId);
            System.out.println("[VirtualHost] basicAck 成功! 消息被成功确认! queueName=" + queueName
                    + ", messageId=" + messageId);
            return true;
        } catch (Exception e) {
            System.out.println("[VirtualHost] basicAck 失败! 消息确认失败! queueName=" + queueName
                    + ", messageId=" + messageId);
            e.printStackTrace();
            return false;
        }
    }


}
