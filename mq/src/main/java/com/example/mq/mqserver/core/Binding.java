package com.example.mq.mqserver.core;

import lombok.Data;

/**
 * 表示队列和交互机之间的关联关系
 */
@Data
public class Binding {
    private String exchangeName;
    private String queueName;
    // bindingKey类似于暗号，用于topic消息的接口暗号
    private String bindingKey;
    // Binding依附于exchange和queue，而对于持久化如果exchange和queue任何一个没有持久化，
    // 此时binding没有持久化的意义
}
