package com.example.mq.mqserver.mapper;

import com.example.mq.mqserver.core.Binding;
import com.example.mq.mqserver.core.Exchange;
import com.example.mq.mqserver.core.MSGQueue;
import org.apache.ibatis.annotations.Mapper;

import javax.xml.bind.Binder;
import java.util.List;

@Mapper
public interface MetaMapper {
    // 将队列，交换机，绑定存储到sqlite中
    // 将消息存储到文件中
    // 因为消息不需要频繁的进行crud操作
    void createExchangeTable();
    void createQueueTable();
    void createBindingTable();

    // 针对上述三个基本概率，进行插入，删除，查询
    void insertExchange(Exchange exchange);
    void deleteExchange(String exchangeName);
    List<Exchange> selectAllExchanges();
    void insertQueue(MSGQueue queue);
    void deleteQueue(String queueName);
    List<MSGQueue> selectAllQueues();
    void insertBinding(Binding binding);
    // 对于绑定来说，没有主键，删除操作可以通过exchangeName和queueName来筛选
    void deleteBinding(Binding binding);
    List<Binding> selectAllBindings();
}
