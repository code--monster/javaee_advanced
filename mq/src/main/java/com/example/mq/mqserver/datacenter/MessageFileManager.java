package com.example.mq.mqserver.datacenter;

import com.example.mq.common.BinaryTool;
import com.example.mq.common.MqException;
import com.example.mq.mqserver.core.MSGQueue;
import com.example.mq.mqserver.core.Message;

import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * 通过这个类，来对存储在文件中的消息进行管理
 * Message存储在硬盘上的原因：
 * 1. 消息的操作不涉及复杂的crud
 * 2. 消息的数量非常多，数据库的访问效率不高
 * 设定消息在文件中俺如下方式存储：
 * 1. 消息依附于队列，因此存储的时候，就把消息按照队列的维度展开
 * 在data中创建子目录，每个队列就是一个子目录，子目录的名字就是队列名
 * data/queueName/
 * 2. 每个队列的子目录下，在分配两个文件用来存储消息
 *   第一个文件：queue_data.txt，用来保存消息的内容
 *   第二个文件：queue_stat.txt 用来保存消息的统计信息
 */
public class MessageFileManager {
    /**
     * 定义静态内部类，表示队列的统计信息
     * Stat类直接使用成员即可，不用搞get,set
     */
    public static class Stat {
        public int totalCount; //消息总数
        public int validCount; // 有效消息数
    }

    public void init() {
        // 暂时不做额外的初始化工作，以备后续扩展
    }

    /**
     * 用来消息文件所在的目录和文件名
     * 获取到指定队列对应消息文件所在位置
     * @param queueName 队列名
     * @return 返回目录路径
     */
    private String getQueueDir(String queueName) {
        return "./data/" + queueName;
    }

    /**
     * 用来获取队列的消息数据文件路径
     * 该文件是二进制文件
     * @param queueName 队列名
     * @return 返回数据文件的地址
     */
    private String getQueueDataPath(String queueName) {
        return getQueueDir(queueName) + "/queue_data.txt";
    }

    /**
     * 获取该队列的消息统计文件的路径
     * @param queueName 队列名
     * @return
     */
    private String getQueueStatPath(String queueName) {
        return getQueueDir(queueName) + "/queue_stat.txt";
    }

    /**
     * 读取队列的消息统计文件
     * @param queueName
     * @return 统计队列消息的对象
     */
    private Stat readStat(String queueName) {
        // 由于当前的消息统计文件是文本文件，可以直接使用Scanner来读取文件内容
        Stat stat = new Stat();
        try(InputStream inputStream = new FileInputStream(getQueueStatPath(queueName))) {
            Scanner scanner = new Scanner(inputStream);
            stat.totalCount = scanner.nextInt();
            stat.validCount = scanner.nextInt();
            return stat;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 写入队列的 消息统计文件中
     * @param queueName
     */
    private void writeStat(String queueName, Stat stat) {
        // 文本文件，使用PrintWrite写文件
        // OutputStream 打开文件，默认情况下，会直接原文件清空，此时相当于新的数据覆盖旧的数据
        try(OutputStream outputStream = new FileOutputStream(getQueueStatPath(queueName))) {
            PrintWriter printWriter = new PrintWriter(outputStream);
            printWriter.write(stat.totalCount + "\t" + stat.validCount);
            printWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 创建队列对应的文件和目录
     * @param queueName
     */
    public void createQueueFiles(String queueName) throws IOException {
        // 1. 先创建队列对应的消息目录
        File baseDir = new File(getQueueDir(queueName));
        if(!baseDir.exists()) {
            // 不存在，就创建这个目录
            boolean ok = baseDir.mkdirs();
            if(!ok) {
                throw new IOException("创建目录失败，baseDir=" + baseDir.getAbsolutePath());
            }
        }
        // 2. 创建队列数据文件
        File queueDataFile = new File(getQueueDataPath(queueName));
        if(!queueDataFile.exists()) {
            boolean ok = queueDataFile.createNewFile();
            if(!ok) {
                throw new IOException("创建文件失败 queueDataFile=" + queueDataFile.getAbsolutePath());
            }
        }
        // 3. 创建消息统计文件
        File queueStatFile = new File(getQueueStatPath(queueName));
        if(!queueStatFile.exists()) {
            boolean ok = queueStatFile.createNewFile();
            if(!ok) {
                throw new IOException("创建文件失败 queueStatFile=" + queueStatFile.getAbsolutePath());
            }
        }
        // 4. 给 消息统计文件 设置初始值
        Stat stat = new Stat();
        stat.totalCount = 0;
        stat.validCount = 0;
        writeStat(queueName, stat);
    }

    /**
     * 删除队列的目录和文件
     * 当队列被删除后，对应的消息文件，也要跟着删除
     * @param queueName
     */
    public void destroyQueueFiles(String queueName) throws IOException {
        // 先删除文件，再删除目录
        File queueDataFile = new File(getQueueDataPath(queueName));
        boolean ok1 = queueDataFile.delete();
        File queueStatFile = new File(getQueueStatPath(queueName));
        boolean ok2 = queueStatFile.delete();
        File baseDir = new File(getQueueDir(queueName));
        boolean ok3 = baseDir.delete();
        if(!ok1 || !ok2 || !ok3) {
            throw new IOException("删除队列目录和文件失败 baseDir=" + baseDir.getAbsolutePath());
        }
    }

    /**
     * 检查队列的目录和文件是否存在
     * 后续有生产者给broker server 生产消息了，这个消息可能需要记录到文件中（取决于是否需要持久化）
     * @param queueName
     * @return
     */
    public boolean checkFilesExists(String queueName) {
        // 判定队列的数据文件和统计文件是否都存在
        File queueDataFile = new File(getQueueDataPath(queueName));
        if(!queueDataFile.exists()) {
            return false;
        }
        File queueStatFile = new File(getQueueStatPath(queueName));
        if(!queueStatFile.exists()) {
            return false;
        }
        return true;
    }

    /**
     * 把一个新的消息，放到队列对应的文件中
     * @param queue 要放入的队列
     * @param message 要写入的消息
     */
    public void sendMessage(MSGQueue queue, Message message) throws MqException, IOException {
        // 1. 检查一下当前写入的队列对应的文件是否存在
        if(! checkFilesExists(queue.getName())) {
            throw new MqException("[MessageFileManager] 队列对应的文件不存在，queueName= " + queue.getName());
        }
        // 2. 把Message对象，进行序列化，转成二进制的字节数组
        byte[] messageBinary = BinaryTool.toBytes(message);
        // 对不同的队列进行加锁，防止出现多个生产者同时向同一个队列生产消息的，造成线程不安全
        synchronized (queue) {
            // 3. 先获取到当前的队列数据文件的长度，用这个来计算出该Message的offsetBeg和offsetEnd
            // 把新的message数据，写入到队列数据文件的末尾，此时message对象的offsetBeg=当前文件长度+4
            // offsetEnd = 当前文件长度 + 4 + message的自身长度
            File queueDataFile = new File(getQueueDataPath(queue.getName()));
            // 通过queueDataFile.length()就能获取到文件的长度，单位字节
            message.setOffsetBeg(queueDataFile.length() + 4);
            message.setOffsetEnd(queueDataFile.length() + 4 + messageBinary.length);
            // 4. 将消息写入数据文件中，采用追加的方式
            try(OutputStream outputStream = new FileOutputStream(queueDataFile, true)) {
                // 把消息写入文件中约定，一条消息由两部分组成
                // 1. 4个字节描述消息的长度
                // 2. 消息的内容
                // 这就有一个问题，怎么样将int的按照字节依次写入文件中呢？？
                // 使用DataOutputStream
                try(DataOutputStream dataOutputStream = new DataOutputStream(outputStream)) {
                    // 按照4个字节的方式将长度写入文件中
                    dataOutputStream.writeInt(messageBinary.length);
                    // 写入消息体
                    dataOutputStream.write(messageBinary);
                }
            }
            // 5. 更新消息统计文件
            Stat stat = readStat(queue.getName());
            stat.totalCount += 1;
            stat.validCount += 1;
            writeStat(queue.getName(), stat);
        }
    }

    /**
     * 逻辑删除消息，即把硬盘存储的消息中的isValid属性设置为Ox1
     * 1. 先把文件中的这段数据读出来，还原为Message对象
     * 2. 把isValid改为0
     * 3. 把上述数据重新写回文件中
     * @param queue
     * @param message
     */
    public void deleteMessage(MSGQueue queue, Message message) throws IOException, ClassNotFoundException {
        // 多个消费者同时对同一个队列进行消费，会出现线程安全问题，需要加锁
        synchronized (queue) {
            // FileOutputStream/FileInputStream都是从文件头开始读写
            // 而此处我们需要在文件的指定位置进行随机访问（内存支持随机访问，硬盘也支持随机访问）
            // 此处使用RandomAccessFile,“rw"表示可读可写
            try (RandomAccessFile randomAccessFile = new RandomAccessFile(getQueueDataPath(queue.getName()),"rw")) {
                // 1. 先从文件汇总读取对应的message数据
                // 先准备好message大小的空间
                byte[] bufferSrc = new byte[(int) (message.getOffsetEnd() - message.getOffsetBeg())];
                // 通过seek方法将光标移动到offsetBeg位置
                randomAccessFile.seek(message.getOffsetBeg());
                // 从offsetBeg开始读，直到bufferSrc满了
                randomAccessFile.read(bufferSrc);
                // 2. 把当前读出来的二进制数据，转换成Message对象
                Message diskMessage = (Message) BinaryTool.fromBytes(bufferSrc);
                // 3. 把isValid设置为无效
                diskMessage.setIsValid((byte) 0x0);
                // 此处不需要给参数的message的isValid设为0，因为这个参数代表的是内存中管理的message对象
                // 而这个对象马上也要从内存中销毁了
                // 4. 重新写入文件
                byte[] bufferDest = BinaryTool.toBytes(diskMessage);
                // 由于上面进行seek操作，现在光标在offsetEnd位置，现在需要让光标回到offsetBeg的位置
                randomAccessFile.seek(message.getOffsetBeg());
                randomAccessFile.write(bufferDest);
            }
            // 删除完消息，要更新统计文件
            Stat stat = readStat(queue.getName());
            if(stat.validCount > 0) {
                stat.validCount -= 1;
            }
            writeStat(queue.getName(), stat);
        }
    }

    /**
     * 从文件中读取出所有消息内容，加载到内存中（即放入链表中）
     * 这个方法是在程序启动的时候进行调用
     * 这里使用链表而不使用顺序表是因为链表的头插头删时间复杂度O（1)
     * @param queueName 使用队列名而不是队列，是因为该方法不需要对队列加锁
     * @return
     */
    public LinkedList<Message> loadAllMessageFromQueue(String queueName) throws IOException, MqException, ClassNotFoundException {
        LinkedList<Message> messages = new LinkedList<>();
        try(InputStream inputStream = new FileInputStream(getQueueDataPath(queueName))) {
            try(DataInputStream dataInputStream = new DataInputStream(inputStream)) {
                // 这个变量记录当前文件光标
                long currentOffset = 0;
                // 一个文件中包含很多消息，此处需要进行循环读取
                while(true) {
                    // 1. 根据消息的存储格式，先读取第一部分描述当前消息的长度，使用readInt才能读取4个字节
                    // readInt方法，读到文件末尾，会抛出EOFException异常，捕获这个异常作为循环退出的条件
                    int messageSize = dataInputStream.readInt();
                    // 2. 根据这个长度去读取消息内容
                    byte[] buffer = new byte[messageSize];
                    int actualSize = dataInputStream.read(buffer);
                    if(messageSize != actualSize) {
                        // 如果不匹配，说明文件格式错乱
                        throw new MqException("[MessageFileManager]文件格式错乱， queueName " +  queueName);
                    }
                    // 3. 把这个读到的二进制数据，反序列化成Message对象
                    Message message = (Message) BinaryTool.fromBytes(buffer);
                    // 4. 判定一下这个消息对象，是不是无效对象
                    if(message.getIsValid() != 0x1) {
                        // 无效数据，直接跳过
                        // offset要进行更新
                        currentOffset += (4 + messageSize);
                        continue;
                    }
                    // 5. 有效数据，需要把这个message对象加入链表中
                    // 写入之前需要重新计算offsetBeg和offsetEnd
                    // 进行offset计算时，需要知道当前文件的光标位置，dataInputStream没办法做到
                    // 使用currentOffset进行计算
                    message.setOffsetBeg(currentOffset + 4);
                    message.setOffsetEnd(currentOffset + 4 + messageSize);
                    currentOffset += (4 + messageSize);
                    messages.add(message);
                }
            }catch (EOFException e) {
                // 此处不算异常情况，而是预期得到的结果
                System.out.println("[MessageFileManager]恢复Message数据完成");
            }
        }
        return messages;
    }

    /**
     * 检查是否需要对队列的消息数据文件进行GC
     * @param queueName
     * @return
     */
    public boolean checkGC(String queueName) {
        // 如果总消息数 > 2048 && 有效数据/总消息数 < 50%就进行GC
        Stat stat = new Stat();
        if(stat.totalCount > 2048 && ((double)stat.validCount / (double) stat.totalCount) < 0.5) {
            return true;
        }
        return false;
    }

    /**
     * 垃圾回收采用复制算法，将有效消息复制到新文件中
     * @param queueName
     * @return
     */
    private String getQueueDataNewPath(String queueName) {
        return getQueueDir(queueName) + "/queue_data_new.txt";
    }

    /**
     * 通过复制算法完成垃圾回收，将有效消息复制到queue_data_new.txt
     * 删除旧文件，再把新的文件格式改回queue_data.txt
     * 同时更新消息统计文件
     * 如果某个队列中的消息特别多，而且大部分是有效消息，此时搬运成本非常高，容易造成阻塞
     * 解决方案：把一个大的文件，拆分成若干小文件
     * 文件拆分：当单个文件长度达到阈值之后，就会拆分成两个文件
     * 文件合并：每个单独文件进行GC之后，如果文件小了很多，就和相邻的其他文件合并
     * 实现思路：
     * 1. 需要专门的数据结构，来存储当前队列中有多少个数据文件，每个文件大小是多少，消息数目，有效消息数，无效消息数
     * 2. 设计策略，什么时候触发文件拆分，什么时候触发文件合并
     * @param queue
     */
    public void gc(MSGQueue queue) throws MqException, IOException, ClassNotFoundException {
        // 进行gc的时候，是针对消息数据文件进行大洗牌，这个过程，其他线程不能针对该队列的消息文件做任何修改
        synchronized (queue) {
            // 由于gc操作比较耗时，此处统计一下执行消耗的时间
            long gcBeg = System.currentTimeMillis();
            // 1. 创建一个新文件
            File queueDataNewFile = new File(getQueueDataNewPath(queue.getName()));
            if(queueDataNewFile.exists()) {
                // 说明上一次gc没执行到把新文件改名，程序就意外崩溃了
                throw new MqException("[MessageFileManager]gc时发现queue_data_new已经存在 queueName= " + queue.getName());
            }
            boolean ok = queueDataNewFile.createNewFile();
            if(!ok) {
                throw new MqException("[MessageFileManager]创建文件失败，queueDataNewFile=" + queueDataNewFile.getAbsolutePath());
            }
            // 2. 从旧文件中读取出所有的有效消息对象
            LinkedList<Message> messages = loadAllMessageFromQueue(queue.getName());
            // 3. 把有效消息，写入新的文件中
            try(OutputStream outputStream = new FileOutputStream(queueDataNewFile)) {
                try(DataOutputStream dataOutputStream = new DataOutputStream(outputStream)) {
                    for (Message message :
                            messages) {
                        byte[] buffer = BinaryTool.toBytes(message);
                        dataOutputStream.writeInt(buffer.length);
                        dataOutputStream.write(buffer);
                    }
                }
            }
            // 4. 删除旧文件，并且把新文件进行重命名
            File queueDataOldFile = new File(getQueueDataPath(queue.getName()));
            ok = queueDataOldFile.delete();
            if(!ok) {
                throw new MqException("[MessageFileManager]删除旧的数据文件失败，queueDataOldFile=" + queueDataOldFile.getAbsolutePath());
            }
            ok = queueDataNewFile.renameTo(queueDataOldFile);
            if(!ok) {
                throw new MqException("[MessageFileManager] 文件重命名失败! queueDataNewFile=" + queueDataNewFile.getAbsolutePath()
                        + ", queueDataOldFile=" + queueDataOldFile.getAbsolutePath());
            }
            // 5. 更新统计文件
            Stat stat = readStat(queue.getName());
            stat.totalCount = messages.size();
            stat.validCount = messages.size();
            writeStat(queue.getName(), stat);
            long gcEnd = System.currentTimeMillis();
            System.out.println("[MessageFileManager] gc 执行完毕! queueName=" + queue.getName() + ", time="
                    + (gcEnd - gcBeg) + "ms");
        }
    }
}
