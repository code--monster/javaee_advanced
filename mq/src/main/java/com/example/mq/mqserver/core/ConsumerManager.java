package com.example.mq.mqserver.core;

import com.example.mq.common.Consumer;
import com.example.mq.common.ConsumerEnv;
import com.example.mq.common.MqException;
import com.example.mq.mqserver.VirtualHost;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 通过这个类来实现消费消息的核心逻辑
 * 1. 让broker server把有哪些消费者管理好
 * 2. 收到对应的消息，把消息推送给消费者
 * 具体实现思路：
 * 1. 令牌队列（阻塞队列），当某个队列中添加了消息，就把该队列名写入阻塞队列中
 * 2. 扫描线程，只要盯着令牌队列即可，拿到队列中的令牌（队列名）就能找到对应的队列
 * 3. 线程池：取到队列的消息后，让线程池消费消息，即执行对应消费者的回调函数
 * 消费者在最初订阅消息的时候，就把回调注册给broker server,这个回调的内容是消费者决定的，具体里面干啥取决于消费者的业务逻辑
 * 为什么搞了扫描线程，还要搞线程池？？直接让扫描线程执行回调不行吗？？
 * 答：由于消费者给出的回调，具体干啥不知道，可能比较耗时
 * 如果只有一个线程，可能周转不开
 * 为什么要搞令牌队列？？？
 * 答：如果直接让扫描线程遍历循环所有队列，有的队列没有消息，可能会发生空转，
 * 引入阻塞队列，当有消息添加到任意一个队列中，就把该队列名记录下来，扫描线程只要盯着阻塞队列即可
 */
public class ConsumerManager {
    // 持有上层的VirtualHost对象的引用，用来操作数据
    private VirtualHost parent;
    // 指定一个线程池，负责去执行具体的回调任务
    private ExecutorService workerPool = Executors.newFixedThreadPool(4);
    // 存放令牌的队列
    private BlockingQueue<String> tokenQueue = new LinkedBlockingQueue<>();
    // 扫描线程
    private Thread scannerThread = null;

    public ConsumerManager(VirtualHost p) {
        this.parent = p;

        scannerThread = new Thread( () -> {
            while(true) {
                try{
                    // 1. 拿到令牌
                    String queueName = tokenQueue.take();
                    // 2. 根据令牌，找到队列
                    MSGQueue queue = parent.getMemoryDataCenter().getQueue(queueName);
                    if(queue == null) {
                        throw new MqException("[ConsumerManger] 取到令牌后发现，该队列名不存在 queueName" + queueName);
                    }
                    // 3. 从这个队列中消费一个消息
                    synchronized (queue) {
                        consumeMessage(queue);
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 这个方法调用时机就是发送消息的时候
     * @param queueName
     */
    public void notifyConsume(String queueName) throws InterruptedException {
        tokenQueue.put(queueName);
    }

    /**
     * 添加消费者到指定的队列中，如果队列中有消息，立马进行消费
     * @param consumerTag
     * @param queueName
     * @param autoAck
     * @param consumer
     * @throws MqException
     */
    public void addConsumer(String consumerTag, String queueName, boolean autoAck, Consumer consumer) throws MqException {
        // 找到对应的队列
        MSGQueue queue = parent.getMemoryDataCenter().getQueue(queueName);
        if(queue == null) {
            throw new MqException("[ConsumerManager] 队列不存在，queueName=" + queueName);
        }
        ConsumerEnv consumerEnv = new ConsumerEnv(consumerTag, queueName, autoAck, consumer);
        synchronized (queue) {
            queue.addConsumerEnv(consumerEnv);
            // 如果当前队列中已经有了一些消息，需要立即消费掉
            int n = parent.getMemoryDataCenter().getMessageCount(queueName);
            for (int i = 0; i < n; i++) {
                // 调用一次就消费一条消息
                consumeMessage(queue);
            }
        }
    }

    /**
     * 消费消息
     * @param queue
     */
    private void consumeMessage(MSGQueue queue) {
        // 1. 按照轮询的方式，找一个消费者出来
        ConsumerEnv luckyDog = queue.chooseConsumer();
        if(luckyDog == null) {
            // 该队列没有消费者订阅，暂时不消费，等有消费者再说
            return;
        }
        // 2. 从队列中取出一个消息
        Message message = parent.getMemoryDataCenter().pollMessage(queue.getName());
        if(message == null) {
            // 当前队列没有消息，不需要消费
            return;
        }
        // 3. 把消息放入消费者的回调函数中，丢给线程池处理
        workerPool.submit(() -> {
            try{
                // 1.消息被正确消费完（在消费者的回调函数中，顺利执行结束，没抛异常）
                // 为了保障消息不丢失，在真正回调之前，先把消息放入“待确认集合”中
                // 避免因为回调失败，导致消息丢失
                parent.getMemoryDataCenter().addMessageWaitAck(queue.getName(),message);
                // 2. 真正执行回调操作
                luckyDog.getConsumer().handleDelivery(luckyDog.getConsumerTag(), message.getBasicProperties(),
                        message.getBody());
                // 3. 如果当前是自动应答，就可以直接把消息删除了
                // 如果是手动应答，则先不处理，交给消费者后续调用basicAck方法来处理
                if(luckyDog.isAutoAck()) {
                    // 1. 删除硬盘上的消息
                    if(message.getDeliverMode() == 2) {
                        parent.getDiskDataCenter().deleteMessage(queue,message);
                    }
                    // 2. 删除上面的待确认集合中的消息
                    parent.getMemoryDataCenter().removeMessageWaitAck(queue.getName(), message.getMessageId());
                    // 3. 删除内存中消息中心里的消息
                    parent.getMemoryDataCenter().removeMessage(message.getMessageId());
                    System.out.println("[ConsumerManager] 消息被成功消费了，queueName= " + queue.getName());
                }
            }catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
    /*
    执行回调方法抛异常，此时消息会保存在待确认的集合中，rabbitMq的做法是再搞一个扫描线程，负责关注这个待确认的集合
    每个待确认的消息待多久了，如果超过时间范围，就会把这个消息放入一个待定的队列“死信队列”
     */
    /*
    如果执行回调过程中，broker server崩溃了，内存数据全没了
    但是硬盘数据还在，重启以后，消息又被加载会内存中，就跟没有消费过一样
    消费者就有机会重新消费这个消息了
     */
}
