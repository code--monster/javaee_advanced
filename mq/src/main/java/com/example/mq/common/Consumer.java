package com.example.mq.common;

import com.example.mq.mqserver.core.BasicProperties;

import java.io.IOException;

/**
 * 函数式接口，lambda的底层实现
 * 1. interface
 * 2. 只能一个方法
 * 3. 需要添加@FunctionalInterface
 */
@FunctionalInterface
public interface Consumer {
    void handleDelivery(String consumerTag, BasicProperties basicProperties, byte[] body) throws MqException, IOException;
}
