package com.example.mq.common;

import lombok.Data;

import java.io.Serializable;
// 订阅消息
@Data
public class BasicConsumeArguments extends BasicArguments implements Serializable {
    private String consumerTag;
    private String queueName;
    private boolean autoAck;
    // 这个类对应basicConsume方法中还有一个参数，就是Consumer回调函数，用来处理消息
    // 而这个回调函数，是不能通过网络传输的
    // 站在broker server角度，针对消息的处理是统一的，即把消息返回给客服端
    // 客服端收到消息后，在客服端这边执行一个用户自定义的回调就行
    // 此时，客户端不需要把自身的回调告诉服务器，所以不需要consumer成员
}
