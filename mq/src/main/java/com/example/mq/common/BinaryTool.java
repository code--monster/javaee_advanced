package com.example.mq.common;

import java.io.*;

/**
 * 该类不仅可以序列化Message,其他java对象也可以通过这样的逻辑进行序列化和反序列化
 * 如果想让这个对象进行序列化和反序列化，需要让这个对象实现Serializable接口
 * 我们使用java标准库中提供的序列化方案：ObjectInputStream和ObjectInputStream
 */
public class BinaryTool {
    /**
     * 将一个对象序列化为一个字节数组
     * @param object
     * @return
     */
    public static byte[] toBytes(Object object) throws IOException {
        // 这个流对象相当于一个变成的字节数组
        // 可以把object序列化的数据给逐渐写入到byteArrayOutPutStream中
        try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()) {
            try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream)) {
                // 此处的writeObject就会把对象进行序列化，生成二进制字节数据，就会写入到objectOutputStream
                // objectOutputStream又关联到ByteArrayOutputStream，最终结果会写到ByteArrayOutputStream
                objectOutputStream.writeObject(object);
            }
            return byteArrayOutputStream.toByteArray();
        }
    }

    /**
     * 将字节数组，反序列化为一个对象
     * @param bytes
     * @return
     */
    public static Object fromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        Object object = null;
        try(ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes)) {
            try(ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream)) {
                // 此处的readObject,就是从bytes这个byte[]中读取数据，并进行反序列化
                object = objectInputStream.readObject();
            }
        }
        return object;
    }
}
