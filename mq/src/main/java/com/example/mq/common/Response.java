package com.example.mq.common;

import lombok.Data;

/**
 * 返回的响应，根据自定义应用层协议组织
 */
@Data
public class Response {
    private int type;
    private int length;
    private byte[] payload;
}
