package com.example.mq.common;

import lombok.Data;

/**
 * 表示一个网络通信中的请求对象，按照自定义应用层协议格式来组织
 * 0x1: 创建channel
 * 0x2: 关闭channel
 * 0x3: 创建exchange
 * 0x4: 销毁exchange
 * 0x5: 创建queue
 * 0x6: 销毁queue
 * 0x7: 创建binding
 * 0x8: 销毁binding
 * 0x9: 发送Message
 * 0xa: 订阅message
 * 0xb: 返回ack
 * 0xc：服务端给客服端推送的消息，（被订阅的消息）响应独有的
 */
@Data
public class Request {
   private int type;
   private int length;
   private byte[] payload;
}
