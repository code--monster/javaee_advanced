package com.example.mq.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 使用这个类表示方法的公共参数/辅助字段
 * 后续每个方法都会有一些不同的参数，不同的参数再分别使用不同的类表示
 */
@Data
public class BasicArguments implements Serializable {
    // 表示一个请求/响应的身份标识，可以用来把请求和响应对上
    protected String rid;
    // 此次通信使用的channel的身份标识
    protected String channelId;

}
