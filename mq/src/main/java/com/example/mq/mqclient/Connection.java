package com.example.mq.mqclient;

import com.example.mq.common.*;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 一个tcp连接
 */
public class Connection {
    private Socket socket = null;

    // 一个连接中有多个channel,使用哈希表进行管理
    private ConcurrentHashMap<String, Channel> channelMap = new ConcurrentHashMap<>();
    // 输入输出流对象
    private InputStream inputStream;
    private OutputStream outputStream;
    private DataInputStream dataInputStream;
    private DataOutputStream dataOutputStream;

    private ExecutorService callbackPool = null;

    public Connection(String host, int port) throws IOException {
        socket = new Socket(host, port);
        inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();
        dataInputStream = new DataInputStream(inputStream);
        dataOutputStream = new DataOutputStream(outputStream);

        callbackPool = Executors.newFixedThreadPool(4);
        // 创建一个扫描线程，由这个线程不停的从socket中读取响应数据，把这个响应数据再交付给对应的channel
        Thread t = new Thread(() -> {
            try {
                while(!socket.isClosed()) {
                    Response response = readResponse();
                    // 把响应交给对应的channel处理
                    dispatchResponse(response);
                }
            }catch (SocketException e) {
                // 连接正常断开，不算异常
                System.out.println("[Connection] 连接正常断开");
            }
            catch (IOException | ClassNotFoundException | MqException e) {
                System.out.println("[Connection] 连接异常断开");
                e.printStackTrace();
            }
        });
        t.start();
    }

    /**
     * 根据response.type的不同，对响应进行处理
     * @param response
     * @throws IOException
     * @throws ClassNotFoundException
     */
    private void dispatchResponse(Response response) throws IOException, ClassNotFoundException, MqException {
        if(response.getType() == 0xc) {
            // 服务器推送过来的消息数据
            SubScribeReturns subScribeReturns = (SubScribeReturns) BinaryTool.fromBytes(response.getPayload());
            // 根据channelId找到对应的channel对象
            Channel channel = channelMap.get(subScribeReturns.getChannelId());
            if(channel == null) {
                throw new MqException("[Connection] 该消息对应的channel在客服端不存在，channelId=" + channel.getChannelId());
            }
            // 执行该channel对象内部的回调
            callbackPool.submit(() -> {
                try {
                    channel.getConsumer().handleDelivery(subScribeReturns.getConsumerTag(), subScribeReturns.getBasicProperties(),
                            subScribeReturns.getBody());
                } catch (MqException | IOException e) {
                    e.printStackTrace();
                }
            });
        }else {
            // 当前响应是针对刚才请求的响应
            BasicReturns basicReturns = (BasicReturns) BinaryTool.fromBytes(response.getPayload());
            // 根据channelId找到对应的channel对象
            Channel channel = channelMap.get(basicReturns.getChannelId());
            if(channel == null) {
                throw new MqException("[Connection] 该消息对应的channel在客服端不存在，channelId=" + channel.getChannelId());
            }
            // 服务端返回响应，需要写入hash表，并唤醒还在阻塞等待channel
            channel.putReturns(basicReturns);
        }
    }

    /**
     * 关闭connection
     */
    public void close() {
        try{
            callbackPool.shutdownNow();
            channelMap.clear();
            inputStream.close();
            outputStream.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * 发送请求
     * @param request 网络通信中的请求对象
     */
    public void writeRequest(Request request) throws IOException {
        dataOutputStream.writeInt(request.getType());
        dataOutputStream.writeInt(request.getLength());
        dataOutputStream.write(request.getPayload());
        dataOutputStream.flush();
        System.out.println("[Connection] 发送请求。 type=" + request.getType() + ", length= " + request.getLength());
    }

    /**
     * 读取响应
     * @return
     */
    public Response readResponse() throws IOException {
        Response response = new Response();
        response.setType(dataInputStream.readInt());
        response.setLength(dataInputStream.readInt());
        byte[] payload = new byte[response.getLength()];
        int n = dataInputStream.read(payload);
        if(n != response.getLength()) {
            throw new IOException("读取响应的数据不完整");
        }
        response.setPayload(payload);
        System.out.println("[Connection] 收到响应。 type= " + response.getType() + ", length=" + response.getLength());
        return response;
    }

    /**
     * 通过这个方法，在connection中创建一个channel
     * @return
     */
    public Channel createChannel() throws IOException {
        String channelId = "C-" + UUID.randomUUID().toString();
        Channel channel = new Channel(channelId, this);
        // 把channel对象放入connection管理 channel 的哈希表中
        channelMap.put(channelId, channel);
        // 同时也需要把创建channel消息告诉服务器，服务器那边就会记录这个channel对应哪个socket
        boolean ok = channel.createChannel();
        if(!ok) {
            // 服务端创建失败，把刚才已经加入hash表的键值对，删除了
            channelMap.remove(channelId);
        }
        return channel;
    }
}
