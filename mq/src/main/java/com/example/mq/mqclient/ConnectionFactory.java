package com.example.mq.mqclient;

import lombok.Data;

import java.io.IOException;

/**
 * 连接工厂
 * 主要功能：创建连接，Connection对象
 */
@Data
public class ConnectionFactory {
    // broker server的ip地址
    private String host;
    // broker server的端口号
    private int port;

    // 访问broker server的哪个虚拟主机，暂时先不实现
//    private String virtualHostName;
//    private String username;
//    private String password;

    public Connection newConnection() throws IOException {
        Connection connection = new Connection(host, port);
        return connection;
    }

}
