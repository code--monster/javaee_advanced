package com.example.demo.mapper;

import com.example.demo.entity.Articleinfo;
import com.example.demo.entity.vo.ArticleinfoVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper //接口在编译时生成具体实现类
public interface ArticleMapper {
    //通过Id获取文章的作者
    ArticleinfoVO getById(@Param("id") Integer id);
}
