package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper //添加mapper注解后这个接口在编译时会生成相应的实现类
public interface UserMapper {
    //通过id获取用户
    Userinfo getUserById(@Param("userId") Integer id);
    //获取全部用户
    List<Userinfo> getAllUser();
    //添加一个用户
    int add(Userinfo userinfo);
    //添加一个用户并返回用户的自增id
    int addGetId(Userinfo userinfo);
    //修改一个数据
    int upUserName(Userinfo userinfo);
    //删除一条记录
    int delById(@Param("id") Integer id);
    //登入功能
    Userinfo login(@Param("username")  String username,
                   @Param("password") String password);

    //like查询
    List<Userinfo> getListByName(@Param("username") String username);

    //动态sql，if标签
    int add2(Userinfo userinfo);

    int add3(Userinfo userinfo);

    List<Userinfo> getListByParam(String username, String password);

    int update2(Userinfo userinfo);

    int dels(List<Integer> ids);
}
