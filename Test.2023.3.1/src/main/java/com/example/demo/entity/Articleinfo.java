package com.example.demo.entity;

import lombok.Data;

@Data
public class Articleinfo {
    private int id;
    private String title;
    private String content;
    private String createtime;
    private String updatetime;
    private int uid;
    private int rcount;
    private int state;
}
