package com.example.demo.entity.vo;

import com.example.demo.entity.Articleinfo;
import lombok.Data;

@Data
public class ArticleinfoVO extends Articleinfo {
    private String username;
}
