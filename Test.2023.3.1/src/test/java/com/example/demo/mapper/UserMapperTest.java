package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest //1.声明测试的环境是在spring boot框架下
@Transactional // 在执行完测试后数据库的数据回滚，不会污染数据库
class UserMapperTest {
    @Autowired //2.注入测试对象
    private UserMapper userMapper;

//    @Test
//    void getUserById() {
//        //3.执行单元测试代码业务
//        Userinfo userinfo = userMapper.getUserById(1);
//        Assertions.assertEquals("admin", userinfo.getUsername());
//    }
//
//    @Test
//    void getAllUser() {
//        List<Userinfo> list = userMapper.getAllUser();
//        //Assertions.assertEquals(1, list.size());
//        System.out.println(list);
//    }


//
//    @Test
//    void add() {
//        //伪代码，spring boot是不用New，构建前端传来的对象
//        Userinfo userinfo = new Userinfo();
//        userinfo.setUsername("张三");
//        userinfo.setPassword("123456");
//        userinfo.setCreatetime(LocalDateTime.now());
//        userinfo.setUpdatetime(LocalDateTime.now());
//        int ret = userMapper.add(userinfo);
//        Assertions.assertEquals(1, ret);
//    }
//
//    @Test
//    void addGetId() {
//        //伪代码
//        Userinfo userinfo = new Userinfo();
//        userinfo.setUsername("李四");
//        userinfo.setPassword("233456");
//        userinfo.setCreatetime(LocalDateTime.now());
//        userinfo.setUpdatetime(LocalDateTime.now());
//        int ret = userMapper.addGetId(userinfo);
//        Assertions.assertEquals(1, ret);
//        int id = userinfo.getId();
//        System.out.println("用户id:" + id);
//    }
//
//    @Test
//    void upUserName() {
//        //伪代码
//        Userinfo userinfo = new Userinfo();
//        userinfo.setId(5);
//        userinfo.setUsername("小六");
//        int ret = userMapper.upUserName(userinfo);
//        Assertions.assertEquals(1, ret);
//    }
//
//    @Test
//    void delById() {
//        Integer id = 6;
//        int ret = userMapper.delById(id);
//        Assertions.assertEquals(1, ret);
//    }
//
//    @Test
//    void login() {
//        String username = "admin";
//        String password = "' or 1='1";
//        userMapper.login(username, password);
//    }
//
//    @Test
//    void getListByName() {
//        String username = "m";
//        userMapper.getListByName(username);
//    }

    @Test
    void add2() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("张三");
        userinfo.setPassword("12345");
        userinfo.setPhoto(null);
        int result = userMapper.add2(userinfo);
        System.out.println(result);
    }

    @Test
    void add3() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername(null);
        userinfo.setPassword(null);
        userinfo.setPhoto(null);
        int result = userMapper.add3(userinfo);
        System.out.println(result);
    }

    @Test
    void getListByParam() {
        Userinfo userinfo = new Userinfo();

        List<Userinfo> list = userMapper.getListByParam(null, "admin");
        System.out.println(list);
    }

    @Test
    void update2() {
        Userinfo userinfo = new Userinfo();
        userinfo.setUsername("张三");
        userinfo.setId(1);
        userinfo.setPassword(null);
        int result = userMapper.update2(userinfo);
        System.out.println(result);
    }

    @Test
    void dels() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        int result = userMapper.dels(list);
        System.out.println(result);
    }
}