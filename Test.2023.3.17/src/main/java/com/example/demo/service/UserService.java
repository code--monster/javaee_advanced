package com.example.demo.service;

import com.example.demo.entity.Userinfo;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public Integer add(Userinfo userinfo) {
        int ret = userMapper.add(userinfo);
        System.out.println(ret);
        return ret;
    }
}
