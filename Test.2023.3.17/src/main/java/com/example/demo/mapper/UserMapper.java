package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper //这个接口在编译时会把生成相应的实现类
public interface UserMapper {
    int add(Userinfo userinfo);
}