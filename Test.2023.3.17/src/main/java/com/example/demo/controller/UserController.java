package com.example.demo.controller;

import com.example.demo.entity.Userinfo;
import com.example.demo.service.LogService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private DataSourceTransactionManager transactionManager;
    @Autowired
    private TransactionDefinition transactionDefinition;//设置事务属性
    @Autowired
    private LogService logService;
    @RequestMapping("/add")
    public int add(Userinfo userinfo) {
        //非空校验
        if(userinfo == null || !StringUtils.hasLength(userinfo.getUsername())
            || !StringUtils.hasLength(userinfo.getPassword())) {
            return 0;
        }
        //开启事务
        TransactionStatus transactionStatus = transactionManager
                .getTransaction(transactionDefinition);
        //手动设置时间
        userinfo.setCreatetime(LocalDateTime.now().toString());
        userinfo.setUpdatetime(LocalDateTime.now().toString());
        int result = userService.add(userinfo);

        System.out.println("添加：" + result);

        //transactionManager.rollback(transactionStatus);//传要回滚哪个事务
        //提交事务
        transactionManager.commit(transactionStatus);
        return result;
    }
    //使用@Transactional声明事务
    @RequestMapping("/insert")
    @Transactional(propagation = Propagation.NESTED)//设置事务传播机制
    public int insert(Userinfo userinfo) {
        if(userinfo == null || !StringUtils.hasLength(userinfo.getUsername())
            || !StringUtils.hasLength(userinfo.getPassword())) {
            return 0;
        }
        int result = userService.add(userinfo);
//        try {
//            int num = 10 / 0;
//        } catch (Exception e) {
//            //使用代码手动回滚事务
//            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
//        }
        //说明数据已经插入成功
        if(result > 0) {
            logService.add();
        }
        return result;
    }
}
