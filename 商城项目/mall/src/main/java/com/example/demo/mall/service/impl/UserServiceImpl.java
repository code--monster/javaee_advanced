package com.example.demo.mall.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.mall.service.IUserService;
import com.example.demo.mall.domain.entity.User;
import com.example.demo.mall.dao.UserMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.mall.util.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User login(String phone, String password) {
        //密码加密
        String passwordMd5 = MD5Util.MD5Encode(password, "utf-8");

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        //满足phone和password都相等
        queryWrapper.eq("phone", phone).eq("password", passwordMd5);
        User user = userMapper.selectOne(queryWrapper);
        return user;
    }
}
