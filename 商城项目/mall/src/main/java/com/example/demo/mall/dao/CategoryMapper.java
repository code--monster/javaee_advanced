package com.example.demo.mall.dao;

import com.example.demo.mall.domain.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
public interface CategoryMapper extends BaseMapper<Category> {

}
