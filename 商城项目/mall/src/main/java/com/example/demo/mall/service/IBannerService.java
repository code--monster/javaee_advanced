package com.example.demo.mall.service;

import com.example.demo.mall.domain.entity.Banner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
public interface IBannerService extends IService<Banner> {

}
