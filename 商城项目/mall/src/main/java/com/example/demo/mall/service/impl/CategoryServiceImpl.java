package com.example.demo.mall.service.impl;

import com.example.demo.mall.service.ICategoryService;
import com.example.demo.mall.domain.entity.Category;
import com.example.demo.mall.dao.CategoryMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

}
