package com.example.demo.mall.controller;

import com.example.demo.mall.domain.ResponseResult;
import com.example.demo.mall.domain.entity.User;
import com.example.demo.mall.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
@Controller
@ResponseBody
@RequestMapping("/user")
public class UserController {
    @Autowired
    private IUserService userService;
    @PostMapping("/login")
    public ResponseResult login(String phone, String password, String verifyCode, HttpSession session) {
        if(phone.isEmpty() || password.isEmpty()) {
            return ResponseResult.failResult("手机号或者密码为空");
        }
        if(verifyCode.isEmpty()) {
            return ResponseResult.failResult("验证码不能为空");
        }
        if(!phone.matches("^\\d{11}$")) {
            return ResponseResult.failResult("手机号格式不对");
        }
        String captchaCode = session.getAttribute("verifyCode").toString();
        if(!verifyCode.toLowerCase().equals(captchaCode)) {
            return ResponseResult.failResult("验证码错误");
        }
        User user = userService.login(phone, password);
        if(user != null) {
            // 用户登入成功后，把用户id存入session中，后序用来判断用户是否已经登入
            session.setAttribute("userid", user.getId());
            return ResponseResult.okResult(user);
        }else {
            return ResponseResult.failResult("登入失败");
        }
    }
}
