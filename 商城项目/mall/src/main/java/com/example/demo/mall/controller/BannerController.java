package com.example.demo.mall.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
@Controller
@RequestMapping("/banner")
public class BannerController {

}
