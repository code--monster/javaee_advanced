package com.example.demo.mall.service.impl;

import com.example.demo.mall.service.IProductService;
import com.example.demo.mall.domain.entity.Product;
import com.example.demo.mall.dao.ProductMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

}
