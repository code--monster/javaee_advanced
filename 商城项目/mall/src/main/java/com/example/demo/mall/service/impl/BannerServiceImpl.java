package com.example.demo.mall.service.impl;

import com.example.demo.mall.service.IBannerService;
import com.example.demo.mall.domain.entity.Banner;
import com.example.demo.mall.dao.BannerMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
@Service
public class BannerServiceImpl extends ServiceImpl<BannerMapper, Banner> implements IBannerService {

}
