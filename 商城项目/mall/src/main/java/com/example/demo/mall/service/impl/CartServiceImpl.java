package com.example.demo.mall.service.impl;

import com.example.demo.mall.service.ICartService;
import com.example.demo.mall.domain.entity.Cart;
import com.example.demo.mall.dao.CartMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
@Service
public class CartServiceImpl extends ServiceImpl<CartMapper, Cart> implements ICartService {

}
