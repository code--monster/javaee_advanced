package com.example.demo.mall.dao;

import com.example.demo.mall.domain.entity.Banner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author WH
 * @since 2023-06-26
 */
public interface BannerMapper extends BaseMapper<Banner> {

}
