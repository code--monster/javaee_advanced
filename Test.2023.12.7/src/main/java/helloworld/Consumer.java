package helloworld;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {
    public static final String QUEUE_NAME = "hello";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("101.133.141.74");
        factory.setPort(5672);
        factory.setVirtualHost("study");
        factory.setUsername("guest");
        factory.setPassword("guest");

        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        // 消费者成功消费时的回调
        DeliverCallback deliverCallback = (consumerTag,message) -> {
            System.out.println(new String(message.getBody()));
        };
        // 消费者取消消费时的回调
        CancelCallback callback = (consumerTag) -> {
            System.out.println("消费者取消消费接口回调");
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, callback);
        System.out.println("消费者执行完毕");
    }
}
