package helloworld;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

public class Producer {
    // 队列名
    public static final String QUEUE_NAME = "hello";

    public static void main(String[] args) throws IOException, TimeoutException {
        // 工厂模式
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("101.133.141.74"); // MQ所在机器的ip
        factory.setPort(5672); // 指定MQ服务端口
        factory.setVirtualHost("study"); // 指定使用的虚拟主机
        factory.setUsername("guest");
        factory.setPassword("guest");

        // 创建tcp连接,使用工厂方法
        Connection connection = factory.newConnection();
        // 创建信道
        Channel channel = connection.createChannel();
        // 创建队列
        channel.queueDeclare(QUEUE_NAME, true, false, false, null);
        String message = "hello RabbitMQ";
        // 发送消息
        channel.basicPublish("", QUEUE_NAME, null, message.getBytes(StandardCharsets.UTF_8));

        // 关闭资源
        channel.close();
        connection.close();

        System.out.println("消息生产完毕");

    }
}
