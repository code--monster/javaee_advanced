package WorkQueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitUtils {
    private static ConnectionFactory connectionFactory;
    // 放到静态代码块中，在类加载时执行，只执行一次，达到工厂只创建一次，每次获取是新连接的效果
    static {
        // 创建连接工厂
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("101.133.141.74");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("study");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
    }
    // 封装连接对象
    public static Connection getConnection() {
        try {
            return connectionFactory.newConnection();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
        return null;
    }

    // 关闭资源
    public static void closeConnectionAndChannel(Channel channel, Connection connection) {
        try {
            if(channel != null) {
                channel.close();
            }
            if(connection != null) {
                connection.close();
            }
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }
}
