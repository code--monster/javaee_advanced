create database if not exists gobang;

use gobang;

drop table if exists user;

create table user (
    userId int primary key auto_increment,
    username varchar(30) unique,
    password varchar(65),
    score int,
    totalCount int,
    winCount int
);

insert into user values(null, "zhangsan", '123456', 1200, 0, 0);
insert into user values(null, "lisi", "234567", 1200, 0, 0);
insert into user values(null, "wangwu", "345678", 1200, 0, 0);