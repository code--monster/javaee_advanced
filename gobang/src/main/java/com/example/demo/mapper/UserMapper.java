package com.example.demo.mapper;

import com.example.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {
    // 注册功能
    int register(User user);

    //登入功能
    User selectByName(@Param("username") String username);

    //胜者总场数+1， 获胜场数+1， 天梯分数+1
    void userWin(int userId);

    // 败者总场数+1，获胜场数不变，天体分数-30
    void userLose(int userId);

}
