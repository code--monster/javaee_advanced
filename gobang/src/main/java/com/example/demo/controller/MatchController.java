package com.example.demo.controller;

import com.example.demo.common.WebSocketUtils;
import com.example.demo.configue.MatchRequestConfig;
import com.example.demo.configue.MatchResponseConfig;
import com.example.demo.entity.User;
import com.example.demo.game.OnlineUserManager;
import com.example.demo.game.Matcher;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;


@Controller
public class MatchController extends TextWebSocketHandler {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private OnlineUserManager onlineUserManege;

    @Autowired
    private MatchResponseConfig responseConfig;

    @Autowired
    private Matcher matcher;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        /*
        1. 先获取当前用户的身份信息（当前谁在游戏大厅建立的连接）
        之所以可以getAttributes,是因为在注册WebSocket的时候
        加上的.addInterceptors(new HttpSessionHandshakeInterceptor())
        把HttpSession中的Attribute拿到WebSocket
        在Http登入时，往HttpSession中存的User数据：HttpSession.setAttribute("user",user);
        此时就可以在WebSocketSession中把之前HttpSession中存的User对象拿到
        注意：此时拿到的user可能为空！例如用户直接通过/game_hall.html访问
         */
        try {

            User user = (User) session.getAttributes().get("user");
            //2. 判断用户是否已经在线，如果已经在线，就不该继续进行后续逻辑，同时关闭webSocket连接
            // 也就是账号登入成功后，禁止在其他地方进行再登入操作
            if(onlineUserManege.getFromGameHall(user.getUserId()) != null) {
                //当前用户已经登入，要告知客服端，你已经重复登入了
                responseConfig.setOk(true);
                responseConfig.setReason("禁止多开");
                responseConfig.setMessage("repeatConnection");
                session.sendMessage(new TextMessage(objectMapper.writeValueAsString(responseConfig)));
                return;
            }
            // 3. 拿到身份信息，把玩家设置为在线状态
            onlineUserManege.enterGameHall(user.getUserId(), session);
            System.out.println("玩家" + user.getUsername() + "进入游戏大厅");
        } catch (NullPointerException e) {
            System.out.println("[MatchAPI.afterConnectionEstablished] 当前用户未登录!");
            //空指针异常，说明用户还未登入
            responseConfig.setOk(false);
            responseConfig.setReason("尚未登入");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(responseConfig)));
        }
    }

    /**
     * 处理开始匹配和处理停止匹配
     * @param session
     * @param message
     * @throws Exception
     */
    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        User user = (User) session.getAttributes().get("user");
        // 获取客户端给服务器发送的数据
        String payload = message.getPayload();
        // 此时payload是一个json格式的字符串，需要将他转为java对象 matchRequestConfig
        MatchRequestConfig requestConfig = objectMapper.readValue(payload, MatchRequestConfig.class);
        MatchResponseConfig responseConfig = new MatchResponseConfig();
        if("startMatch".equals(requestConfig.getMessage())) {
            // 进入匹配队列
            matcher.add(user);
            responseConfig.setOk(true);
            responseConfig.setMessage("startMatch");
        }else if("stopMatch".equals(requestConfig.getMessage())){
            // 退出匹配
            matcher.remove(user);
            responseConfig.setOk(true);
            responseConfig.setMessage("stopMatch");
        }else {
            responseConfig.setOk(false);
            responseConfig.setReason("非法请求");
        }
        String jsonString = objectMapper.writeValueAsString(responseConfig);
        session.sendMessage(new TextMessage(jsonString));
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        try {
            //异常下线，需要从onlineUserManager中删除
            User user = (User) session.getAttributes().get("user");//当前登入对象的会话
            //获取哈希表中存的用户信息
            WebSocketSession tmpSession = onlineUserManege.getFromGameHall(user.getUserId());
            //用户重复登入的时候，在哈希表中同样的Key，后面的value会覆盖前面的value
            //所以关闭的时候需要进行判定，是重复登入用户的关闭连接，还是第一次登入用户的关闭连接
            //如果是第一次登入，会话信息和哈希表中的会话信息是一致的
            //如果是重复登入， 哈希表存的是第一次登入的会话信息，和当前会话不一致
            if (session == tmpSession) {
                onlineUserManege.exitGameHall(user.getUserId());
            }
        } catch (NullPointerException e) {
//            e.printStackTrace();
//            responseConfig.setOk(false);
//            responseConfig.setReason("尚未登入");
//            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(responseConfig)));
            System.out.println("当前用户未登入");
        }
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        try {
            //正常下线，需要从onlineUserManager中删除
            User user = (User) session.getAttributes().get("user");//当前登入对象的会话
            //获取哈希表中存的用户信息
            WebSocketSession tmpSession = onlineUserManege.getFromGameHall(user.getUserId());
            //用户重复登入的时候，在哈希表中同样的Key，后面的value会覆盖前面的value
            //所以关闭的时候需要进行判定，是重复登入用户的关闭连接，还是第一次登入用户的关闭连接
            //如果是第一次登入，会话信息和哈希表中的会话信息是一致的
            //如果是重复登入， 哈希表存的是第一次登入的会话信息，和当前会话不一致
            if (session == tmpSession) {
                onlineUserManege.exitGameHall(user.getUserId());
            }
        } catch (NullPointerException e) {
            // 连接关闭后，不能在给客户端发送响应
//            e.printStackTrace();
//            responseConfig.setOk(false);
//            responseConfig.setReason("尚未登入");
//            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(responseConfig)));
            System.out.println("当前用户未登入");
        }

    }

}
