package com.example.demo.controller;

import com.example.demo.common.WebSocketUtils;
import com.example.demo.configue.GameReadyResponseConfig;
import com.example.demo.configue.GameRequestConfig;
import com.example.demo.configue.GameResponseConfig;
import com.example.demo.entity.User;
import com.example.demo.game.OnlineUserManager;
import com.example.demo.game.Room;
import com.example.demo.game.RoomManager;
import com.example.demo.service.UserService;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.io.IOException;

@Controller
public class GameController extends WebSocketUtils {
    private ObjectMapper objectMapper = new ObjectMapper();
//    @Autowired
//    private GameReadyResponseConfig readyResponseConfig;

    @Autowired
    private GameRequestConfig requestConfig;

    @Autowired
    private GameResponseConfig responseConfig;

    @Autowired
    private RoomManager roomManager;

    @Autowired
    private OnlineUserManager userManager;

    @Resource
    private UserService userService;

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        GameReadyResponseConfig readyResponseConfig = new GameReadyResponseConfig();
        // 1. 先获取到用户的身份信息
        User user = (User) session.getAttributes().get("user");
        if(user == null) {
            readyResponseConfig.setOk(false);
            readyResponseConfig.setReason("用户尚未登入");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyResponseConfig)));
            return;
        }
        // 2. 判断用户是否已经进入房间
        Room room = roomManager.getRoomByUserId(user.getUserId());
        if(room == null) {
            // 说明玩家没有匹配成功
            readyResponseConfig.setOk(false);
            readyResponseConfig.setReason("用户尚未匹配到!");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyResponseConfig)));
            return;
        }
        // 3. 判定当前是否多开(用户是否从其他地方进入游戏中）
        if(userManager.getFromGameHall(user.getUserId()) != null
            && userManager.getFromGameRoom(user.getUserId()) != null) {
            // 一个账号在游戏大厅，一个账号在游戏房间，也是多开
            readyResponseConfig.setOk(true);
            readyResponseConfig.setMessage("repeatConnection");
            readyResponseConfig.setReason("禁止多开游戏页面");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyResponseConfig)));
            return;
        }
        // 4. 设置当前玩家上线
        userManager.entryGameRoom(user.getUserId(), session);
        /*
        5.把两个玩家加入到游戏房间中，
          前面匹配，创建房间的过程，是在game_hall.html页面中完成的
          匹配到对手后，需要经过页面跳转到game_room.html才算正式进入游戏房间（才算玩家准备就绪）
          现在的逻辑是在game_room.html页面跳转的时候进行的（页面跳转是“大活”可能出现失败）
          执行到当前逻辑，说明玩家已经页面跳转成功了
         */
        synchronized (room) {
            if (room.getUser1() == null) {
                // 第一个玩家还未加入房间
                // 把当前的webSocket的玩家作为user1，加入房间中
                room.setUser1(user);
                // 先连入房间的玩家作为先手方
                room.setWhiteUser(user.getUserId());
                System.out.println("玩家1" + user.getUsername() + "准备就绪");
                return;
            }
            if(room.getUser2() == null) {
                // 走到这里说明，玩家1已经加入房间，当前玩家作为玩家2
                room.setUser2(user);
                System.out.println("玩家2" + user.getUsername() + "准备就绪");
                // 6. 当两个玩家都连接成功，就要让服务器，给这两个玩家返回webSocket的响应，告知双方已经准备好了
                // 通知玩家1
                noticeGameReady(room, room.getUser1(), room.getUser2());
                // 通知玩家2
                noticeGameReady(room, room.getUser2(), room.getUser1());
                return;
            }
            // 7. 如果还有玩家尝试连接，就提示报错
            readyResponseConfig.setOk(false);
            readyResponseConfig.setReason("当前玩家已经满了，您不能加入房间");
            session.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyResponseConfig)));
        }
    }

    private void noticeGameReady(Room room, User thisUser, User thatUser) throws IOException {
        GameReadyResponseConfig readyResponseConfig = new GameReadyResponseConfig();
        readyResponseConfig.setOk(true);
        readyResponseConfig.setMessage("gameReady");
        readyResponseConfig.setReason("");
        readyResponseConfig.setThisUserId(thisUser.getUserId());
        readyResponseConfig.setThatUserId(thatUser.getUserId());
        readyResponseConfig.setWhiteUser(room.getWhiteUser());
        // 把响应数据传给玩家
        WebSocketSession webSocketSession = userManager.getFromGameRoom(thisUser.getUserId());
        webSocketSession.sendMessage(new TextMessage(objectMapper.writeValueAsString(readyResponseConfig)));
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        // 1. 先从session中拿到当前用户的身份信息
        User user = (User) session.getAttributes().get("user");
        if(user == null) {
            System.out.println("[GameController]当前玩家未登入");
            return;
        }
        // 根据玩家id获取玩家对象
        Room room = roomManager.getRoomByUserId(user.getUserId());

        // 在Room中进行下棋操作
        room.putChess(message.getPayload());
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        User user = (User) session.getAttributes().get("user");
        if(user == null) {
            return;
        }
        WebSocketSession exitSession = userManager.getFromGameRoom(user.getUserId());
        if(session == exitSession) {
            userManager.exitGameRoom(user.getUserId());
        }
        System.out.println("当前用户" + user.getUsername() + "游戏房间连接异常");
        // 通知对手获胜
        noticeThatUserWin(user);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        User user = (User) session.getAttributes().get("user");
        if(user == null) {
            return;
        }
        WebSocketSession exitSession = userManager.getFromGameRoom(user.getUserId());
        if(session == exitSession) {
            userManager.exitGameRoom(user.getUserId());
        }
        System.out.println("当前用户" + user.getUsername() + "离开游戏房间");
        //通知对手获胜
        noticeThatUserWin(user);
    }

    /**
     * 通知另外一个玩家获胜
     * @param user
     */
    private void noticeThatUserWin(User user) throws IOException {
        //1. 根据当前玩家，找到玩家的房间
        Room room = roomManager.getRoomByUserId(user.getUserId());
        if(room == null) {
            // 房间已经被释放，没有对手了
            System.out.println("当前房间已经释放，无需通知对手");
            return;
        }
        // 2. 根据房间找到对手
        User thatUser = (user == room.getUser1()) ? room.getUser2() : room.getUser1();
        // 3. 拿到对手的在线状态
        WebSocketSession webSocketSession = userManager.getFromGameRoom(thatUser.getUserId());
        if(webSocketSession == null) {
            // 对手也掉线
            System.out.println("对手也掉线了，无需通知");
            return;
        }
        // 4. 构造一个响应，通知对手，你是获胜方

        responseConfig.setMessage("putChess");
        responseConfig.setUserId(thatUser.getUserId());
        responseConfig.setWinner(thatUser.getUserId());
        webSocketSession.sendMessage(new TextMessage(objectMapper.writeValueAsString(responseConfig)));
        // 5. 更新玩家的分数
        int winUserId = thatUser.getUserId();
        int loseUserId = user.getUserId();
        userService.userWin(winUserId);
        userService.userLose(loseUserId);
        // 6. 释放房间对象
        roomManager.remove(room.getRoomId(), room.getUser1().getUserId(), room.getUser2().getUserId());
    }
}
