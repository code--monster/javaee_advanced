package com.example.demo.controller;

import com.example.demo.common.PasswordUtils;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;

import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class UserController {
    @Resource
    private UserService userService;
    /**
     * 登入和注册都使用Post接口
     */
    //注册
    @PostMapping("/register")
    @ResponseBody
    public int register(String username, String password, String confirmPassword) {
        //确认密码正确
        if (password.equals(confirmPassword)) {
            User user = new User();
            user.setUsername(username);
            user.setPassword(PasswordUtils.encrypt(password));
            int result = userService.register(user);
            return result;
        }
        return -1;
    }

    //登入
    @PostMapping("/login")
    @ResponseBody
    public User login(String username, String password, HttpServletRequest request,
                      @RequestParam("verifyCode") String verifyCode, HttpSession session) {
        String kaptchaCode = session.getAttribute("verifyCode") + "";
        User user = userService.selectByName(username);
        if(user == null || !PasswordUtils.checkPassword(password, user.getPassword())
            || !verifyCode.equals(kaptchaCode)) {
            //登入失败
            return new User();
        }
        // 登入成功，保存登入信息
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("user", user);
        return user;
    }

    //获取用户信息
    @GetMapping("/userinfo")
    @ResponseBody
    public Object getUserinfo(HttpServletRequest request) {
        try {
            HttpSession session = request.getSession(false);
            User user = (User) session.getAttribute("user");
            // 用user对象，去数据库中查找，找到最新的数据
            User newUser = userService.selectByName(user.getUsername());
            return newUser;
        } catch (NullPointerException e) {
            return new User();
        }
    }
}
