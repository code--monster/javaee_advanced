package com.example.demo.common;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class PasswordUtils {
    /**
     * 注册时对密码加密
     * @param password
     * @return
     */
    public static String encrypt(String password) {
        //1. 生成盐值
        String salt = UUID.randomUUID().toString().replace("-", "");
        //2. 生成加盐以后的密码
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes(StandardCharsets.UTF_8));
        //3. 生成最终密码， 格式：salt + ”$" + saltPassword
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }

    /**
     * 登入时检查输入密码是否正确
     * @param password
     * @param finalPassword
     * @return
     */
    public static boolean checkPassword(String password, String finalPassword) {
        if(StringUtils.hasLength(password) && StringUtils.hasLength(finalPassword)
            && finalPassword.length() == 65) {
            // 1. 得到盐值
            String salt = finalPassword.split("\\$")[0];
            // 2. 使用这个盐值对password进行加密
            String confirmPassword = PasswordUtils.encrypt(password, salt);
            return finalPassword.equals(confirmPassword);
        }
        return false;
    }

    private static String encrypt(String password, String salt) {
        // 1. 根据盐值生成密码
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes(StandardCharsets.UTF_8));
        // 2. 生成约定格式的最终密码
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }
}
