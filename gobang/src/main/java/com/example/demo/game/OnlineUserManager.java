package com.example.demo.game;


import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 维护用户的在线状态，能够在代码中比较方便获取导某个用户当前的WebSocket会话，
 * 从而可以通过这个会话来给这个客户端发送消失。
 * 使用哈希表维护用户上下线状态，key就是用户id，value就是用户当前使用的websocket会话
 */
@Component
public class OnlineUserManager {
    //表示用户在游戏大厅的在线状态
    //多个用户同时匹配，同时给服务端请求建立/关闭连接，即同时对哈希表进行修改，线程不安全，
    // 使用ConcurrentHashMap来解决并发问题
    // 表示当前用户在游戏大厅的在线状态
    private ConcurrentHashMap<Integer, WebSocketSession> gameHall = new ConcurrentHashMap<>();
    // 表示当前用户在游戏房间的在线状态
    private ConcurrentHashMap<Integer, WebSocketSession> gameRoom = new ConcurrentHashMap<>();

    public void enterGameHall(int userId, WebSocketSession session) {
        gameHall.put(userId, session);
    }

    public void exitGameHall(int userId) {
        gameHall.remove(userId);
    }

    public WebSocketSession getFromGameHall(int userId) {
        return gameHall.get(userId);
    }

    public void entryGameRoom(int userId, WebSocketSession session) {
        gameRoom.put(userId, session);
    }

    public void exitGameRoom(int userId) {
        gameRoom.remove(userId);
    }

    public WebSocketSession getFromGameRoom(int userId) {
        return gameRoom.get(userId);
    }
}
