package com.example.demo.game;

import com.example.demo.DemoApplication;
import com.example.demo.configue.GameRequestConfig;
import com.example.demo.configue.GameResponseConfig;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.util.UUID;

/**
 * 由于房间不止有一个，所以不能注入到容器中（单例）
 */
@Data
public class Room {
    //给每个room生成唯一的roomId
    private String roomId;
    private User user1;
    private User user2;

    private int whiteUser; //先手方玩家的id

    private static final int MAX_ROW = 15;
    private static final int MAX_COL = 15;

    private OnlineUserManager userManager;

    private UserService userService;

    private RoomManager roomManager;

    public Room() {
        roomId = UUID.randomUUID().toString();
        userManager = DemoApplication.context.getBean(OnlineUserManager.class);
        userService = DemoApplication.context.getBean(UserService.class);
        roomManager = DemoApplication.context.getBean(RoomManager.class);
    }

    /*
    使用二维数组表示棋盘:
    1. 使用0表示当前位置还未落子
    2. 使用1表示user1落子的位置
    3. 使用2表示user2落子的位置
     */
    private int[][] board = new int[MAX_ROW][MAX_COL];

    //创建objectMapper转换json
    private ObjectMapper objectMapper = new ObjectMapper();

    public void putChess(String reqJson) throws IOException {
        // 1. 接受请求json
        GameRequestConfig requestConfig = objectMapper.readValue(reqJson, GameRequestConfig.class);
        GameResponseConfig responseConfig = new GameResponseConfig();
        // 2. 根据落子是玩家1还是玩家2，来决定数组中是1还是2
        int chess = requestConfig.getUserId() == user1.getUserId()? 1 : 2;
        int row = requestConfig.getRow();
        int col = requestConfig.getCol();
        if(board[row][col] != 0) {
            // 重复落子
            System.out.println("当前位置（" + row + "," + col + ")已经有子了" );
            return;
        }
        board[row][col] = chess;
        // 打印落子后的棋盘信息
        printBoard();
        // 3. 进行胜负判断
        int winner = checkWinner(row, col, chess);
        // 4. 给房间的所有客服端返回响应
        responseConfig.setMessage("putChess");
        responseConfig.setUserId(requestConfig.getUserId());
        responseConfig.setRow(row);
        responseConfig.setCol(col);
        responseConfig.setWinner(winner);
        // 要给用户发送webSocket数据，就需要获取到这个用户的WebSocketSession
        WebSocketSession session1 = userManager.getFromGameRoom(user1.getUserId());
        WebSocketSession session2 = userManager.getFromGameRoom(user2.getUserId());
        // 特殊情况：玩家下线，session为空
        if(session1 == null) {
            // 直接认为玩家2获胜
            responseConfig.setWinner(user2.getUserId());
            System.out.println("玩家一下线");
        }
        if(session2 == null) {
            // 直接认为玩家1获胜
            responseConfig.setWinner(user1.getUserId());
            System.out.println("玩家二下线");
        }
        // 把响应构造成json字符串，通过session进行传输
        String respJson = objectMapper.writeValueAsString(responseConfig);
        if(session1 != null) {
            session1.sendMessage(new TextMessage(respJson));
        }
        if(session2 != null) {
            session2.sendMessage(new TextMessage(respJson));
        }
        // 5. 已经决出胜负，此时这个房间就失去了存在的意义了，要记得销毁（否则回内存泄露）
        if(responseConfig.getWinner() != 0) {
            // 胜负已分
            System.out.println("游戏结束，房间销毁：" + roomId + "获胜方为：" + responseConfig.getWinner());
            // 更新获胜方和失败方的信息
            int winUserId = responseConfig.getWinner();
            int loseUserId = responseConfig.getWinner() == user1.getUserId() ? user2.getUserId() : user1.getUserId();
            userService.userWin(winUserId);
            userService.userLose(loseUserId);
            // 销毁房间
            roomManager.remove(roomId, user1.getUserId(), user2.getUserId());
        }

    }

    /**
     * 判断当前落子是否分出胜负
     * @param row
     * @param col
     * @param chess
     * @return 谁赢返回谁的userId, 胜负未分返回0
     */
    private int checkWinner(int row, int col, int chess) {
        // 1. 检查所有的行
        // 行成线，落子有5个位置即5种情况，c代表最左边的棋子
        for (int c = col - 4; c <= col ; c++) {
            try{
                if(board[row][c] == chess
                    && board[row][c+1] == chess
                    && board[row][c+2] == chess
                    && board[row][c+3] == chess
                    && board[row][c+4] == chess) {
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            }catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        //2. 检查所有列
        // 列成线，落子有5个位置5种情况， r代表最上面的棋子
        for (int r = row - 4; r <= row ; r++) {
            try {
                if(board[r][col] == chess
                    && board[r+1][col] == chess
                    && board[r+2][col] == chess
                    && board[r+3][col] == chess
                    && board[r+4][col] == chess) {
                    return chess == 1 ? user1.getUserId() :user2.getUserId();
                }
            }catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        // 检查左斜线
        for (int r = row - 4, c = col - 4; r <= row && c <= col; r++, c++) {
            try {
                if(board[r][c] == chess
                    && board[r+1][c+1] == chess
                    && board[r+2][c+2] == chess
                    && board[r+3][c+3] == chess
                    && board[r+4][c+4] == chess) {
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            }catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        //检查右斜线
        for (int r = row - 4, c = col + 4; r <= row && c >= col ; r++, c--) {
            try {
                if (board[r][c] == chess
                    && board[r-1][c+1] == chess
                    && board[r-2][c+2] == chess
                    && board[r-3][c+3] == chess
                    && board[r-4][c+4] == chess) {
                    return chess == 1 ? user1.getUserId() : user2.getUserId();
                }
            }catch (ArrayIndexOutOfBoundsException e) {
                continue;
            }
        }
        return 0;
    }

    private void printBoard() {
        System.out.println("棋盘信息" + roomId);
        System.out.println("==============================");
        for (int i = 0; i < MAX_ROW; i++) {
            for (int j = 0; j < MAX_COL; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("================================");
    }

    public static void main(String[] args) {
        Room room = new Room();
        System.out.println(room.getRoomId());
    }
}
