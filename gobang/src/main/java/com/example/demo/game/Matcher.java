package com.example.demo.game;

import com.example.demo.configue.MatchResponseConfig;
import com.example.demo.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;

/**
 * 用于实现匹配功能
 */
@Component
public class Matcher {
    //创建三个匹配队列，同一个队列的用户进行匹配
    private Queue<User> normalQueue = new LinkedList<>();
    private Queue<User> highQueue = new LinkedList<>();
    private Queue<User> veryHighQueue = new LinkedList<>();
    @Resource
    private OnlineUserManager onlineUserConfig;

    @Autowired
    private RoomManager roomManager;

//    private Room room;

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 将玩家放入匹配队列中
     * @param user
     */
    public void add(User user) {
        if(user.getScore() < 1500) {
            synchronized (normalQueue) {
                normalQueue.offer(user);
                normalQueue.notify();
            }
            System.out.println("把玩家" + user.getUsername() + "加入到normalQueue中");
        }else if (user.getScore() >= 1500 && user.getScore() < 2000) {
            synchronized (highQueue) {
                highQueue.offer(user);
                highQueue.notify();
            }
            System.out.println("把玩家" + user.getUsername() + "加入到highQueue中");
        }else {
            synchronized(veryHighQueue) {
                veryHighQueue.offer(user);
                veryHighQueue.notify();
            }
            System.out.println("把玩家" + user.getUsername() + "加入到veryHighQueue") ;
        }
    }

    /**
     * 当玩家点击停止匹配的时候，就把玩家从匹配队列中删除
     * @param user
     */
    public void remove(User user) {
        if(user.getScore() < 1500) {
            synchronized (normalQueue) {
                normalQueue.remove(user);
            }
            System.out.println("把玩家" + user.getUsername() + "移除了normalQueue");
        }else if(user.getScore() >= 1500 && user.getScore() < 2000) {
            synchronized (highQueue) {
                highQueue.remove(user);
            }
            System.out.println("把玩家" + user.getUsername() + "移除了highQueue");
        }else {
            synchronized (veryHighQueue) {
                veryHighQueue.remove(user);
            }
            System.out.println("把玩家" + user.getUsername() + "移除了veryHighQueue");
        }
    }

    /**
     * 处理匹配功能
     * @param matchQueue  匹配队列
     */
    private void handlerMatch(Queue<User> matchQueue) {
        synchronized (matchQueue) {
            /*
            1. 当匹配队列中<2个时，使用wait等待，没必要轮询
            并且使用while循环检查，因为可能是0个用户到1个用户
             */
            while(matchQueue.size() < 2) {
                try {
                    matchQueue.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            // 2. 从队列中取出两个用户
            User player1 = matchQueue.poll();
            User player2 = matchQueue.poll();
            System.out.println("匹配出两个玩家" + player1.getUsername() + "," +
                    player2.getUsername());
            //3. 获取到玩家的webSocket的会话，告诉玩家匹配成功
            WebSocketSession session1 = onlineUserConfig.getFromGameHall(player1.getUserId());
            WebSocketSession session2 = onlineUserConfig.getFromGameHall(player2.getUserId());
            //到这里匹配队列的玩家一定是在线的，因为前面的逻辑中
            //当玩家断开连接的时候就把玩家从匹配队列中移除
            //但是为了安全，咱们double check
            if(session1 == null) {
                // 如果玩家一不在线，就把玩家二重新放到匹配队列中
                matchQueue.offer(player2);
                return;
            }
            if(session2 == null) {
                // 如果玩家二不在线，就把玩家一重新放到匹配队列中
                matchQueue.offer(player1);
                return;
            }
            // 理论上现在不会发生匹配到的两个玩家是同一个玩家的情况
            // 因为1. 玩家下线，就会将玩家移出队列
            // 2. 前面的逻辑中已经禁止多开情况
            //但是咱们进行double check，防止前面的逻辑出现bug
            if(session1 == session2) {
                //把其中一个玩家放回匹配队列中
                matchQueue.offer(player1);
                return;
            }
            // 4. 把两个玩家放入一游戏房间中
            Room room = new Room();
            roomManager.add(room, player1.getUserId(), player2.getUserId());
            /*
             5. 给玩家反馈信息：你匹配到对手了
             通过webSocket返回一个message为“matchSuccess"的响应
             给两个玩家都返回，所以要返回两次
             */
            MatchResponseConfig responseConfig1 = new MatchResponseConfig();
            responseConfig1.setOk(true);
            responseConfig1.setMessage("matchSuccess");
            try {
                String json1 = objectMapper.writeValueAsString(responseConfig1);
                session1.sendMessage(new TextMessage(json1));
            } catch (IOException e) {
                e.printStackTrace();
            }
            MatchResponseConfig responseConfig2 = new MatchResponseConfig();
            responseConfig2.setOk(true);
            responseConfig2.setMessage("matchSuccess");
            String json2 = null;
            try {
                json2 = objectMapper.writeValueAsString(responseConfig2);
                session2.sendMessage(new TextMessage(json2));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Matcher() {
        //创建三个线程，分别针对这三个匹配队列，进行操作
        Thread t1 = new Thread(()-> {
            while(true) {
                handlerMatch(normalQueue);
            }
        });
        t1.start();
        Thread t2 = new Thread(() -> {
            while(true) {
                handlerMatch(highQueue);
            }
        });
        t2.start();
        Thread t3 = new Thread(() -> {
            while(true) {
                handlerMatch(veryHighQueue);
            }
        });
        t3.start();
    }
}
