package com.example.demo.configue;

import lombok.Data;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class MatchResponseConfig {
    private boolean ok;
    private String reason;
    private String message;
}
