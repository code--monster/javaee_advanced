package com.example.demo.configue;

import lombok.Data;
import org.springframework.context.annotation.Configuration;

/**
 * 落子响应
 */
@Configuration
@Data
public class GameResponseConfig {
    private String message;
    private int userId;
    private int row;
    private int col;
    private int winner;
}
