package com.example.demo.configue;

import lombok.Data;
import org.springframework.context.annotation.Configuration;

/**
 * 落子请求
 */
@Data
@Configuration
public class GameRequestConfig {
    private String message;
    private int userId;
    private int row;
    private int col;
}
