package com.example.demo.configue;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.Properties;
import com.google.code.kaptcha.util.Config;

@Configuration
public class KaptchaConfig {
    @Bean
    public DefaultKaptcha producer() {
        Properties properties = new Properties();
        properties.put("kaptcha.border", "no"); // 图片边框
        properties.put("kaptcha.textproducer.font.color", "black"); // 字体颜色
        properties.put("kaptcha.background.clear.from", "black");  //背景颜色渐变色开始色  rgb或者Color中定义的
        properties.put("kaptcha.background.clear.to", "green"); //背景颜色渐变色结束色   rgb或者Color中定义的
        properties.put("kaptcha.textproducer.char.space", "10"); //验证码文本字符间距  默认为2
        properties.put("kaptcha.textproducer.char.length","4");  // 验证码长度
        properties.put("kaptcha.image.height","34");  // 图片高

        properties.put("kaptcha.image.width", "110");  //图片宽
        properties.put("kaptcha.textproducer.font.size","25"); // 字体大小
        properties.put("kaptcha.word.impl","com.google.code.kaptcha.text.impl.DefaultWordRenderer"); //文字渲染器
        // 字体
        properties.put("kaptcha.textproducer.font.names", "宋体,楷体,微软雅黑"); //字体类型
        /**
         *  图片样式的实现类，默认WaterRipple（水纹），还有下面2种可选
         * 鱼眼com.google.code.kaptcha.impl.FishEyeGimpy    阴影com.google.code.kaptcha.impl.ShadowGimpy
         */
        properties.put("kaptcha.obscurificator.impl", "com.google.code.kaptcha.impl.WaterRipple"); //字体类型
        properties.put("kaptcha.background.impl", "com.google.code.kaptcha.impl.DefaultBackground"); //背景图片实现类，默认DefaultBackground，也只有这一个默认实现类
        properties.put("kaptcha.noise.impl","com.google.code.kaptcha.impl.NoNoise"); //文字干扰实现类，默认DefaultNoise，还可以选择com.google.code.kaptcha.impl.NoNoise没有干扰线的实现类
        Config config = new Config(properties);
        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        defaultKaptcha.setConfig(config);
        return defaultKaptcha;
    }
}



