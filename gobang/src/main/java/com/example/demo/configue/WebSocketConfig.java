package com.example.demo.configue;
import com.example.demo.common.WebSocketUtils;
import com.example.demo.controller.GameController;
import com.example.demo.controller.MatchController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

@Configuration
@EnableWebSocket //开启WebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    @Autowired
    private WebSocketUtils webSocketUtils;
    @Autowired
    private MatchController matchController;
    @Autowired
    private GameController gameController;
    //关联路径
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(webSocketUtils, "/base");//客户端访问的url调用webSocketUtils的方法
        //不但要将matchController和路径关联起来，还要添加拦截器将登入时的会话HttpSession带进来，
        // 带到WebSocket的session中
        registry.addHandler(matchController, "/findMatch")
                .addInterceptors(new HttpSessionHandshakeInterceptor());
        registry.addHandler(gameController, "/game")
                .addInterceptors(new HttpSessionHandshakeInterceptor());
    }
}
