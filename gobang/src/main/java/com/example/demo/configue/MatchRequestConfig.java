package com.example.demo.configue;

import lombok.Data;
import org.springframework.context.annotation.Configuration;
//表示一个WebSocket的匹配请求
@Configuration
@Data
public class MatchRequestConfig {
    private String message;
}
