package com.example.demo.configue;

import lombok.Data;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

/**
 * 客服端连接上到游戏房间后，服务器返回的响应
 *
 */
@Data
@Configuration
public class GameReadyResponseConfig implements Serializable {
    private String message;
    private boolean ok;
    private String reason;
    private String roomId;
    private int thisUserId;
    private int thatUserId;
    private int whiteUser; //白色棋子的先手


}
