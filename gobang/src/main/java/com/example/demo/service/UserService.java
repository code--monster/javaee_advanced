package com.example.demo.service;

import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    //注册
    public int register(User user) {
        return userMapper.register(user);
    }

    //登入
    public User selectByName(String username) {
        return userMapper.selectByName(username);
    }

    // 胜者
    public void userWin(int userId) {
        userMapper.userWin(userId);
    }

    public void userLose(int userId) {
        userMapper.userLose(userId);
    }
}
