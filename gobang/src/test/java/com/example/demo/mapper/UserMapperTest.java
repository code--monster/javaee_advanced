package com.example.demo.mapper;

import com.example.demo.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.soap.SOAPBinding;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@Transactional
class UserMapperTest {
    @Autowired
    private UserMapper userMapper;
    @Test
    void register() {
        User user = new User();
        user.setUsername("zhangsan");
        user.setPassword("123456");
        int result = userMapper.register(user);
        Assertions.assertEquals(1, result);
        System.out.println(user);

    }

    @Test
    void login() {
        String username = "zhangsan";
        String password = "123456";
        User user = userMapper.selectByName(username);
        Assertions.assertEquals(user.getPassword(), password);
    }
}