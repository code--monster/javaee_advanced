package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController //@Controller + @ResponseBody
@RequestMapping("/user")
public class UserController {
    @RequestMapping("/show-user")
    public String showUser(User user) {
        return user.toString();
    }
}
