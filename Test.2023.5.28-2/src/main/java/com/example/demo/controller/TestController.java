package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/test") //路由地址
@Controller //让框架在启动的时候加载当前类，别人才能访问
@ResponseBody //让程序返回的是一个数据而不是页面
public class TestController {
    @RequestMapping("/json")
    public String showJson(@RequestBody User user) {
        return user.toString();
    }

    @RequestMapping("/show-time")
    public String showTime(@RequestParam(value = "t1", required = false) String start,
                           @RequestParam("t2") String end) {
        return "开始时间：" + start + "| 结束时间：" + end;
    }
    //表示当前方法只支持post请求
    @RequestMapping(value = "/hi")
    public String sayHi(String name, Integer age) {
        return "hi," + name + " age: " + age;
    }
}
