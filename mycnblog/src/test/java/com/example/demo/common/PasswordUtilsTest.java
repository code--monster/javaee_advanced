package com.example.demo.common;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@Transactional
class PasswordUtilsTest {

    @Test
    void encrypt() {
        String password = PasswordUtils.encrypt("admin");
        System.out.println(password);
        System.out.println(PasswordUtils.encrypt("1234"));
        System.out.println(PasswordUtils.encrypt("456"));
    }

    @Test
    void checkPassword() {
        boolean result = PasswordUtils.checkPassword("admin", PasswordUtils.encrypt("admin"));
        System.out.println(result);
    }
}