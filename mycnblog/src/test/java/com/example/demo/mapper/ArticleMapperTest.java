package com.example.demo.mapper;

import com.example.demo.entity.ArticleInfo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
@Transactional
class ArticleMapperTest {
    @Resource
    private ArticleMapper articleMapper;

    @Test
    void getArtCountById() {
        int result = articleMapper.getArtCountById(1);
        System.out.println(result);
    }

    @Test
    void getMyList() {
        List<ArticleInfo> myList = articleMapper.getMyList(1);
        System.out.println(myList);
    }

    @Test
    void del() {
        int result = articleMapper.del(1, 1);
        System.out.println(result);
    }

    @Test
    void getDetail() {
        ArticleInfo articleInfo = articleMapper.getDetail(1);
        System.out.println(articleInfo.toString());
    }

    @Test
    void incrRCount() {
        int result = articleMapper.incrRCount(1);
        System.out.println(articleMapper.getDetail(1).toString());
    }

    @Test
    void add() {
        ArticleInfo articleInfo = new ArticleInfo();
        articleInfo.setTitle("c++");
        articleInfo.setContent("学习c++");
        articleInfo.setUpdatetime(LocalDateTime.now());
        articleInfo.setUid(1);
        int result = articleMapper.add(articleInfo);

    }

    @Test
    void update() {
        ArticleInfo articleInfo = new ArticleInfo();
        articleInfo.setUid(1);
        articleInfo.setId(1);
        articleInfo.setTitle("c++");
        articleInfo.setContent("学习c++");
        int result = articleMapper.update(articleInfo);
        System.out.println(articleMapper.getDetail(1));
    }

    @Test
    void getListByPage() {
        List<ArticleInfo> list = articleMapper.getListByPage(2, 0);
        System.out.println(list);

    }

    @Test
    void getCount() {
        int result = articleMapper.getCount();
        System.out.println(result);
    }
}