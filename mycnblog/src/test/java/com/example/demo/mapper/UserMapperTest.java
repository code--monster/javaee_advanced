package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@SpringBootTest
@Transactional
class UserMapperTest {
    @Resource
    private UserMapper userMapper;

    @Test
    void getUserById() {

        Userinfo userinfo = userMapper.getUserById(1);
        System.out.println(userinfo.toString());
    }
}