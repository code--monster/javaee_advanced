package com.example.demo.mapper;

import com.example.demo.entity.ArticleInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleMapper {
    int getArtCountById(@Param("uid") Integer uid);

    List<ArticleInfo> getMyList(@Param("uid") Integer uid);

    //删除文章，需要验证身份
    int del(@Param("id") Integer id, @Param("uid") Integer uid);

    ArticleInfo getDetail(@Param("id") Integer id);

    int incrRCount(Integer id);

    int add(ArticleInfo articleInfo);

    int update(ArticleInfo articleInfo);

    List<ArticleInfo> getListByPage(@Param("psize") Integer psize,
                                    @Param("offsize") Integer offsize);

    int getCount();
}
