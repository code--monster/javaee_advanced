package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper //在编译时把接口变成实现类
public interface UserMapper {
    //注册
    int reg(Userinfo userinfo);

    //登入功能，通过前端传来的username获取Userinfo
    Userinfo getUserByName(String username);

    Userinfo getUserById(@Param("id") Integer id);
}
