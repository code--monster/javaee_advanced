package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.entity.ArticleInfo;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@RequestMapping("/art")
@RestController
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @RequestMapping("/mylist")
    public AjaxResult getMyList(HttpServletRequest request) {
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if (userinfo == null) {
            return AjaxResult.fail(-1, "非法请求");
        }
        List<ArticleInfo> list = articleService.getMyList(userinfo.getId());
        return AjaxResult.success(list);
    }

    @RequestMapping("/del")
    public AjaxResult del(HttpServletRequest request, Integer id) {
        if (id == null || id <= 0) {
            return AjaxResult.fail(-1, "参数异常");
        }
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if (userinfo == null) {
            return AjaxResult.fail(-1, "用户未登入");
        }
        return AjaxResult.success(articleService.del(id, userinfo.getId()));
    }

    @RequestMapping("/detail")
    public AjaxResult getDetail(Integer id) {
        if (id == null || id <= 0) {
            return AjaxResult.fail(-1, "非法参数");
        }
        return AjaxResult.success(articleService.getDetail(id));
    }

    @RequestMapping("/incr-rcount")
    public AjaxResult incrRCount(Integer id) {
        if (id == null || id <= 0) {
            return AjaxResult.fail(-1, "非法参数");
        }
        return AjaxResult.success(articleService.incrRCount(id));
    }

    @RequestMapping("/add")
    public AjaxResult add(HttpServletRequest request, ArticleInfo articleInfo) {
        //非空校验
        if (articleInfo == null || !StringUtils.hasLength(articleInfo.getTitle())
                || !StringUtils.hasLength(articleInfo.getContent())) {
            return AjaxResult.fail(-1, "非法参数");
        }
        //获取发布文章用户的Id
        Userinfo user = UserSessionUtils.getUser(request);
        if (user == null || user.getId() <= 0) {
            return AjaxResult.fail(-1, "无效用户");
        }
        articleInfo.setUid(user.getId());
        return AjaxResult.success(articleService.add(articleInfo));
    }

    @RequestMapping("/update")
    public AjaxResult update(HttpServletRequest request, ArticleInfo articleInfo) {
        if (articleInfo == null || !StringUtils.hasLength(articleInfo.getContent())
                || !StringUtils.hasLength(articleInfo.getTitle()) || articleInfo.getId() <= 0) {
            return AjaxResult.fail(-1, "非法参数");
        }
        //得到登入用户的信息，id
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if (userinfo == null || userinfo.getId() <= 0) {
            return AjaxResult.fail(-1, "无效用户");
        }
        //很关键，保证修改文章人要是作者
        articleInfo.setUid(userinfo.getId());
        articleInfo.setUpdatetime(LocalDateTime.now());
        return AjaxResult.success(articleService.update(articleInfo));
    }

    /**
     * @param pindex 当前是第几页
     * @param psize  每页显示条数
     * @return
     */
    @RequestMapping("/listbypage")
    public AjaxResult getListByPage(Integer pindex, Integer psize) {
        if (pindex == null || pindex <= 1) {
            pindex = 1;
        }
        if (psize == null || psize <= 1) {
            psize = 3;
        }
        int offset = (pindex - 1) * psize;
        List<ArticleInfo> list = articleService.getListByPage(psize, offset);
        //计算当前列表有多少页
        //a.总共有多少条数据
        int totalCount = articleService.getCount();
        //b.每页有多少条数据
        int pcount = totalCount % psize == 0 ? totalCount / psize : totalCount / psize + 1;
        HashMap<String, Object> result = new HashMap<>();
        result.put("list", list);
        result.put("pcount", pcount);
        return AjaxResult.success(result);
    }
}
