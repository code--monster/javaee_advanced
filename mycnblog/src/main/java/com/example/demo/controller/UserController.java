package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.AppVariable;
import com.example.demo.common.PasswordUtils;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.UserinfoVO;
import com.example.demo.service.ArticleService;
import com.example.demo.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private ArticleService articleService;

    @RequestMapping("/reg")
    public AjaxResult reg(Userinfo userinfo) {
        //非空校验和参数判断
        if (userinfo == null || !StringUtils.hasLength(userinfo.getUsername())
                || !StringUtils.hasLength(userinfo.getPassword())) {
            return AjaxResult.fail(-1, "用户为空或参数非法");
        }
        if (userService.getUserByName(userinfo.getUsername()) != null) {
            return AjaxResult.fail(-2, "该用户名已经存在");
        }
        //密码加盐处理
        userinfo.setPassword(PasswordUtils.encrypt(userinfo.getPassword()));
        return AjaxResult.success(userService.reg(userinfo));
    }

    @RequestMapping("/login")
    public AjaxResult login(HttpServletRequest request, String username, String password) {
        System.out.println(password);
        //非空校验
        if (!StringUtils.hasLength(username) || !StringUtils.hasLength(password)) {
            return AjaxResult.fail(-1, "非法请求");
        }
        //查询数据库
        Userinfo userinfo = userService.getUserByName(username);
        //有效的用户名
        if (userinfo != null) {
            //验证密码是否正确
            if (PasswordUtils.checkPassword(password, userinfo.getPassword())) {
                //userinfo.setPassword("");//隐藏敏感信息
                //新用户，保存用户信息
                HttpSession session = request.getSession(true);
                //
                session.setAttribute(AppVariable.USER_SESSION_KEY, userinfo);
                userinfo.setPassword("");//隐藏敏感信息
                return AjaxResult.success(userinfo);
            } else {
                return AjaxResult.fail(-1, "password : " + password + " userPassword : " + userinfo.getPassword());
            }
        }
        //密码错误情况
        return AjaxResult.fail(-1, null);
    }

    @RequestMapping("/showinfo")
    public AjaxResult showinfo(HttpServletRequest request) {
        UserinfoVO userinfoVO = new UserinfoVO();
        //1.先获取到用户id
        Userinfo userinfo = UserSessionUtils.getUser(request);
        if (userinfo == null) {
            return AjaxResult.fail(-1, "非法请求");
        }
        //Spring提供的深拷贝
        BeanUtils.copyProperties(userinfo, userinfoVO);
        //2.通过用户id查询文章信息
        userinfoVO.setArtCount(articleService.getArtCountById(userinfo.getId()));
        return AjaxResult.success(userinfoVO);//返回对象的data类型是Userinfo
    }

    @RequestMapping("/logout")
    public AjaxResult logout(HttpSession session) {
        session.removeAttribute(AppVariable.USER_SESSION_KEY);
        return AjaxResult.success(1);
    }

    @RequestMapping("getuserbyid")
    public AjaxResult getUserById(Integer id) {
        if (id == null && id <= 0) {
            return AjaxResult.fail(-1, "非法参数");
        }
        Userinfo userinfo = userService.getUserById(id);
        if (userinfo == null || userinfo.getId() <= 0) {
            return AjaxResult.fail(-1, "非法参数");
        }
        UserinfoVO userinfoVO = new UserinfoVO();
        //去除用户的敏感信息
        userinfo.setPassword("");
        //深拷贝
        BeanUtils.copyProperties(userinfo, userinfoVO);
        //查询用户发表的文章数
        userinfoVO.setArtCount(articleService.getArtCountById(id));
        return AjaxResult.success(userinfoVO);
    }
}
