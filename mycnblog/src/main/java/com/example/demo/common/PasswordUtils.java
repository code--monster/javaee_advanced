package com.example.demo.common;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class PasswordUtils {
    /**
     * 注册时对密码进行加盐加密
     *
     * @param password
     * @return 保存到数据库中的密码
     */
    public static String encrypt(String password) {
        //1.生成盐值
        String salt = UUID.randomUUID().toString().replace("-", "");
        //2.生成加盐之后的密码(md5)
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes(StandardCharsets.UTF_8));
        //3.生成最终密码，约定格式：salt + $ + 加盐后的密码
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }

    /**
     * 登入时检查密码是否正确
     *
     * @param password      输入的密码
     * @param finalPassword 数据库中加密过的密码
     * @return
     */
    public static boolean checkPassword(String password, String finalPassword) {
        if (StringUtils.hasLength(password) && StringUtils.hasLength(finalPassword)
                && finalPassword.length() == 65) {
            //1.得到盐值salt
            String salt = finalPassword.split("\\$")[0];//$是通配符需要\\
            //2.拿着盐值，按照之前的加密步骤，将输入的密码进行加密，生成最终的密码
            String confirmPassword = PasswordUtils.encrypt(password, salt);
            return finalPassword.equals(confirmPassword);
        }
        return false;
    }

    /**
     * 验证输入密码是否正确时，根据盐值生成密码
     *
     * @param password
     * @param salt
     * @return
     */
    private static String encrypt(String password, String salt) {
        //根据salt生成加盐后的密码
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes(StandardCharsets.UTF_8));
        //生成约定格式的最终密码
        String finalPassword = salt + "$" + saltPassword;
        return finalPassword;
    }
}
