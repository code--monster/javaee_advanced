package com.example.demo.common;

public class AppVariable {
    //类似于同一家超市的会员卡，每个人都有外观一样的会员卡，但是卡里面的信息不同
    //是根据jsessionid来区别用户
    public static final String USER_SESSION_KEY = "USER_SESSION_KEY";
}
