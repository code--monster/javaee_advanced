package com.example.demo.service;

import com.example.demo.entity.ArticleInfo;
import com.example.demo.mapper.ArticleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ArticleService {
    @Resource
    private ArticleMapper articleMapper;

    public Integer getArtCountById(int uid) {
        return articleMapper.getArtCountById(uid);
    }

    public List<ArticleInfo> getMyList(int uid) {
        return articleMapper.getMyList(uid);
    }

    public int del(int id, int uid) {
        return articleMapper.del(id, uid);
    }

    public ArticleInfo getDetail(int id) {
        return articleMapper.getDetail(id);
    }

    public int incrRCount(int id) {
        return articleMapper.incrRCount(id);
    }

    public int add(ArticleInfo articleInfo) {
        return articleMapper.add(articleInfo);
    }

    public int update(ArticleInfo articleInfo) {
        return articleMapper.update(articleInfo);
    }

    public List<ArticleInfo> getListByPage(Integer psize, Integer offset) {
        return articleMapper.getListByPage(psize, offset);
    }

    public int getCount() {
        return articleMapper.getCount();
    }
}
