package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@RequestMapping("/user")
public class UserController {
    @RequestMapping("/hi")
    public String sayHi(String name) {
        System.out.println("测试环绕通知");
        return "Hi" + name;
    }

}
