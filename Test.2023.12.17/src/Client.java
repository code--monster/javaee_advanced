import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

public class Client {
    private Socket socket = null;
    // 建立连接
    public Client(String serverIp, Integer serverPort) throws IOException {
        socket = new Socket(serverIp, serverPort);
    }
    public void start() {
        Random random = new Random();
        try(InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream()) {
            for (int i = 0; i < 10; i++) {
                int request = random.nextInt(100);
                // 发送请求
                PrintWriter printWriter = new PrintWriter(outputStream);
                printWriter.println(request);
                System.out.println("发送的随机数："+ request);
                printWriter.flush();
            }

            // 读取响应
            Scanner scanner = new Scanner(inputStream);
            int response = scanner.nextInt();
            System.out.println(response);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void main(String[] args) throws IOException {
        Client client = new Client("127.0.0.1",6060);
        client.start();
    }
}
