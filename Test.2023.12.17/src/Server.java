import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private ServerSocket listenSocket = null;
    public Server(int port) throws IOException {
        listenSocket = new ServerSocket(port);
    }

    public void start() throws IOException {
        ExecutorService service = Executors.newCachedThreadPool();
        while (true) {
            // 接收客服端连接
            Socket clientSocket = listenSocket.accept();
            // 处理连接请求
            service.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        processConection(clientSocket);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        }
    }

    private void processConection(Socket clientSocket) throws IOException {
        try(InputStream inputStream = clientSocket.getInputStream();
            OutputStream outputStream = clientSocket.getOutputStream()) {
            while (true) {
                Scanner scanner = new Scanner(inputStream);
                if(!scanner.hasNext()) {
                    break;
                }
                int sum = 0;
                for (int i = 0; i < 10; i++) {
                    int request = scanner.nextInt();
                    sum += request;
                }

                //写回响应
                PrintWriter printWriter = new PrintWriter(outputStream);
                printWriter.println(sum / 10);
                printWriter.flush();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }finally {
            clientSocket.close();
        }
    }

//    private int process(int[] request) {
//        int sum = 0;
//        for (int i = 0; i < request.length; i++) {
//            sum += request[i];
//        }
//        return sum / request.length;
//
//    }

    public static void main(String[] args) throws IOException {
        Server server = new Server(6060);
        server.start();;
    }
}
