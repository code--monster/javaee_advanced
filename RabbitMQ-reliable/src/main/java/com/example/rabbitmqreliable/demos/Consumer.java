package com.example.rabbitmqreliable.demos;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Component
public class Consumer {
    @RabbitListener(queues = RabbitMQConfig.CONFIRM_QUEUE_NAME) // 监听的队列
    public void consumerListener(Message message, Channel channel) {
        byte[] body = message.getBody();;
        String msg = new String(body);
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        try {
            // 进行业务处理
            int a = 1 / 0; //产生异常
            System.out.println("[consumerListener],msg:" + msg);

            // 没有产生异常，给服务端返回ack
            // 第一个参数：表示消息的标签，保证消息唯一性
            // 第二个数：表示是否需要进行批量应答
            channel.basicAck(deliveryTag, true);
        } catch (Exception e) {
            e.printStackTrace();
            // 产生异常
            // 给rabbitmq返回nack
            // 第三个参数：表示是否将消息重新放入队列中
            try {
               channel.basicNack(deliveryTag, true, true);
                // 执行代码会死循环，因为没有设置最大重试次数
                // 消息的实际消费次数可以借助redis计算
                // 一旦消息的实际消费次数大于最大消费次数，那么此时需要给rabbitmq返回ack删除该消息
                // 返回之前要将该消息记录数据库中，后期人工处理
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
