package com.example.rabbitmqreliable;

import com.example.rabbitmqreliable.demos.RabbitMQConfig;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.UUID;

@SpringBootTest
public class ProviderTests {
    /**
     * 目标：让生产者获取到rabbitmq服务返回的ack或nack
     * 做法：rabbitTemplate需要绑定对应的回调函数
     * 分析：目前的rabbitTemplate是spring托管的，并没有对应的回调函数，需要自定义
     * 实施：需要自定义rabbitTemplate，并注入到spring容器中
     * 一旦我们在spring容器中配置了一个rabbitTemplate，
     * 那么spring boot就不会对rabbitTemplate进行自动化配置
     */
    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Test
    public void test1() {
        // 发送消息前把消息写入数据库，并分配唯一id（如果发送失败，可以拿这个id去查数据库，重新发送）
        // 并且还要记录消息实际发送次数，以及消息状态。当超过发送次数超过了规定值，就设置消息的status为2,发送成功设置消息状态为1
        String msgId = UUID.randomUUID().toString().replace("-","");
        CorrelationData correlationData =new CorrelationData(msgId);
        // 把routingKey写错
        rabbitTemplate.convertAndSend(RabbitMQConfig.CONFIRM_EXCHANGE_NAME , RabbitMQConfig.CONFIRM_ROUTING_KEY , "HELLO CONFIRM", correlationData);
    }

}
