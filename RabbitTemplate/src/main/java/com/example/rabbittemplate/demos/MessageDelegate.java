package com.example.rabbittemplate.demos;

public class MessageDelegate {
    public void handleMessage(byte[] messageBody) {
        System.out.println("默认方法，消息内容：" + new String(messageBody));
    }

    public void consumerMessage(byte[] messageBody) {
        System.out.println("自定义名称适配器方法，消息内容：" + new String(messageBody));
    }

    public void method1(byte[] messageBody) {
        System.out.println("method1收到消息内容：" + new String(messageBody));
    }

    public void method2(byte[] messageBody) {
        System.out.println("method2收到消息内容：" + new String(messageBody));
    }


    // 添加string参数方法接收转换为string的消息
    public void consumerMessage(String messageInfo) {
        System.out.println("自定义名称转换string,消息内容：" + messageInfo);
    }

    public void method1(String messageInfo) {
        System.out.println("method1只接收string方法，收到消息内容：" + messageInfo);
    }

    public void method2(String messageInfo) {
        System.out.println("method2只接收string方法，收到消息内容：" + messageInfo);
    }

}
