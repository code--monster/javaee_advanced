package com.example.rabbittemplate.demos;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.amqp.support.ConsumerTagStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Configuration
public class RabbitConfig {
    @Autowired
    private RabbitProperties properties;

    /**
     *rabbitmq连接池，从配置文件读取参数
     * @return
     */
    @Bean
    public ConnectionFactory connectionFactory() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        connectionFactory.setHost(properties.getHost());
        connectionFactory.setPort(properties.getPort());
        connectionFactory.setUsername(properties.getUsername());
        connectionFactory.setPassword(properties.getPassword());
        connectionFactory.setVirtualHost(properties.getVirtualHost());
        return connectionFactory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(CachingConnectionFactory cachingConnectionFactory) {
        return new RabbitTemplate(connectionFactory());
    }

    // 创建交换机
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("TemplateDirectEx", false, false);
    }
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("TemplateFanoutEx", false, false);
    }

    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange("TemplateTopicEx", false, false);
    }

    // 创建队列
    @Bean
    public Queue directQueue1() {
        return new Queue("directQueue1", true);
    }
    @Bean
    public Queue directQueue2() {
        return new Queue("directQueue2", true);
    }

    @Bean
    public Queue topicQueue1() {
        return QueueBuilder.durable("topicQueue1").build();
    }
    @Bean
    public Queue topicQueue2() {
        return QueueBuilder.durable("topicQueue2").build();
    }

    @Bean
    public Binding directBind1() {
        return new Binding("directQueue1", Binding.DestinationType.QUEUE,
                "TemplateDirectEx", "weixin", null);
    }
    @Bean
    public Binding directBind2() {
        return BindingBuilder.bind(new Queue("directQueue2", false))
                .to(new DirectExchange("TemplateDirectEx"))
                .with("weixin");
    }
    // 将Bean方法名作为参数代入
    @Bean
    public Binding topicBind1(@Qualifier("topicQueue1") Queue queue,
                              @Qualifier("topicExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("user.#");

    }
    @Bean
    public Binding topicBind2(@Qualifier("topicQueue2") Queue queue,
                              @Qualifier("topicExchange") TopicExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("vip.*");
    }

    @Bean
    public SimpleMessageListenerContainer messageListenerContainer(CachingConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.setQueues(topicQueue1(), topicQueue2(), directQueue1(), directQueue2());
        container.setConcurrentConsumers(1);
        container.setMaxConcurrentConsumers(10);
        container.setDefaultRequeueRejected(false);
        container.setAcknowledgeMode(AcknowledgeMode.AUTO);
        container.setConsumerTagStrategy(new ConsumerTagStrategy() {
            @Override
            public String createConsumerTag(String s) {
                return s + "_" + UUID.randomUUID().toString();
            }
        });
//        container.setMessageListener(new ChannelAwareMessageListener() {
//            @Override
//            public void onMessage(Message message, Channel channel) throws Exception {
//                System.out.println("消费者的消息：" + new String(message.getBody()));
//            }
//        });

        // 消息监听方法2：使用消息适配器
//        MessageListenerAdapter adapter = new MessageListenerAdapter(new MessageDelegate());
//        adapter.setDefaultListenerMethod("consumerMessage");
//        Map<String, String> queueOrTagToMethodName = new HashMap<>();
//        queueOrTagToMethodName.put("directQueue1", "method1");
//        queueOrTagToMethodName.put("directQueue2", "method2");
//        adapter.setQueueOrTagToMethodName(queueOrTagToMethodName);
//        container.setMessageListener(adapter);


//        MessageListenerAdapter adapter = new MessageListenerAdapter(new MessageDelegate());
//        adapter.setDefaultListenerMethod("consumerMessage");
//        adapter.setMessageConverter(new MyMessageConverter());
//        container.setMessageListener(adapter);

        MessageListenerAdapter adapter = new MessageListenerAdapter(new MessageDelegate());
        adapter.setMessageConverter(new MyMessageConverter());
        adapter.setDefaultListenerMethod("consumerMessage");
        Map<String, String> queueOrTagMethodName = new HashMap<>();
        queueOrTagMethodName.put("directQueue1", "method1");
        queueOrTagMethodName.put("directQueue2", "method2");
        adapter.setQueueOrTagToMethodName(queueOrTagMethodName);
        container.setMessageListener(adapter);

        return container;
    }
}
