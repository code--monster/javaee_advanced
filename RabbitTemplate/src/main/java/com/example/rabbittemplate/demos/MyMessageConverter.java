package com.example.rabbittemplate.demos;


import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.support.converter.MessageConversionException;
import org.springframework.amqp.support.converter.MessageConverter;

import java.nio.charset.StandardCharsets;

// 自定义消息转换器
public class MyMessageConverter implements MessageConverter {
    // 将java对象转换成message对象
    @Override
    public Message toMessage(Object object, MessageProperties messageProperties) throws MessageConversionException {
        return new Message(object.toString().getBytes(StandardCharsets.UTF_8),messageProperties);
    }
    // 将message对象转换成java对象
    @Override
    public Object fromMessage(Message message) throws MessageConversionException {
        String contentType = message.getMessageProperties().getContentType();
        if(contentType != null && contentType.contains("application/json")) {
            return new String(message.getBody());
        }
        return message.getBody();
    }
}
