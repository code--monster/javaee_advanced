package controller;

import component.User;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
public class BeanScopeController2 {
    @Resource
    private User user;

    public User getUser() {
        User user2 = user;
        return user2;
    }
}
