package controller;

import component.User;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

@Controller
public class BeanScopeController {
    @Resource
    private User user;

    public User getUser() {
        User user1 = user;
        user1.setId(2);
        user1.setName("李四");
        return user1;
    }
}
