import component.User;
import controller.BeanScopeController;
import controller.BeanScopeController2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context =
                new ClassPathXmlApplicationContext("spring-config.xml");
        //修改前的Bean对象
        User user = context.getBean("user", User.class);
        System.out.println("Bean对象修改前： " + user.toString() );
        //A修改Bean对象后
        BeanScopeController beanScopeController =
                context.getBean("beanScopeController", BeanScopeController.class);
        System.out.println("A对象修改后： " + beanScopeController.getUser().toString());
        //B从Spring中获取Bean对象，看是修改前的值还是没有被修改
        BeanScopeController2 beanScopeController2 =
                context.getBean("beanScopeController2", BeanScopeController2.class);
        System.out.println("B对象修改后： " + beanScopeController2.getUser().toString());
    }
}
