package com.example.priorityqueue;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class PriorityProvider {
    private static final String EXCHANGE_NAME = "fanoutExchange";

    public static void main(String[] args) throws IOException {
        Connection connection = RabbitUtils.getConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);

        // 对队列设置优先级为10，值应该为0-255之间
        Map<String, Object> map = new HashMap<>();
        map.put("x-max-priority", 10);
        channel.queueDeclare("KeFuQueue", false, false, false,map);
        channel.queueBind("KeFuQueue", EXCHANGE_NAME, "", null);

        // 发送消息
        for (int i = 0; i < 10; i++) {
            String message = "第" + i + "条消息";
            if(i % 5 == 0) {
                AMQP.BasicProperties properties = new AMQP.BasicProperties()
                        .builder().priority(5).build(); // 设置消息优先级为5
                channel.basicPublish(EXCHANGE_NAME, "", properties, message.getBytes(StandardCharsets.UTF_8));
            }else {
                channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes(StandardCharsets.UTF_8));
            }
        }
        System.out.println("消息全部发送");
    }
}
