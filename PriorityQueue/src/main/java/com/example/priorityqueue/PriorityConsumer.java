package com.example.priorityqueue;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.Arrays;

/**
 * 只有消息在队列中排序完成之后再被消费者消费才能体现优先级，如果消费者消费时间小于排序时间不会生效
 */
public class PriorityConsumer {
    private static final String QUEUE_NAME = "KeFuQueue";

    public static void main(String[] args) throws IOException {
        Connection connection = RabbitUtils.getConnection();
        Channel channel = connection.createChannel();
        // 消费者成功消费回调逻辑
        DeliverCallback deliverCallback = (consumerTag, message) -> {
            System.out.println("收到消息为：" + new String(message.getBody()));
            channel.basicAck(message.getEnvelope().getDeliveryTag(), false);
        };

        // 消费者取消消费回调逻辑
        CancelCallback cancelCallback = s -> {
            System.out.println("取消消费操作");
        };

        channel.basicConsume(QUEUE_NAME, false, deliverCallback,cancelCallback);
    }
}
