package com.example.priorityqueue;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitUtils {
    private static final ConnectionFactory connectionFactory;
    static {
        connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("101.133.141.74");
        connectionFactory.setPort(5672);
        connectionFactory.setVirtualHost("study");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
    }

    // 提供连接对象的方法，封装
    public static Connection getConnection() {
        try {
            return connectionFactory.newConnection();
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

    // 关闭资源
    public static void closeConnectionAndChannel(Channel channel, Connection connection) {
        try {
            if(channel != null) {
                channel.close();
            }
            if(connection != null) {
               connection.close();
            }
        } catch (IOException | TimeoutException e) {
            throw new RuntimeException(e);
        }
    }

}
